drop database if exists `Acme-Music`;
create database `Acme-Music`;
grant select, insert, update, delete on `Acme-Music`.* to 'acme-user'@'%';
grant select, insert, update, delete, create, drop, references, index, alter, create temporary tables, 
lock tables, create view, create routine, alter routine, execute, trigger, show view
    on `Acme-Music`.* to 'acme-manager'@'%';