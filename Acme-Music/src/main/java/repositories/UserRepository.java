
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.User;
import security.UserAccount;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	// Default
	@Query("select u from User u where u.userAccount = ?1")
	User findByUserAccount(UserAccount userAccount);

	// Get Users following
	@Query("select a.actorsImFollowing from Actor a where a.id = ?1")
	Collection<User> findUsersImFollowing(int id);

	// Search an user by keyword which must match in artist's name or nickname
	@Query("select u from User u where u.name like %?1% or u.nickname like %?1%")
	Collection<User> searchUsersByKeyword(String keyword);

	//Query - The users who have posted +7 the average number of comments per song..
	@Query("select a from User a where a.writtenComments.size>=(select avg(a.writtenComments.size) from User a)*0.07")
	Collection<User> actorsPostedMoreSevenPorcentOfComments();

	@Query("select a from User a where a.writtenComments.size<=(select avg(a.writtenComments.size) from User a)*0.07")
	Collection<User> actorsPostedLessSevenPorcentOfComments();
}
