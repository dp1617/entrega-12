
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Song;

@Repository
public interface SongRepository extends JpaRepository<Song, Integer> {

	// Search a song by keyword which must match in song's genre or name
	@Query("select s from Song s where s.name like %?1% or s.genre = ?1")
	Collection<Song> searchSongByKeyword(String keyword);

	// Get favoties songs from a user
	@Query("select s from Song s join s.users u where u.id = ?1")
	Collection<Song> getFavoritesSongs(int userId);

	// Querys ---------------------------------------------------------------------

	// The ratio of songs versus albums
	@Query("select 1.0*(select count(s) from Song s)/count(a) from Album a")
	Double ratioSongsVAlbums();

	// The average, maximum and minimum of songs per artist.
	@Query("select avg(al.songs.size), max(al.songs.size), min(al.songs.size) from Artist a join a.albums al")
	Collection<Object[]> avgMaxMinSongsPerArtist();

	//The song which have been mark as favorite more times.
	@Query("select max(s.favs) from Song s")
	Double maxFavsPerSong();

}
