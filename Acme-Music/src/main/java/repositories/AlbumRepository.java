
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Album;
import domain.Artist;
import domain.Song;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Integer> {

	// The average, maximum and minimum of albums per artist
	@Query("select avg(a.albums.size), max(a.albums.size), min(a.albums.size) from Artist a")
	Collection<Object[]> avgMaxMinAlbumsPerArtist();

	@Query("select a from Album a where a.artist = ?1")
	Collection<Album> findByArtist(Artist artist);

	// Get album songs
	@Query("select a.songs from Album a where a.id = ?1")
	Collection<Song> getAlbumSongs(int albumId);

	// Get album of an artist
	@Query("select a from Album a where a.artist.id = ?1")
	Collection<Album> getArtistAlbums(int artistId);

	// Get songs of artist albums
	@Query("select a.songs from Album a where a.artist.id = ?1")
	Collection<Song> getMyAlbumSongs(int artistId);

	//The album with more songs.
	@Query("select max(a.songs.size) from Album a")
	Double maxSongsPerAlbum();

}
