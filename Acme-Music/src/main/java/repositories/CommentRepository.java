
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

	// The average of comments per user
	@Query("select avg(u.writtenComments.size) from User u")
	Double avgCommentsPerUser();

}
