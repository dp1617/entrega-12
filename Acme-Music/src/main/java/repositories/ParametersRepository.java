
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import domain.Parameters;

@Repository
public interface ParametersRepository extends JpaRepository<Parameters, Integer> {

}
