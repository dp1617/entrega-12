
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Billboard;

@Repository
public interface BillboardRepository extends JpaRepository<Billboard, Integer> {

	//Query - The average, minimum, maximum of albums validate per artist.
	@Query("select min(a.albums.size), max(a.albums.size), avg(a.albums.size) from Artist a")
	Collection<Object[]> minMaxAvgAgeOfAlbumsValidatePerArtist();
}
