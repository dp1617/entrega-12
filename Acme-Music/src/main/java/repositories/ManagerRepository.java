
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Integer> {

	// Default
	@Query("select a from Manager a where a.userAccount = ?1")
	Manager findByUserAccount(UserAccount userAccount);
}
