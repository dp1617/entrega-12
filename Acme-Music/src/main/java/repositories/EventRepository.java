
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {

	//Query - Listing of events in descending order regarding the number of users that are going to go.
	@Query("select e from Event e order by e.actualParticipants desc")
	Collection<Event> eventsOrderByUsers();

}
