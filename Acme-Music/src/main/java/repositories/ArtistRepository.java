
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Artist;
import security.UserAccount;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Integer> {

	// Default
	@Query("select a from Artist a where a.userAccount = ?1")
	Artist findByUserAccount(UserAccount userAccount);

	// Search an artist by keyword which must match in artist's name or nickname
	@Query("select a from Artist a where a.name like %?1% or a.nickname like %?1%")
	Collection<Artist> searchArtistByKeyword(String keyword);

	// Search an artist that doesn't have manager
	@Query("select a from Artist a where a.manager is null")
	Collection<Artist> artistsWithoutManager();

}
