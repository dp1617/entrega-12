
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import domain.EventsCalendar;

@Repository
public interface EventsCalendarRepository extends JpaRepository<EventsCalendar, Integer> {

}
