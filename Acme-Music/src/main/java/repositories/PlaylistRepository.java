
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Playlist;
import domain.Song;

@Repository
public interface PlaylistRepository extends JpaRepository<Playlist, Integer> {

	// Search a playlist by keyword which must match in the title
	@Query("select p from Playlist p where p.title like %?1%")
	Collection<Playlist> searchPlaylistByKeyword(String keyword);

	// Get playlists songs
	@Query("select p.songs from Playlist p where p.id = ?1")
	Collection<Song> getPlaylistSongs(int playlistId);

	// Get my playlists
	@Query("select p from Playlist p where p.user.id = ?1")
	Collection<Playlist> getMyPlaylists(int userId);

	//QUERIES

	//Average number of songs per playlist
	@Query("select avg(p.songs.size) from Playlist p")
	Double avgNumberSongsPerPlaylist();

}
