
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.Actor;
import domain.Artist;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {

	@Query("select a from Actor a where a.userAccount = ?1")
	Actor findByUserAccount(UserAccount userAccount);

	@Query("select a from Actor a where a.userAccount.username = ?1")
	Actor findActorByUsername(String username);

	//Query - The average, minimum, maximum of followers per artist.
	@Query("select min(a.actorsFollowingMe.size), max(a.actorsFollowingMe.size), avg(a.actorsFollowingMe.size) from Artist a")
	Collection<Object[]> minMaxAvgAgeOfFollowersPerArtist();

	//Query - The artist who have more followers
	@Query("select a from Artist a where a.actorsFollowingMe.size >= (select max(a.actorsFollowingMe.size) from Artist a)")
	Collection<Artist> artistMoreFollowers();
}
