
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.PlaylistRepository;
import domain.Playlist;
import domain.Song;
import domain.User;

@Service
@Transactional
public class PlaylistService {

	// Managed repository ----------------------------------------------------------
	@Autowired
	private PlaylistRepository	playlistRepository;


	// Constructors ----------------------------------------------------------
	public PlaylistService() {
		super();
	}


	// Supporting services ----------------------------------------------------------
	@Autowired
	private UserService		userService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private SongService		songService;


	// Simple CRUD methods ----------------------------------------------------------

	public Playlist create() {
		this.checkIfUser();

		final Playlist pl = new Playlist();
		pl.setUser(this.userService.findByPrincipal());
		pl.setTotalMinutes(0);
		pl.setTotalSeconds(0);

		return pl;
	}

	public Collection<Playlist> findAll() {
		Collection<Playlist> playlists;
		playlists = this.playlistRepository.findAll();
		Assert.notNull(playlists);
		return playlists;
	}

	public Playlist findOne(final int playlistId) {
		Assert.isTrue(playlistId != 0);
		final Playlist result = this.playlistRepository.findOne(playlistId);
		Assert.notNull(result);
		return result;
	}

	public Playlist save(final Playlist playlist) {
		this.checkIfUser();
		this.checkUserAuthor(playlist);

		Assert.notNull(playlist);
		final Playlist result = this.playlistRepository.save(playlist);
		return result;
	}

	public void delete(final Playlist playlist) {
		this.checkIfUser();
		this.checkUserAuthor(playlist);
		if (!this.getPlaylistSongs(playlist).isEmpty())
			for (final Song s : this.getPlaylistSongs(playlist))
				this.deleteSongForDelete(playlist, s);
		Assert.notNull(playlist);
		this.playlistRepository.delete(playlist);

	}
	// Other methods ----------------------------------------------------------

	public void checkUserAuthor(final Playlist pl) {
		Assert.isTrue(pl.getUser().getId() == this.userService.findByPrincipal().getId(), "No eres el autor de esta playlist");
	}

	public void addSongToPlaylist(final Song s) {
		Collection<Song> songs = new ArrayList<>();

		for (final Playlist pl : s.getPlaylists())
			if (!this.getPlaylistSongs(pl).contains(s)) {
				songs = this.getPlaylistSongs(pl);
				songs.add(s);
				pl.setSongs(songs);

				pl.setTotalMinutes(pl.getTotalMinutes() + s.getMinutes());
				pl.setTotalSeconds(pl.getTotalSeconds() + s.getSeconds());

				this.save(pl);
			}

		this.songService.saveForUser(s);
	}

	public void deleteSongForDelete(final Playlist pl, final Song s) {
		final Collection<Song> songs = this.getPlaylistSongs(pl);
		Assert.isTrue(songs.contains(s), "La playlist no contiene la canci�n");
		songs.remove(s);
		pl.setSongs(songs);

		this.save(pl);
	}

	// Get my playlists
	public Collection<Playlist> getMyPlaylists(final User u) {
		return this.playlistRepository.getMyPlaylists(u.getId());
	}

	// Delete song from playlist
	public void deleteSong(final Playlist pl, final Song s) {
		final Collection<Song> songs = this.getPlaylistSongs(pl);
		Assert.isTrue(songs.contains(s), "La playlist no contiene la canci�n");
		songs.remove(s);
		pl.setSongs(songs);

		pl.setTotalMinutes(pl.getTotalMinutes() - s.getMinutes());
		pl.setTotalSeconds(pl.getTotalSeconds() - s.getSeconds());

		this.save(pl);
	}

	public Collection<Song> getPlaylistSongs(final Playlist pl) {
		return this.playlistRepository.getPlaylistSongs(pl.getId());
	}

	public Collection<Playlist> filter(final Collection<Playlist> p) {
		this.checkIfUser();
		final Collection<Playlist> res = new ArrayList<>();

		if (this.actorService.findByPrincipal() instanceof User)
			for (final Playlist pl : p)
				if (pl.getUser().getId() != this.userService.findByPrincipal().getId())
					res.add(pl);

		return res;
	}

	// From repository, search by keyword
	public Collection<Playlist> searchPlaylistByKeyword(final String keyword) {
		return this.playlistRepository.searchPlaylistByKeyword(keyword);
	}

	// Check if user
	public void checkIfUser() {
		this.userService.checkIfUser();
	}

	//QUERY
	public Double avgNumberSongsPerPlaylist() {
		return this.playlistRepository.avgNumberSongsPerPlaylist();
	}

}
