
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Actor;
import domain.Comment;
import domain.EventsCalendar;
import domain.Manager;
import forms.ActorForm;
import repositories.ManagerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class ManagerService {

	// Managed Repository ----------------------------------------------------------
	@Autowired
	private ManagerRepository managerRepository;


	// Constructor methods ----------------------------------------------------------
	public ManagerService() {
		super();
	}


	// Supporting services ------------------------------------------------------
	@Autowired
	private ActorService	actorService;


	// Simple CRUD methods ------------------------------------------------------

	public Manager findOne(final int managerId) {
		Assert.isTrue(managerId != 0);
		Manager result;

		result = this.managerRepository.findOne(managerId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Manager> findAll() {
		Collection<Manager> result;

		result = this.managerRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Manager manager) {
		Assert.notNull(manager);

		this.managerRepository.save(manager);
	}

	// Other bussines methods ----------------------------------------------------------

	public Manager findByPrincipal() {
		Manager result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();

		result = this.managerRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Manager reconstruct(final ActorForm a) {
		final Manager manager = new Manager();

		final Collection<EventsCalendar> eventsCalendars = new ArrayList<>();
		final Collection<Comment> writtenComments = new ArrayList<>();
		final Collection<Actor> actorsImFollowing = new ArrayList<>();
		final Collection<Actor> actorsFollowingMe = new ArrayList<>();

		final UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		manager.setId(0);
		manager.setVersion(0);
		manager.setName(a.getName());
		manager.setNickname(a.getNickname());
		manager.setEmail(a.getEmail());
		manager.setPhone(a.getPhone());
		manager.setPicture(a.getPicture());
		//manager.setBanned(false);

		manager.setUserAccount(account);

		manager.setEventsCalendars(eventsCalendars);
		manager.setWrittenComments(writtenComments);
		manager.setActorsFollowingMe(actorsFollowingMe);
		manager.setActorsImFollowing(actorsImFollowing);

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return manager;
	}
	public void saveForm(Manager manager) {

		Assert.notNull(manager);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("MANAGER");
		auths.add(auth);

		password = manager.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		manager.getUserAccount().setPassword(password);

		manager.getUserAccount().setAuthorities(auths);

		manager = this.managerRepository.saveAndFlush(manager);
	}
	public void checkIfManager() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.MANAGER))
				res = true;
		Assert.isTrue(res);
	}

	public void checkAttributes(final ActorForm a) {
		final Manager manager = this.reconstruct(a);
		Assert.isTrue(manager.getPhone().matches("(^(\\+\\d{1,3})?\\d{9}$)|(^(\\*\\*\\*)$)"));
		Assert.isTrue(manager.getEmail().matches("^[A-Za-z0-9+_.-]+@(.+)$|^(\\*\\*\\*)"));
		Assert.isTrue(manager.getPicture().matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"));
		for (final Actor actor : this.actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(a.getUsername()));
	}

	public void checkAttributes(final Manager m) {
		Assert.isTrue(m.getPhone().matches("(^(\\+\\d{1,3})?\\d{9}$)|(^(\\*\\*\\*)$)"));
		Assert.isTrue(m.getEmail().matches("^[A-Za-z0-9+_.-]+@(.+)$|^(\\*\\*\\*)"));
		Assert.isTrue(m.getPicture().matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"));
	}
}
