
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.AlbumRepository;
import domain.Album;
import domain.Artist;
import domain.Song;

@Service
@Transactional
public class AlbumService {

	// Managed repository ----------------------------------------------------------
	@Autowired
	private AlbumRepository	albumRepository;


	// Constructors ----------------------------------------------------------
	public AlbumService() {
		super();
	}


	// Supporting services ----------------------------------------------------------
	@Autowired
	private ArtistService	artistService;
	@Autowired
	private SongService		songService;


	// Simple CRUD methods ----------------------------------------------------------

	public Album create() {
		this.checkIfArtist();

		final Album album = new Album();
		final Artist artist = this.artistService.findByPrincipal();
		album.setSongs(new ArrayList<Song>());
		album.setDateOfRelease(new Date());
		album.setArtist(artist);

		Assert.notNull(album);
		return album;
	}

	public Collection<Album> findAll() {
		Collection<Album> albums;
		albums = this.albumRepository.findAll();
		Assert.notNull(albums);
		return albums;
	}

	public Album findOne(final int albumId) {
		Assert.isTrue(albumId != 0);
		final Album result = this.albumRepository.findOne(albumId);
		Assert.notNull(result);
		return result;
	}

	public Album save(final Album album) {
		this.checkIfArtist();

		Assert.notNull(album);
		final Album result = this.albumRepository.save(album);
		return result;
	}

	public void delete(final Album album) {
		this.checkIfArtist();

		for (final Song s : this.getAlbumSongs(album))
			this.songService.delete(s);
		Assert.notNull(album);
		this.albumRepository.delete(album);
	}

	// Other methods ----------------------------------------------------------

	// Get songs of an album
	public Collection<Song> getAlbumSongs(final Album a) {
		return this.albumRepository.getAlbumSongs(a.getId());
	}
	// Get songs of my albums
	public Collection<Song> getSongsOfMyAlbums() {
		final Artist artist = this.artistService.findByPrincipal();
		return this.albumRepository.getMyAlbumSongs(artist.getId());
	}

	// Get albums of an artist
	public Collection<Album> getArtistAlbums(final Artist a) {
		return this.albumRepository.getArtistAlbums(a.getId());
	}

	public Collection<Album> findAlbumsByArtist(final Artist artist) {
		Assert.notNull(artist);

		Collection<Album> res = new ArrayList<>();
		res = this.albumRepository.findByArtist(artist);

		return res;
	}

	// Check if artist
	public void checkIfArtist() {
		this.artistService.checkIfArtist();
	}

	// Find actual artist
	public Artist findArtistByPrincipal() {
		return this.artistService.findByPrincipal();
	}

	//Query more songs per album
	public Double maxSongsPerAlbum() {
		return this.albumRepository.maxSongsPerAlbum();
	}

	// Querys -----------------------------------------
	public Collection<Object[]> avgMaxMinAlbumsPerArtist() {
		return this.albumRepository.avgMaxMinAlbumsPerArtist();
	}
}
