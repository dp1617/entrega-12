
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.UserRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Comment;
import domain.Event;
import domain.Playlist;
import domain.Song;
import domain.User;
import forms.ActorForm;

@Service
@Transactional
public class UserService {

	// Managed Repository ----------------------------------------------------------
	@Autowired
	private UserRepository	userRepository;


	// Constructor methods ----------------------------------------------------------
	public UserService() {
		super();
	}


	// Supporting services ----------------------------------------------------------
	@Autowired
	private ActorService	actorService;


	// Simple CRUD methods ------------------------------------------------------

	public User findOne(final int userId) {
		Assert.isTrue(userId != 0);
		User result;

		result = this.userRepository.findOne(userId);
		Assert.notNull(result);

		return result;
	}

	public Collection<User> findAll() {
		Collection<User> result;

		result = this.userRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final User user) {
		Assert.notNull(user);

		this.userRepository.save(user);
	}

	// Other bussines methods ----------------------------------------------------------

	public void checkIfUser() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.USER))
				res = true;
		Assert.isTrue(res);
	}

	public User findByPrincipal() {
		User result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();

		result = this.userRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Collection<User> findUsersImFollowing(final int id) {
		return this.userRepository.findUsersImFollowing(id);
	}

	public User reconstruct(final ActorForm a) {
		final User user = new User();

		final Collection<Playlist> playlists = new ArrayList<>();
		final Collection<Event> events = new ArrayList<>();
		final Collection<Song> favoritesSongs = new ArrayList<>();
		final Collection<Comment> writtenComments = new ArrayList<>();
		final Collection<Actor> actorsImFollowing = new ArrayList<>();
		final Collection<Actor> actorsFollowingMe = new ArrayList<>();

		final UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		user.setId(0);
		user.setVersion(0);
		user.setName(a.getName());
		user.setNickname(a.getNickname());
		user.setEmail(a.getEmail());
		user.setPhone(a.getPhone());
		user.setPicture(a.getPicture());
		//user.setBanned(false);

		user.setUserAccount(account);

		user.setPlaylists(playlists);
		user.setEvents(events);
		user.setFavoritesSongs(favoritesSongs);
		user.setWrittenComments(writtenComments);
		user.setActorsFollowingMe(actorsFollowingMe);
		user.setActorsImFollowing(actorsImFollowing);

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return user;
	}
	public void saveForm(User user) {

		Assert.notNull(user);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("USER");
		auths.add(auth);

		password = user.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		user.getUserAccount().setPassword(password);

		user.getUserAccount().setAuthorities(auths);

		user = this.userRepository.saveAndFlush(user);
	}

	public Collection<User> searchUsersByKeyword(final String keyword) {
		return this.userRepository.searchUsersByKeyword(keyword);
	}

	public Collection<User> actorsPostedMoreSevenPorcentOfComments() {
		return this.userRepository.actorsPostedMoreSevenPorcentOfComments();
	}

	public Collection<User> actorsPostedLessSevenPorcentOfComments() {
		return this.userRepository.actorsPostedLessSevenPorcentOfComments();
	}

	public void checkAttributes(final ActorForm a) {
		final User user = this.reconstruct(a);
		Assert.isTrue(user.getPhone().matches("(^(\\+\\d{1,3})?\\d{9}$)|(^(\\*\\*\\*)$)"));
		Assert.isTrue(user.getEmail().matches("^[A-Za-z0-9+_.-]+@(.+)$|^(\\*\\*\\*)"));
		Assert.isTrue(user.getPicture().matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"));
		for (final Actor actor : this.actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(a.getUsername()));
	}

	public void checkAttributes(final User u) {
		Assert.isTrue(u.getPhone().matches("(^(\\+\\d{1,3})?\\d{9}$)|(^(\\*\\*\\*)$)"));
		Assert.isTrue(u.getEmail().matches("^[A-Za-z0-9+_.-]+@(.+)$|^(\\*\\*\\*)"));
		Assert.isTrue(u.getPicture().matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"));
	}
}
