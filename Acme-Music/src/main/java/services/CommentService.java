
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.CommentRepository;
import domain.Actor;
import domain.Administrator;
import domain.Comment;
import domain.Commentable;
import domain.Song;

@Service
@Transactional
public class CommentService {

	// Managed repository
	@Autowired
	private CommentRepository	commentRepository;


	// Constructors
	public CommentService() {
		super();
	}


	// Supporting services
	@Autowired
	private ActorService	actorService;
	@Autowired
	private SongService		songService;


	// Simple CRUD methods
	public Comment create() {
		this.actorService.checkIfActor();
		Comment comment;
		Actor actor;
		Date now;

		actor = this.actorService.findByPrincipal();
		now = new Date(System.currentTimeMillis() - 1000);
		comment = new Comment();

		comment.setPostedMoment(now);
		comment.setBanned(false);
		comment.setActor(actor);

		return comment;
	}

	public Collection<Comment> findAll() {
		Collection<Comment> comments;
		comments = this.commentRepository.findAll();
		Assert.notNull(comments);
		return comments;
	}

	public Comment findOne(final int commentId) {
		Assert.isTrue(commentId != 0);
		Comment result;
		result = this.commentRepository.findOne(commentId);
		Assert.notNull(result);
		return result;
	}

	public Comment save(final Comment comment) {
		this.actorService.checkIfActor();
		this.checkIfCommentableIsLogued(comment.getCommentable());
		this.checkComment(comment);
		this.checkRangeStars(comment);
		Comment result;
		Assert.notNull(comment);
		result = this.commentRepository.save(comment);
		return result;
	}

	public Comment saveForAdministrator(final Comment comment) {
		this.actorService.checkIfAdministrator();
		Comment result;
		Assert.notNull(comment);
		result = this.commentRepository.save(comment);
		return result;
	}

	public void delete(final Comment comment) {
		this.actorService.checkIfActor();
		Assert.notNull(comment);
		this.commentRepository.delete(comment);
	}

	public Collection<Comment> filterCommentsWithBan(final Collection<Comment> comments) {
		final Actor actor = this.actorService.findByPrincipal();
		final List<Comment> res = new ArrayList<Comment>();
		res.addAll(comments);
		for (int i = 0; i < comments.size(); i++)
			if (res.get(i).getActor().getId() != actor.getId() && res.get(i).getBanned() && !(actor instanceof Administrator))
				comments.remove(res.get(i));
		return comments;
	}

	// Other methods ------------------------------------------

	public void checkIfCommentableIsLogued(final Commentable commentable) {
		Boolean res = true;
		if (commentable.getId() == this.actorService.findByPrincipal().getId())
			res = false;
		Assert.isTrue(res);
	}

	public void checkComment(final Comment c) {
		Boolean res = true;
		if (c.getStars() == null || c.getText() == null || c.getTitle() == null || c.getStars() == null)
			res = false;
		Assert.isTrue(res);
	}

	public void checkRangeStars(final Comment c) {
		Boolean res = true;
		if (c.getStars() < 0 || c.getStars() > 10)
			res = false;
		Assert.isTrue(res);
	}

	// Ban a comment. Only available for administrators
	public void ban(final Comment comment) {
		this.actorService.checkIfAdministrator();
		Assert.isTrue(comment.getBanned() == false);
		comment.setBanned(true);
		this.saveForAdministrator(comment);
	}

	// Get all written comments to actors
	public Collection<Comment> getAllWrittenCommentsToActors() {
		this.actorService.checkIfActor();

		Collection<Actor> actors = new ArrayList<Actor>();
		final Collection<Comment> comments = new ArrayList<>();

		actors = this.actorService.findAll();

		for (final Actor a : actors)
			for (final Comment c : a.getWrittenComments())
				if (c.getCommentable() instanceof Actor)
					comments.add(c);

		return comments;
	}

	// Get written comments of an actor to an song
	public Collection<Comment> getAllWrittenCommentsToSongsByAnActor() {
		this.actorService.checkIfActor();

		Actor actor;
		final Collection<Comment> commentsSongs = new ArrayList<>();

		actor = this.actorService.findByPrincipal();
		for (final Comment c : actor.getWrittenComments())
			if (!(c.getCommentable() instanceof Actor))
				commentsSongs.add(c);

		return commentsSongs;
	}

	// Get written comments of an actor to another actor
	public Collection<Comment> getAllWrittenCommentsToActorByAnActor() {
		this.actorService.checkIfActor();

		Actor actor;
		final Collection<Comment> comments = new ArrayList<>();

		actor = this.actorService.findByPrincipal();
		for (final Comment c : actor.getWrittenComments())
			if (c.getCommentable() instanceof Actor)
				comments.add(c);

		return comments;
	}

	// Check if a commentable is an actor (false) or a song (true)
	public boolean checkCommentable(final Commentable commentable) {
		if (commentable instanceof Actor)
			return false;
		else
			return true;
	}

	// Get all existing commentables
	public Collection<Commentable> getCommentables() {
		Actor actor;
		Collection<Actor> actors = new ArrayList<>();
		Collection<Song> songs = new ArrayList<>();
		final Collection<Commentable> commentables = new ArrayList<>();

		actor = this.actorService.findByPrincipal();
		songs = this.songService.findAll();
		actors = this.actorService.findAll();

		actors.remove(actor);
		commentables.addAll(actors);
		commentables.addAll(songs);

		return commentables;
	}

	//QUERY
	public Double avgCommentsPerUser() {
		return this.commentRepository.avgCommentsPerUser();
	}
}
