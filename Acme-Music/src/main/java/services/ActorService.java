
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Artist;

@Service
@Transactional
public class ActorService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ActorRepository	actorRepository;


	// Constructor methods --------------------------------------------------------------
	public ActorService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Actor findOne(final int actorId) {
		Assert.isTrue(actorId != 0);
		Actor result;

		result = this.actorRepository.findOne(actorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Actor> findAll() {
		Collection<Actor> result;

		result = this.actorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Actor actor) {
		Assert.notNull(actor);

		this.actorRepository.save(actor);
	}

	// Other bussines methods -----------------------------------------------------

	public Collection<Actor> actorsFollowingMe(final Actor a) {
		final Collection<Actor> res = new ArrayList<>();
		for (final Actor at : this.findAll())
			if (at.getActorsImFollowing().contains(a))
				res.add(at);
		return res;
	}

	// Actor follow an actor
	public void follow(final Actor a) {
		Collection<Actor> newArtists = new ArrayList<>();
		Actor actor;

		actor = this.findByPrincipal();

		Assert.isTrue(actor.getId() != a.getId(), "No puedes seguirte a ti mismo");
		Assert.isTrue(!actor.getActorsImFollowing().contains(a), "Ya has seguido a este artista");

		newArtists = actor.getActorsImFollowing();
		newArtists.add(a);

		actor.setActorsImFollowing(newArtists);
		this.save(actor);
	}

	// Actor unfollow an actor
	public void unfollow(final Actor a) {
		Collection<Actor> newArtists = new ArrayList<>();
		Actor actor;

		actor = this.findByPrincipal();

		Assert.isTrue(actor.getId() != a.getId(), "No puedes dejar de seguirte a ti mismo");
		Assert.isTrue(actor.getActorsImFollowing().contains(a), "No sigues a este artista");

		newArtists = actor.getActorsImFollowing();
		newArtists.remove(a);

		actor.setActorsImFollowing(newArtists);
		this.save(actor);
	}

	public void checkIfActor() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ADMIN) || a.getAuthority().equals(Authority.USER) || a.getAuthority().equals(Authority.ARTIST) || a.getAuthority().equals(Authority.MANAGER))
				res = true;
		Assert.isTrue(res);
	}

	public void checkIfAdministrator() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ADMIN))
				res = true;
		Assert.isTrue(res);
	}

	public Actor findByPrincipal() {
		Actor result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.actorRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public boolean authenticated() {
		Boolean res = true;
		SecurityContext context;
		Authentication authentication;

		context = SecurityContextHolder.getContext();
		authentication = context.getAuthentication();
		if (!authentication.isAuthenticated())
			res = false;

		return res;
	}

	public Actor findActorByUsername(final String username) {
		return this.actorRepository.findActorByUsername(username);
	}

	public Collection<Object[]> minMaxAvgAgeOfFollowersPerArtist() {
		return this.actorRepository.minMaxAvgAgeOfFollowersPerArtist();

	}

	public Collection<Artist> artistMoreFollowers() {
		return this.actorRepository.artistMoreFollowers();
	}
}
