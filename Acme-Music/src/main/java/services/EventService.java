
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.EventRepository;
import security.Authority;
import security.LoginService;
import domain.Actor;
import domain.Artist;
import domain.Event;
import domain.EventsCalendar;
import domain.Manager;
import domain.User;

@Service
@Transactional
public class EventService {

	// Managed Repository ----------------------------------------------------------
	@Autowired
	private EventRepository	eventRepository;


	// Constructor methods ----------------------------------------------------------
	public EventService() {
		super();
	}


	// Supporting services ------------------------------------------------------

	@Autowired
	private EventsCalendarService	eventsCalendarService;
	@Autowired
	private UserService				userService;


	// Simple CRUD methods ------------------------------------------------------

	public Event findOne(final int eventId) {
		Assert.isTrue(eventId != 0);
		Event result;

		result = this.eventRepository.findOne(eventId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Event> findAll() {
		Collection<Event> result;

		result = this.eventRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Event event) {
		Assert.notNull(event);

		this.eventRepository.save(event);
	}

	public void delete(final Event event) {
		Assert.notNull(event);

		this.eventRepository.delete(event);
	}

	// Other bussines methods ----------------------------------------------------------

	public Collection<Event> findEventsByArtistsIFollow(final User user) {
		final Collection<Event> res = new ArrayList<Event>();
		final Collection<Event> aux = this.findAll();

		for (final Event e : aux)
			for (final Actor a : user.getActorsImFollowing())
				if (e.getArtist().getId() == a.getId())
					res.add(e);
		return res;
	}

	@SuppressWarnings("deprecation")
	public void setCalendarEvent(final Event e, final Actor a) {
		Collection<EventsCalendar> eventsCalendar = new ArrayList<EventsCalendar>();
		eventsCalendar = this.eventsCalendarService.findAll();
		int id = 0;

		for (final EventsCalendar c : eventsCalendar)
			if (c.getMonthAndYear().getMonth() == e.getStartDate().getMonth() && c.getMonthAndYear().getYear() == e.getStartDate().getYear())
				id = c.getId();

		if (id != 0) {
			final EventsCalendar eventCalendar = this.eventsCalendarService.findOne(id);
			eventCalendar.getEvents().add(e);
			e.setEventsCalendar(eventCalendar);
		} else if (a instanceof Artist) {
			e.setArtist((Artist) a);

			final EventsCalendar eventCalendar = new EventsCalendar();
			eventCalendar.setManager(((Artist) a).getManager());

			final Collection<Event> events = new ArrayList<Event>();
			events.add(e);

			eventCalendar.setEvents(events);

			eventCalendar.setMonthAndYear(e.getStartDate());

			e.setEventsCalendar(eventCalendar);

			this.eventsCalendarService.save(eventCalendar);

		} else if (a instanceof Manager) {
			e.setArtist(((Manager) a).getArtist());

			final EventsCalendar eventCalendar = new EventsCalendar();
			eventCalendar.setManager(((Manager) a));

			final Collection<Event> events = new ArrayList<Event>();
			events.add(e);

			eventCalendar.setEvents(events);

			eventCalendar.setMonthAndYear(e.getStartDate());

			e.setEventsCalendar(eventCalendar);

			this.eventsCalendarService.save(eventCalendar);

		}
	}

	public Collection<Event> getLastEvents() {
		final Collection<Event> events = new ArrayList<Event>();
		final User user = this.userService.findByPrincipal();

		for (final Actor a : this.userService.findUsersImFollowing(user.getId()))
			if (a instanceof Artist) {
				final List<Event> eventsArtist = new ArrayList<Event>();
				eventsArtist.addAll(((Artist) a).getEvents());
				if (eventsArtist.size() != 0)
					events.add(eventsArtist.get(eventsArtist.size() - 1));
			}

		return events;
	}

	public void checkAttributes(final Event event) {
		boolean res = true;

		if (event.getName() == null || event.getStartDate() == null || event.getEndDate() == null || event.getPlace() == null || event.getMaxParticipants() == null)
			res = false;

		Assert.isTrue(res);
	}

	public void checkIfArtistOrManager() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ARTIST))
				res = true;

		if (!res) {
			authority = LoginService.getPrincipal().getAuthorities();
			for (final Authority a : authority)
				if (a.getAuthority().equals(Authority.MANAGER))
					res = true;
		}

		Assert.isTrue(res);
	}

	public void checkEvent(final Event event, final Actor actor) {
		Collection<Event> events = new ArrayList<Event>();
		if (actor instanceof Artist)
			events = ((Artist) actor).getEvents();
		else if (actor instanceof Manager)
			for (final EventsCalendar c : ((Manager) actor).getEventsCalendars())
				events.addAll(c.getEvents());
		Assert.isTrue(events.contains(event));
	}

	public Collection<Event> eventsOrderByUsers() {
		return this.eventRepository.eventsOrderByUsers();
	}
}
