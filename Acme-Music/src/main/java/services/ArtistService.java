
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ArtistRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Album;
import domain.Artist;
import domain.Comment;
import domain.Manager;
import forms.ActorForm;

@Service
@Transactional
public class ArtistService {

	// Managed Repository ----------------------------------------------------------
	@Autowired
	private ArtistRepository	artistRepository;


	// Constructor methods ----------------------------------------------------------
	public ArtistService() {
		super();
	}


	// Supporting services ------------------------------------------------------

	@Autowired
	private ManagerService	managerService;

	private ActorService	actorService;


	// Simple CRUD methods ------------------------------------------------------

	public Artist findOne(final int artistId) {
		Assert.isTrue(artistId != 0);
		Artist result;

		result = this.artistRepository.findOne(artistId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Artist> findAll() {
		Collection<Artist> result;

		result = this.artistRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Artist artist) {
		Assert.notNull(artist);

		this.artistRepository.save(artist);
	}

	// Other bussines methods ----------------------------------------------------------

	// Search artist by keyword (from repository)
	public Collection<Artist> searchArtistByKeyword(final String keyword) {
		return this.artistRepository.searchArtistByKeyword(keyword);
	}

	public void checkIfArtist() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ARTIST))
				res = true;
		Assert.isTrue(res);
	}

	public Artist findByPrincipal() {
		Artist result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();

		result = this.artistRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Artist reconstruct(final ActorForm a) {
		final Artist artist = new Artist();

		final Collection<Album> albums = new ArrayList<>();
		final Collection<Comment> writtenComments = new ArrayList<>();
		final Collection<Actor> actorsImFollowing = new ArrayList<>();
		final Collection<Actor> actorsFollowingMe = new ArrayList<>();

		final UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		artist.setId(0);
		artist.setVersion(0);
		artist.setName(a.getName());
		artist.setNickname(a.getNickname());
		artist.setEmail(a.getEmail());
		artist.setPhone(a.getPhone());
		artist.setPicture(a.getPicture());
		//artist.setBanned(false);

		artist.setUserAccount(account);

		artist.setAlbums(albums);
		artist.setWrittenComments(writtenComments);
		artist.setActorsFollowingMe(actorsFollowingMe);
		artist.setActorsImFollowing(actorsImFollowing);

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return artist;
	}
	public void saveForm(Artist artist) {

		Assert.notNull(artist);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("ARTIST");
		auths.add(auth);

		password = artist.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		artist.getUserAccount().setPassword(password);

		artist.getUserAccount().setAuthorities(auths);

		artist = this.artistRepository.saveAndFlush(artist);
	}

	public Collection<Artist> artistsWithoutManager() {
		final Collection<Artist> res;
		//res = this.findAll();
		res = this.artistRepository.artistsWithoutManager();
		return res;
	}

	public void hireManager(final Artist artist, final Manager manager) {
		Assert.notNull(artist);
		Assert.notNull(manager);

		artist.setManager(manager);
		manager.setArtist(artist);

		this.save(artist);
		this.managerService.save(manager);
	}

	public void fireManager(final Artist artist, final Manager manager) {
		Assert.notNull(artist);
		Assert.notNull(manager);

		artist.setManager(null);
		manager.setArtist(null);

		this.save(artist);
		this.managerService.save(manager);
	}
	public void checkAttributes(final ActorForm a) {
		final Artist artist = this.reconstruct(a);
		Assert.isTrue(artist.getPhone().matches("(^(\\+\\d{1,3})?\\d{9}$)|(^(\\*\\*\\*)$)"));
		Assert.isTrue(artist.getEmail().matches("^[A-Za-z0-9+_.-]+@(.+)$|^(\\*\\*\\*)"));
		Assert.isTrue(artist.getPicture().matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"));
		for (final Actor actor : this.actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(a.getUsername()));
	}

	public void checkAttributes(final Artist a) {
		Assert.isTrue(a.getPhone().matches("(^(\\+\\d{1,3})?\\d{9}$)|(^(\\*\\*\\*)$)"));
		Assert.isTrue(a.getEmail().matches("^[A-Za-z0-9+_.-]+@(.+)$|^(\\*\\*\\*)"));
		Assert.isTrue(a.getPicture().matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"));
	}

}
