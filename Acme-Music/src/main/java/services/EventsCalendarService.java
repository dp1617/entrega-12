
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Event;
import domain.EventsCalendar;
import repositories.EventsCalendarRepository;

@Service
@Transactional
public class EventsCalendarService {

	// Managed Repository ----------------------------------------------------------
	@Autowired
	private EventsCalendarRepository eventsCalendarRepository;


	// Constructor methods ----------------------------------------------------------
	public EventsCalendarService() {
		super();
	}


	// Supporting services ------------------------------------------------------

	@Autowired
	private EventService eventService;


	// Simple CRUD methods ------------------------------------------------------

	public EventsCalendar findOne(final int eventsCalendarId) {
		Assert.isTrue(eventsCalendarId != 0);
		EventsCalendar result;

		result = this.eventsCalendarRepository.findOne(eventsCalendarId);
		Assert.notNull(result);

		return result;
	}

	public Collection<EventsCalendar> findAll() {
		Collection<EventsCalendar> result;

		result = this.eventsCalendarRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final EventsCalendar eventsCalendar) {
		Assert.notNull(eventsCalendar);

		this.eventsCalendarRepository.save(eventsCalendar);
	}

	public void delete(final EventsCalendar eventsCalendar) {
		Assert.notNull(eventsCalendar);

		for (final Event e : eventsCalendar.getEvents())
			this.eventService.delete(e);

		this.eventsCalendarRepository.delete(eventsCalendar);
	}

	// Other bussines methods ----------------------------------------------------------

}
