
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ParametersRepository;
import domain.Parameters;

@Service
@Transactional
public class ParametersService {

	// Managed repository ----------------------------------------------------------
	@Autowired
	private ParametersRepository	parametersRepository;

	@Autowired
	private ActorService			actorService;


	// Constructors ----------------------------------------------------------
	public ParametersService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------------

	public Collection<Parameters> findAll() {
		Collection<Parameters> parameters;
		parameters = this.parametersRepository.findAll();
		Assert.notNull(parameters);
		return parameters;
	}

	public Parameters findOne(final int parametersId) {
		Assert.isTrue(parametersId != 0);
		final Parameters result = this.parametersRepository.findOne(parametersId);
		Assert.notNull(result);
		return result;
	}

	public Parameters save(final Parameters parameters) {
		this.actorService.checkIfAdministrator();

		Assert.notNull(parameters);
		final Parameters result = this.parametersRepository.save(parameters);
		return result;
	}

	public void delete(final Parameters parameters) {
		this.actorService.checkIfAdministrator();

		Assert.notNull(parameters);
		this.parametersRepository.delete(parameters);
	}

	// Other methods ----------------------------------------------------------

	public Parameters getParameters() {
		this.actorService.checkIfAdministrator();
		Parameters res = new Parameters();
		for (final Parameters p : this.findAll()) {
			res = p;
			break;
		}
		return res;
	}

	public void addGenre(final String genre) {
		Collection<Parameters> parameters;
		this.actorService.checkIfAdministrator();
		parameters = this.findAll();

		for (final Parameters p : parameters) {
			Assert.isTrue(p.getGenres().contains(genre) == false);
			p.getGenres().add(genre.toUpperCase());
		}
	}

	public void removeGenre(final String genre) {
		Collection<Parameters> parameters;
		this.actorService.checkIfAdministrator();
		parameters = this.findAll();

		for (final Parameters p : parameters)
			p.getGenres().remove(genre);
	}

	public Collection<String> getGenres() {
		Collection<String> result = null;
		Collection<Parameters> parameters;
		parameters = this.findAll();

		for (final Parameters p : parameters)
			result = p.getGenres();

		return result;
	}

}
