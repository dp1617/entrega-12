
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Album;
import domain.Artist;
import domain.Song;
import domain.User;
import repositories.SongRepository;

@Service
@Transactional
public class SongService {

	// Managed repository ----------------------------------------------------------
	@Autowired
	private SongRepository songRepository;


	// Constructors ----------------------------------------------------------
	public SongService() {
		super();
	}


	// Supporting services ----------------------------------------------------------
	@Autowired
	private ArtistService	artistService;
	@Autowired
	private UserService		userService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private AlbumService	albumService;


	// Simple CRUD methods ----------------------------------------------------------
	public Song create() {
		this.checkIfArtist();

		final Song song = new Song();

		song.setBanned(false);
		song.setExplicit(false);
		song.setAlbum(new Album());
		song.setFavs(0);

		Assert.notNull(song);
		return song;
	}

	public Collection<Song> findAll() {
		Collection<Song> songs;
		songs = this.songRepository.findAll();

		Assert.notNull(songs);
		return songs;
	}

	public Song findOne(final int songId) {
		Assert.isTrue(songId != 0);
		final Song result = this.songRepository.findOne(songId);
		Assert.notNull(result);
		return result;
	}

	public Song save(final Song song) {
		this.checkIfArtist();
		this.checkArtistAuthor(song);

		Assert.notNull(song);
		final Song result = this.songRepository.save(song);
		return result;
	}

	public Song saveCreate(final Song song) {
		this.checkIfArtist();

		Assert.notNull(song);
		final Song result = this.songRepository.save(song);
		return result;
	}

	public Song saveForUser(final Song song) {
		this.userService.checkIfUser();

		Assert.notNull(song);
		final Song result = this.songRepository.save(song);
		return result;
	}

	public Song saveAdmin(final Song song) {
		this.actorService.checkIfAdministrator();

		Assert.notNull(song);
		final Song result = this.songRepository.save(song);
		return result;
	}
	public Song generalSave(final Song song) {
		if (song.getVisualizations() == null)
			song.setVisualizations(0);
		song.setVisualizations(song.getVisualizations() + 1);

		Assert.notNull(song);
		final Song result = this.songRepository.save(song);
		return result;
	}

	public void delete(final Song song) {
		this.checkIfArtist();
		//this.checkArtistAuthor(song);

		for (final User u : this.userService.findAll())
			if (!this.getFavoritesSongs(u).isEmpty())
				for (final Song s : this.getFavoritesSongs(u))
					if (s.equals(song))
						this.deleteFavorite2(s, u);
		Assert.notNull(song);
		this.songRepository.delete(song);
	}

	// Other methods ----------------------------------------------------------

	public Collection<Song> filterPrivate(final Collection<Song> ss) {
		final Collection<Song> res = new ArrayList<>();
		for (final Song s : ss)
			if (!s.getPrivate_())
				res.add(s);
		return res;
	}

	public Collection<Song> filter(final Collection<Song> s) {
		final Collection<Song> res = new ArrayList<>();
		for (final Song sg : s)
			//if (!sg.getBanned())
			res.add(sg);

		if (this.actorService.findByPrincipal() instanceof Artist)
			for (final Album a : this.albumService.getArtistAlbums(this.artistService.findByPrincipal()))
				for (final Song sg : this.albumService.getAlbumSongs(a))
					res.remove(sg);
		return res;
	}

	public Collection<Song> filterBanned(final Collection<Song> s) {
		final Collection<Song> res = new ArrayList<>();
		for (final Song sg : s)
			if (sg.getBanned() == false)
				res.add(sg);

		if (this.actorService.findByPrincipal() instanceof Artist)
			for (final Album a : this.albumService.getArtistAlbums(this.artistService.findByPrincipal()))
				for (final Song sg : this.albumService.getAlbumSongs(a))
					res.remove(sg);
		return res;
	}

	public Collection<Song> filterBannedNotAuth(final Collection<Song> s) {
		final Collection<Song> res = new ArrayList<>();
		for (final Song sg : s)
			if (sg.getBanned() == false)
				res.add(sg);
		return res;
	}

	// Search song by keyword (form repository)
	public Collection<Song> searchSongByKeyword(final String keyword) {
		return this.songRepository.searchSongByKeyword(keyword);
	}

	// Find actual user
	public User findUserByPrincipal() {
		return this.userService.findByPrincipal();
	}

	// Delete actual favorite song from a favorites songs' list of a user
	public void deleteFavorite(final Song s) {
		this.userService.checkIfUser();

		Collection<Song> aux = new ArrayList<>();
		final User user = this.findUserByPrincipal();

		aux = this.getFavoritesSongs(user);

		Assert.isTrue(this.getFavoritesSongs(user).contains(s), "Esta canci�n no est� en tu lista de favoritas");

		aux.remove(s);

		user.setFavoritesSongs(aux);
		this.userService.save(user);
	}

	public void deleteFavorite2(final Song s, final User user) {
		this.artistService.checkIfArtist();

		Collection<Song> aux = new ArrayList<>();

		aux = this.getFavoritesSongs(user);

		aux.remove(s);

		user.setFavoritesSongs(aux);
		this.userService.save(user);
	}

	// Add actual favorite song to a favorites songs' list of a user
	public void addFavorite(final Song s) {
		this.userService.checkIfUser();

		Collection<Song> aux = new ArrayList<>();
		final User user = this.findUserByPrincipal();

		aux = this.getFavoritesSongs(user);

		Assert.isTrue(!this.getFavoritesSongs(user).contains(s), "Esta canci�n ya est� en tu lista de favoritas");

		aux.add(s);
		s.setFavs(s.getFavs() + 1);

		user.setFavoritesSongs(aux);
		this.userService.save(user);
	}

	// Find all favorites songs of a user
	public Collection<Song> getFavoritesSongs(final User u) {
		return this.songRepository.getFavoritesSongs(u.getId());
	}

	public void makeExplicit(final int songId) {
		final Song song = this.findOne(songId);
		this.actorService.checkIfAdministrator();
		Assert.isTrue(song.getExplicit() == false);
		song.setExplicit(true);
	}

	public void makeNotExplicit(final int songId) {
		final Song song = this.findOne(songId);
		this.actorService.checkIfAdministrator();
		Assert.isTrue(song.getExplicit() == true);
		song.setExplicit(false);
	}

	// Check if artist
	public void checkIfArtist() {
		this.artistService.checkIfArtist();
	}
	public Double maxFavsPerSong() {
		return this.songRepository.maxFavsPerSong();
	}

	// Find actual artist
	public Artist findArtistByPrincipal() {
		return this.artistService.findByPrincipal();
	}
	// Check if artist is actual author of a song
	public void checkArtistAuthor(final Song s) {
		Assert.isTrue(s.getAlbum().getArtist().getId() == this.findArtistByPrincipal().getId(), "No eres el autor de esta canci�n");
	}
	public Collection<Song> getTenSongs() {
		final Collection<Song> todas = this.findAll();
		final List<Song> todasList = new ArrayList<Song>(todas);
		final Comparator<Song> comparator = new Comparator<Song>() {

			@Override
			public int compare(final Song s1, final Song s2) {
				return s1.getFavs() + s2.getFavs();
			}
		};
		Collections.sort(todasList, comparator);
		//Integer i = 0;
		final Collection<Song> tenSongs = new ArrayList<Song>();
		//for (final Song s : todasList)
		//for (i = 0; i<; i++)
		//if (!(tenSongs.contains(s)))

		tenSongs.add(todasList.get(0));
		tenSongs.add(todasList.get(1));
		tenSongs.add(todasList.get(2));
		tenSongs.add(todasList.get(3));
		tenSongs.add(todasList.get(4));
		tenSongs.add(todasList.get(5));
		tenSongs.add(todasList.get(6));
		tenSongs.add(todasList.get(7));
		tenSongs.add(todasList.get(8));
		tenSongs.add(todasList.get(9));

		return tenSongs;

	}

	// Querys -------------------------------------------------

	public Collection<Object[]> avgMaxMinSongsPerArtist() {
		return this.songRepository.avgMaxMinSongsPerArtist();
	}

	public Double ratioSongsVAlbums() {
		return this.songRepository.ratioSongsVAlbums();
	}
}
