
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Administrator;
import domain.Comment;
import domain.Song;
import repositories.AdministratorRepository;

@Service
@Transactional
public class AdministratorService {

	// Managed Repository
	@Autowired
	private AdministratorRepository	administratorRepository;

	//Managed Services
	@Autowired
	private ActorService			actorService;
	@Autowired
	private CommentService			commentService;
	@Autowired
	private SongService				songService;


	// Constructor methods
	public AdministratorService() {
		super();
	}
	public Administrator findOne(final int administratorId) {
		Assert.isTrue(administratorId != 0);
		Administrator result;

		result = this.administratorRepository.findOne(administratorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Administrator> findAll() {
		Collection<Administrator> result;

		result = this.administratorRepository.findAll();
		Assert.notNull(result);

		return result;
	}
	// Ban comment
	public void ban(final Comment c) {
		this.actorService.checkIfAdministrator();
		Assert.isTrue(!c.getBanned());
		c.setBanned(true);
		this.commentService.saveForAdministrator(c);
	}

	// Unban comment
	public void unban(final Comment c) {
		this.actorService.checkIfAdministrator();
		//Assert.isTrue(!c.getBanned());
		c.setBanned(false);
		this.commentService.saveForAdministrator(c);
	}

	// Ban song
	public void ban(final Song s) {
		this.actorService.checkIfAdministrator();
		Assert.isTrue(!s.getBanned());
		s.setBanned(true);
		this.songService.saveAdmin(s);
	}

	// Unban song
	public void unban(final Song s) {
		this.actorService.checkIfAdministrator();
		Assert.isTrue(s.getBanned());
		s.setBanned(false);
		this.songService.saveAdmin(s);
	}

}
