
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Album;
import domain.Artist;
import domain.Billboard;
import domain.Manager;
import domain.Song;
import repositories.BillboardRepository;

@Service
@Transactional
public class BillboardService {

	// Managed repository ----------------------------------------------------------
	@Autowired
	private BillboardRepository billboardRepository;


	// Constructors ----------------------------------------------------------
	public BillboardService() {
		super();
	}


	// Supporting services ----------------------------------------------------------
	@Autowired
	private ManagerService	managerService;
	@Autowired
	private AlbumService	albumService;
	@Autowired
	private ActorService	actorService;


	// Simple CRUD methods ----------------------------------------------------------

	public Billboard create() {
		this.managerService.checkIfManager();

		final Billboard billboard = new Billboard();
		billboard.setValidated(false);
		Assert.notNull(billboard);
		return billboard;
	}

	public Billboard save(final Billboard billboard) {
		this.managerService.checkIfManager();

		Assert.notNull(billboard);
		final Billboard result = this.billboardRepository.save(billboard);
		return result;
	}

	public Collection<Billboard> findAll() {
		Collection<Billboard> billboards;
		billboards = this.billboardRepository.findAll();
		Assert.notNull(billboards);
		return billboards;
	}

	public Billboard findOne(final int billboardId) {
		Assert.isTrue(billboardId != 0);
		final Billboard result = this.billboardRepository.findOne(billboardId);
		Assert.notNull(result);
		return result;
	}
	public Billboard showBillboard() {
		final List<Billboard> showBillboards = new ArrayList<Billboard>();
		for (final Billboard b : this.findAll())
			if (b.getValidated() == true)
				showBillboards.add(b);
		final Double random = Math.random() * showBillboards.size();
		final int aux = (int) Math.floor(random);
		final Billboard res = showBillboards.get(aux);

		return res;

	}
	public Collection<Album> getAlbumsManagerArtist() {
		final Manager manager = this.managerService.findByPrincipal();
		final Artist artist = manager.getArtist();
		Collection<Album> res = new ArrayList<Album>();
		final Collection<Album> result = new ArrayList<Album>();
		res = this.albumService.findAlbumsByArtist(artist);
		for (final Album a : res)
			if ((a.getBillboard() == null))
				result.add(a);

		return result;
	}
	public Collection<Song> getSongsManagerArtist() {
		final Manager manager = this.managerService.findByPrincipal();
		final Artist artist = manager.getArtist();
		Collection<Song> res = new ArrayList<Song>();
		for (final Album a : this.albumService.findAlbumsByArtist(artist))
			res = this.albumService.getAlbumSongs(a);

		return res;
	}

	public void deleteBillboard(final int billboardId) {
		final Billboard billboard = this.findOne(billboardId);
		this.actorService.checkIfAdministrator();
		final Album album = billboard.getAlbum();
		album.setBillboard(null);
		billboard.setAlbum(null);
		this.billboardRepository.delete(billboard);
	}

	public void validateBillboard(final int billboardId) {
		final Billboard billboard = this.findOne(billboardId);
		this.actorService.checkIfAdministrator();
		Assert.isTrue(billboard.getValidated() == false);
		billboard.setValidated(true);
	}

	public Collection<Object[]> minMaxAvgAgeOfAlbumsValidatePerArtist() {
		return this.billboardRepository.minMaxAvgAgeOfAlbumsValidatePerArtist();

	}
}
