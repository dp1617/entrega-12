
package forms;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

public class ActorForm {

	// Constructor

	public ActorForm() {
		super();
	}


	// Atributos

	private String	name;
	private String	nickname;
	private String	email;
	private String	phone;
	private String	picture;

	private String	secondPassword;
	private Boolean	checkBox;
	private String	username;
	private String	password;


	// Gets y Sets
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Size(min = 5, max = 32)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Size(min = 5, max = 32)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Size(min = 5, max = 32)
	public String getSecondPassword() {
		return this.secondPassword;
	}

	public void setSecondPassword(final String secondPassword) {
		this.secondPassword = secondPassword;
	}

	public Boolean getCheckBox() {
		return this.checkBox;
	}

	public void setCheckBox(final Boolean checkBox) {
		this.checkBox = checkBox;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotEmpty
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@NotEmpty
	public String getNickname() {
		return this.nickname;
	}

	public void setNickname(final String nickname) {
		this.nickname = nickname;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Email
	@NotEmpty
	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Pattern(regexp = "^([+]\\d{1,3})?[ ]?(\\d{9})")
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPicture() {
		return this.picture;
	}

	public void setPicture(final String picture) {
		this.picture = picture;
	}
}
