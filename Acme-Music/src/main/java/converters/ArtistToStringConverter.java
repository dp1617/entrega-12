
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Artist;

@Component
@Transactional
public class ArtistToStringConverter implements Converter<Artist, String> {

	@Override
	public String convert(final Artist artist) {
		String res;

		if (artist == null)
			res = null;
		else
			res = String.valueOf(artist.getId());

		return res;

	}

}
