
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Parameters;

@Component
@Transactional
public class ParametersToStringConverter implements Converter<Parameters, String> {

	@Override
	public String convert(final Parameters parameters) {
		String res;

		if (parameters == null)
			res = null;
		else
			res = String.valueOf(parameters.getId());

		return res;

	}

}
