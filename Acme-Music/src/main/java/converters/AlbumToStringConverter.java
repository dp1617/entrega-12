
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Album;

@Component
@Transactional
public class AlbumToStringConverter implements Converter<Album, String> {

	@Override
	public String convert(final Album album) {
		String res;

		if (album == null)
			res = null;
		else
			res = String.valueOf(album.getId());

		return res;

	}

}
