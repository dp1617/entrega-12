
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Playlist;

@Component
@Transactional
public class PlaylistToStringConverter implements Converter<Playlist, String> {

	@Override
	public String convert(final Playlist playlist) {
		String res;

		if (playlist == null)
			res = null;
		else
			res = String.valueOf(playlist.getId());

		return res;

	}

}
