
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.PlaylistRepository;
import domain.Playlist;

@Component
@Transactional
public class StringToPlaylistConverter implements Converter<String, Playlist> {

	@Autowired
	PlaylistRepository	playlistRepository;


	@Override
	public Playlist convert(final String text) {
		Playlist res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.playlistRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}

}
