
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.EventsCalendar;
import repositories.EventsCalendarRepository;

@Component
@Transactional
public class StringToEventsCalendarConverter implements Converter<String, EventsCalendar> {

	@Autowired
	EventsCalendarRepository eventsCalendarRepository;


	@Override
	public EventsCalendar convert(final String text) {
		EventsCalendar res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.eventsCalendarRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}
}
