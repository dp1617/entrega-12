
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Billboard;

@Component
@Transactional
public class BillboardToStringConverter implements Converter<Billboard, String> {

	@Override
	public String convert(final Billboard billboard) {
		String res;

		if (billboard == null)
			res = null;
		else
			res = String.valueOf(billboard.getId());

		return res;

	}

}
