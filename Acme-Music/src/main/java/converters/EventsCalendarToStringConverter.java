
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.EventsCalendar;

@Component
@Transactional
public class EventsCalendarToStringConverter implements Converter<EventsCalendar, String> {

	@Override
	public String convert(final EventsCalendar eventsCalendar) {
		String res;

		if (eventsCalendar == null)
			res = null;
		else
			res = String.valueOf(eventsCalendar.getId());

		return res;

	}
}
