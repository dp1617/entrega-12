
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Song;

@Component
@Transactional
public class SongToStringConverter implements Converter<Song, String> {

	@Override
	public String convert(final Song song) {
		String res;

		if (song == null)
			res = null;
		else
			res = String.valueOf(song.getId());

		return res;

	}

}
