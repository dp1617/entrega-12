
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.BillboardRepository;
import domain.Billboard;

@Component
@Transactional
public class StringToBillboardConverter implements Converter<String, Billboard> {

	@Autowired
	BillboardRepository	billboardRepository;


	@Override
	public Billboard convert(final String text) {
		Billboard res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.billboardRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}

}
