/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.user;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.PlaylistService;
import services.UserService;
import controllers.AbstractController;
import domain.Actor;
import domain.Comment;
import domain.Playlist;
import domain.User;
import forms.ActorForm;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {

	// Services

	@Autowired
	private PlaylistService	playlistService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private UserService		userService;


	// Constructors -----------------------------------------------------------

	public UserController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public ModelAndView details(@RequestParam final int playlistId) {
		ModelAndView result;
		Integer auxFollow;
		Playlist playlist;
		User user;
		Collection<Playlist> userPlaylists = new ArrayList<>();
		final Collection<Comment> comments = new ArrayList<>();
		Collection<Comment> aux = new ArrayList<>();

		playlist = this.playlistService.findOne(playlistId);
		user = playlist.getUser();
		userPlaylists = this.playlistService.getMyPlaylists(user);

		aux = user.getReceivedComments();
		for (final Comment c : aux)
			if (!c.getBanned())
				comments.add(c);

		auxFollow = 0;
		try {
			final Actor actor = this.actorService.findByPrincipal();
			if (actor.getActorsImFollowing().contains(user))
				auxFollow = 2;
			else
				auxFollow = 1;
		} catch (final Throwable oops) {
			auxFollow = 0;
		}

		result = new ModelAndView("user/details");
		result.addObject("comments", comments);
		result.addObject("auxFollow", auxFollow);
		result.addObject("user", user);
		result.addObject("playlists", userPlaylists);
		result.addObject("requestURI", "user/details.do");

		return result;
	}

	@RequestMapping(value = "/details1", method = RequestMethod.GET)
	public ModelAndView details1(@RequestParam final int userId) {
		ModelAndView result;
		Integer auxFollow;
		User user;
		Collection<Playlist> userPlaylists = new ArrayList<>();
		Collection<Comment> aux = new ArrayList<>();
		final Collection<Comment> comments = new ArrayList<>();

		user = this.userService.findOne(userId);
		userPlaylists = this.playlistService.getMyPlaylists(user);

		aux = user.getReceivedComments();
		for (final Comment c : aux)
			if (!c.getBanned())
				comments.add(c);

		auxFollow = 0;
		try {
			final Actor actor = this.actorService.findByPrincipal();
			if (actor.getActorsImFollowing().contains(user))
				auxFollow = 2;
			else
				auxFollow = 1;
		} catch (final Throwable oops) {
			auxFollow = 0;
		}

		result = new ModelAndView("user/details");
		result.addObject("comments", comments);
		result.addObject("auxFollow", auxFollow);
		result.addObject("user", user);
		result.addObject("playlists", userPlaylists);
		result.addObject("requestURI", "user/details.do");

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView save() {
		ModelAndView result;
		User user;

		user = this.userService.findByPrincipal();

		result = this.createEditModelAndView(user);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final User user, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createEditModelAndView(user);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.userService.save(user);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(user, "user.commit.error");
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("El formato del tel�fono es incorrecto") || oops.getLocalizedMessage().contains("The format of the phone is incorrect"))
					result.addObject("phoneError", "phoneError");
			}
		return result;
	}

	// (REGISTRO) Creaci�n de un user
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ActorForm actorForm = new ActorForm();

		res = this.createFormModelAndView(actorForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo user
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ActorForm actorForm, final BindingResult binding) {
		ModelAndView res;
		User user;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(actorForm);
			System.out.println(binding.getAllErrors());
		} else
			try {

				user = this.userService.reconstruct(actorForm);
				this.userService.saveForm(user);
				res = new ModelAndView("redirect:/security/login.do");

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(actorForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("El formato del tel�fono es incorrecto") || oops.getLocalizedMessage().contains("The format of the phone is incorrect"))
					res.addObject("phoneError", "phoneError");
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicate", "duplicate");
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions"))
					res.addObject("terms", "terms");
				if (oops.getLocalizedMessage().contains("Passwords do not match"))
					res.addObject("passwordMatch", "pass");
			}

		return res;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<User> allUsers = new ArrayList<>();

		allUsers = this.userService.findAll();

		try {
			final User u = this.userService.findByPrincipal();
			if (u instanceof User)
				allUsers.remove(u);
		} catch (final Throwable oops) {
		}

		result = new ModelAndView("user/all");
		result.addObject("users", allUsers);
		result.addObject("requestURI", "user/all.do");

		return result;
	}
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam final String keyword) {
		ModelAndView result;
		Collection<User> foundUsers = new ArrayList<>();

		foundUsers = this.userService.searchUsersByKeyword(keyword);

		result = new ModelAndView("user/all");
		result.addObject("users", foundUsers);
		result.addObject("requestURI", "user/all.do");

		return result;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final ActorForm actorForm) {
		ModelAndView res;

		res = this.createFormModelAndView(actorForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final ActorForm actorForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("user/create");
		res.addObject("actorForm", actorForm);
		res.addObject("message", message);

		return res;
	}

	//Creacion ModelAndView para el edit
	protected ModelAndView createEditModelAndView(final User user) {
		ModelAndView result;

		result = this.createEditModelAndView(user, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final User user, final String message) {
		ModelAndView result;

		result = new ModelAndView("user/edit");

		result.addObject("user", user);
		result.addObject("message", message);

		return result;
	}
}
