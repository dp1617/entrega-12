
package controllers.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.EventService;
import services.UserService;
import controllers.AbstractController;
import domain.Event;
import domain.User;

@Controller
@RequestMapping("/user/event")
public class UserEventController extends AbstractController {

	// Services

	@Autowired
	private UserService		userService;

	@Autowired
	private EventService	eventService;


	// Constructors -----------------------------------------------------------

	public UserEventController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Listar todos los eventos
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		User user;
		Collection<Event> events = new ArrayList<>();

		events = this.eventService.findAll();
		user = this.userService.findByPrincipal();

		result = new ModelAndView("user/event/all");
		result.addObject("events", events);
		result.addObject("user", user);
		result.addObject("requestURI", "user/event/all.do");

		return result;
	}

	// Listar todos los eventos de los artistas a los que user sigue
	@RequestMapping(value = "/byArtists", method = RequestMethod.GET)
	public ModelAndView iFollow() {
		final ModelAndView result;
		Collection<Event> events = new ArrayList<>();
		final User user = this.userService.findByPrincipal();
		events = this.eventService.findEventsByArtistsIFollow(user);

		result = new ModelAndView("user/event/byArtists");
		result.addObject("events", events);
		result.addObject("requestURI", "user/event/byArtists.do");

		return result;
	}

	// Accept that an user is going to assist to an event
	@RequestMapping(value = "/joinEvent", method = RequestMethod.GET)
	public ModelAndView acepted(@RequestParam final int eventID) {
		ModelAndView result;
		Event event;
		User user;

		event = this.eventService.findOne(eventID);
		user = this.userService.findByPrincipal();

		if (event.getMaxParticipants() > event.getActualParticipants()) {
			//Guardar user en el evento
			final List<User> usersEvent = new ArrayList<User>();
			for (final User u : event.getUsers())
				usersEvent.add(u);
			usersEvent.add(user);
			event.setUsers(usersEvent);
			event.setActualParticipants(event.getActualParticipants() + 1);
			this.eventService.save(event);

			//Guardar evento en el user
			final List<Event> eventsUser = new ArrayList<Event>();
			for (final Event e : user.getEvents())
				eventsUser.add(e);
			eventsUser.add(event);
			user.setEvents(eventsUser);
			this.userService.save(user);

			result = new ModelAndView("redirect:all.do");
		} else
			result = new ModelAndView("redirect:joinError.do");
		return result;
	}

	// Error al intentar unirse a un evento
	@RequestMapping(value = "/joinError", method = RequestMethod.GET)
	public ModelAndView joinError() {
		ModelAndView result;
		result = new ModelAndView("user/event/joinError");
		result.addObject("requestURI", "user/event/joinError.do");
		return result;
	}

	// Listar todas las personas que asistirán a un evento
	@RequestMapping(value = "/joined", method = RequestMethod.GET)
	public ModelAndView joined(@RequestParam final int eventID) {
		final ModelAndView result;
		final Event event = this.eventService.findOne(eventID);
		final Collection<User> users = event.getUsers();

		result = new ModelAndView("user/event/joined");
		result.addObject("users", users);
		result.addObject("event", event);
		result.addObject("requestURI", "user/event/joined.do");

		return result;
	}
}
