/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.user;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SongService;
import services.UserService;
import controllers.AbstractController;
import domain.Song;
import domain.User;

@Controller
@RequestMapping("/song/user")
public class SongUserController extends AbstractController {

	// Services

	@Autowired
	private SongService	songService;
	@Autowired
	private UserService	userService;


	// Constructors -----------------------------------------------------------

	public SongUserController() {
		super();
	}

	// Methods ------------------------------------------------------------------		

	// List all favorites songs of a user
	@RequestMapping(value = "/favorites", method = RequestMethod.GET)
	public ModelAndView listFavorites() {
		ModelAndView result;
		User user;
		Collection<Song> favoriteSongs = new ArrayList<>();

		user = this.userService.findByPrincipal();
		favoriteSongs = this.songService.getFavoritesSongs(user);

		result = new ModelAndView("song/user/favorites");
		result.addObject("songs", favoriteSongs);
		result.addObject("requestURI", "song/user/favorites.do");

		return result;
	}

	// Stop making a song favorite (through the favorite songs' list)
	@RequestMapping(value = "/favorites/delete", method = RequestMethod.GET)
	public ModelAndView deleteFavorite(@RequestParam final int songId) {
		ModelAndView result;
		Song song;

		song = this.songService.findOne(songId);
		this.songService.deleteFavorite(song);

		result = new ModelAndView("redirect:/song/user/favorites.do");
		return result;
	}

	// Add as favorite
	@RequestMapping(value = "/favorites/add", method = RequestMethod.GET)
	public ModelAndView addFavorite(@RequestParam final int songId) {
		ModelAndView result;
		Song song;

		song = this.songService.findOne(songId);
		this.songService.addFavorite(song);

		result = new ModelAndView("redirect:/song/user/favorites.do");
		return result;
	}

}
