/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.user;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.PlaylistService;
import services.SongService;
import services.UserService;
import controllers.AbstractController;
import domain.Playlist;
import domain.Song;
import domain.User;

@Controller
@RequestMapping("/playlist/user")
public class PlaylistUserController extends AbstractController {

	// Services
	@Autowired
	private UserService		userService;
	@Autowired
	private PlaylistService	playlistService;
	@Autowired
	private SongService		songService;


	// Constructors -----------------------------------------------------------

	public PlaylistUserController() {
		super();
	}

	// Methods ------------------------------------------------------------------	

	@RequestMapping(value = "/song/delete", method = RequestMethod.GET)
	public ModelAndView deleteFromPlaylist(@RequestParam final int songId, @RequestParam final int playlistId) {
		ModelAndView result;
		Song song;
		Playlist pl;

		pl = this.playlistService.findOne(playlistId);
		song = this.songService.findOne(songId);
		this.playlistService.deleteSong(pl, song);

		result = new ModelAndView("redirect:/playlist/user/myPlaylists.do");
		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView deleteFromPlaylist(@RequestParam final int playlistId) {
		ModelAndView result;
		Playlist pl;

		pl = this.playlistService.findOne(playlistId);

		this.playlistService.delete(pl);

		result = new ModelAndView("redirect:/playlist/user/myPlaylistsAux.do?playlistId=" + pl.getId());
		return result;
	}

	@RequestMapping(value = "/myPlaylistsAux", method = RequestMethod.GET)
	public ModelAndView deleteAux(@RequestParam final int playlistId) {
		ModelAndView result;
		Playlist pl;

		pl = this.playlistService.findOne(playlistId);

		this.playlistService.delete(pl);

		result = new ModelAndView("redirect:/playlist/user/myPlaylists.do");
		return result;
	}

	@RequestMapping(value = "/myPlaylists", method = RequestMethod.GET)
	public ModelAndView myPlaylists() {
		ModelAndView result;
		User user;
		Collection<Playlist> myPlaylists = new ArrayList<>();

		user = this.userService.findByPrincipal();
		myPlaylists = user.getPlaylists();

		result = new ModelAndView("playlist/user/myPlaylists");
		result.addObject("playlists", myPlaylists);
		result.addObject("requestURI", "playlist/user/myPlaylists.do");

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int playlistId) {
		ModelAndView result;
		Playlist pl;

		pl = this.playlistService.findOne(playlistId);

		result = new ModelAndView("playlist/user/edit");
		result.addObject("playlist", pl);
		result.addObject("requestURI", "playlist/user/edit.do");

		return result;
	}
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Playlist pl, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = new ModelAndView("playlist/user/edit");
			result.addObject("playlist", pl);
			result.addObject("requestURI", "playlist/user/edit.do");
		} else
			try {
				this.playlistService.save(pl);
				result = new ModelAndView("redirect:/playlist/user/myPlaylists.do");
			} catch (final Throwable oops) {
				result = new ModelAndView("playlist/user/edit");
				result.addObject("playlist", pl);
				result.addObject("message", "playlist.commit.error");
				result.addObject("requestURI", "playlist/user/edit.do");
			}

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		Playlist pl;

		pl = this.playlistService.create();

		result = new ModelAndView("playlist/user/create");
		result.addObject("playlist", pl);
		result.addObject("requestURI", "playlist/user/create.do");

		return result;
	}
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Playlist pl, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = new ModelAndView("playlist/user/create");
			result.addObject("playlist", pl);
			result.addObject("requestURI", "playlist/user/create.do");
		} else
			try {
				this.playlistService.save(pl);
				result = new ModelAndView("redirect:/playlist/user/myPlaylists.do");
			} catch (final Throwable oops) {
				result = new ModelAndView("playlist/user/create");
				result.addObject("playlist", pl);
				result.addObject("message", "playlist.commit.error");
				result.addObject("requestURI", "playlist/user/create.do");
			}

		return result;
	}

}
