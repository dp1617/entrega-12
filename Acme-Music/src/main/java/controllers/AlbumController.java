/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AlbumService;
import services.SongService;
import domain.Album;
import domain.Song;

@Controller
@RequestMapping("/album")
public class AlbumController extends AbstractController {

	// Services

	@Autowired
	private AlbumService	albumService;
	@Autowired
	private SongService		songService;


	// Constructors -----------------------------------------------------------

	public AlbumController() {
		super();
	}

	// Methods ------------------------------------------------------------------	

	@RequestMapping(value = "/details1", method = RequestMethod.GET)
	public ModelAndView details1(@RequestParam final int albumId) {
		ModelAndView result;
		Album album;
		Collection<Song> albumSongs = new ArrayList<>();

		album = this.albumService.findOne(albumId);
		albumSongs = this.albumService.getAlbumSongs(album);

		final String dateOfRelease = new SimpleDateFormat("dd/MM/yyyy").format(album.getDateOfRelease());

		result = new ModelAndView("album/details");
		result.addObject("album", album);
		result.addObject("dateOfRelease", dateOfRelease);
		result.addObject("songs", albumSongs);
		result.addObject("requestURI", "album/details.do");

		return result;
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public ModelAndView details(@RequestParam final int songId) {
		ModelAndView result;
		Album album;
		Song song;
		Collection<Song> albumSongs = new ArrayList<>();

		song = this.songService.findOne(songId);
		album = song.getAlbum();
		albumSongs = this.albumService.getAlbumSongs(album);

		final String dateOfRelease = new SimpleDateFormat("dd/MM/yyyy").format(album.getDateOfRelease());

		result = new ModelAndView("album/details");
		result.addObject("album", album);
		result.addObject("dateOfRelease", dateOfRelease);
		result.addObject("songs", albumSongs);
		result.addObject("requestURI", "album/details.do");

		return result;
	}

}
