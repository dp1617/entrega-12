
package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.BillboardService;
import domain.Billboard;

@Controller
@RequestMapping("/billboard/administrator")
public class BillboardAdministratorController {

	// Constructors -----------------------------------------------------------

	public BillboardAdministratorController() {
		super();
	}


	//Services
	@Autowired
	private BillboardService	billboardService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		final Collection<Billboard> billboards = this.billboardService.findAll();

		result = new ModelAndView("billboard/administrator/list");
		result.addObject("billboards", billboards);
		result.addObject("requestURI", "billboard/administrator/list.do");

		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int billboardId) {
		ModelAndView res;

		this.billboardService.deleteBillboard(billboardId);

		res = new ModelAndView("redirect:/billboard/administrator/list.do");

		return res;
	}

	@RequestMapping(value = "/validate", method = RequestMethod.GET)
	public ModelAndView validate(@RequestParam final int billboardId) {
		ModelAndView res;

		this.billboardService.validateBillboard(billboardId);

		res = new ModelAndView("redirect:/billboard/administrator/list.do");

		return res;
	}

}
