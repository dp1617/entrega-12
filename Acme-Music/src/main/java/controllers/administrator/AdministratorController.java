/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AlbumService;
import services.BillboardService;
import services.CommentService;
import services.EventService;
import services.PlaylistService;
import services.SongService;
import services.UserService;
import controllers.AbstractController;
import domain.Artist;
import domain.Event;
import domain.User;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Services -------------------------------------------

	//@Autowired
	//private AdministratorService	administratorService;
	@Autowired
	private AlbumService		albumService;
	@Autowired
	private SongService			songService;
	@Autowired
	private CommentService		commentService;
	@Autowired
	private PlaylistService		playlistService;
	@Autowired
	private UserService			userService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private BillboardService	billboardService;
	@Autowired
	private EventService		eventService;


	// Methods --------------------------------------------

	// Dashboard for administrator

	@RequestMapping("/dashboard")
	public ModelAndView dashboard() {
		final ModelAndView result = new ModelAndView();

		// Queries

		final Collection<Object[]> avgMaxMinAlbumsPerArtist;
		final Collection<Object[]> avgMaxMinSongsPerArtist;
		final Double ratioSongsVAlbums;
		final Double maxSongsPerAlbum;
		final Double maxFavsPerSong;
		final Double avgCommentsPerUser;
		final Double avgNumberSongsPerPlaylist;
		final Collection<User> actorsPostedMoreSevenPorcentOfComments;
		final Collection<User> actorsPostedLessSevenPorcentOfComments;
		final Collection<Object[]> minMaxAvgAgeOfFollowersPerArtist;
		final Collection<Object[]> minMaxAvgAgeOfAlbumsValidatePerArtist;
		final Collection<Artist> artistMoreFollowers;
		final Collection<Event> eventsOrderByUsers;

		// Initiate variables

		avgMaxMinAlbumsPerArtist = this.albumService.avgMaxMinAlbumsPerArtist();
		avgMaxMinSongsPerArtist = this.songService.avgMaxMinSongsPerArtist();
		ratioSongsVAlbums = this.songService.ratioSongsVAlbums();
		maxSongsPerAlbum = this.albumService.maxSongsPerAlbum();
		maxFavsPerSong = this.songService.maxFavsPerSong();
		avgCommentsPerUser = this.commentService.avgCommentsPerUser();
		avgNumberSongsPerPlaylist = this.playlistService.avgNumberSongsPerPlaylist();
		actorsPostedMoreSevenPorcentOfComments = this.userService.actorsPostedMoreSevenPorcentOfComments();
		actorsPostedLessSevenPorcentOfComments = this.userService.actorsPostedLessSevenPorcentOfComments();
		minMaxAvgAgeOfFollowersPerArtist = this.actorService.minMaxAvgAgeOfFollowersPerArtist();
		minMaxAvgAgeOfAlbumsValidatePerArtist = this.billboardService.minMaxAvgAgeOfAlbumsValidatePerArtist();
		artistMoreFollowers = this.actorService.artistMoreFollowers();
		eventsOrderByUsers = this.eventService.eventsOrderByUsers();

		// Adding objects

		result.addObject("avgMaxMinAlbumsPerArtist", avgMaxMinAlbumsPerArtist);
		result.addObject("avgMaxMinSongsPerArtist", avgMaxMinSongsPerArtist);
		result.addObject("ratioSongsVAlbums", ratioSongsVAlbums);
		result.addObject("maxSongsPerAlbum", maxSongsPerAlbum);
		result.addObject("maxFavsPerSong", maxFavsPerSong);
		result.addObject("avgCommentsPerUser", avgCommentsPerUser);
		result.addObject("avgNumberSongsPerPlaylist", avgNumberSongsPerPlaylist);
		result.addObject("actorsPostedMoreSevenPorcentOfComments", actorsPostedMoreSevenPorcentOfComments);
		result.addObject("actorsPostedLessSevenPorcentOfComments", actorsPostedLessSevenPorcentOfComments);
		result.addObject("minMaxAvgAgeOfFollowersPerArtist", minMaxAvgAgeOfFollowersPerArtist);
		result.addObject("minMaxAvgAgeOfAlbumsValidatePerArtist", minMaxAvgAgeOfAlbumsValidatePerArtist);
		result.addObject("artistMoreFollowers", artistMoreFollowers);
		result.addObject("eventsOrderByUsers", eventsOrderByUsers);

		return result;
	}
}
