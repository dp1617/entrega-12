
package controllers.administrator;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ManagerService;
import domain.Manager;
import forms.ActorForm;

@Controller
@RequestMapping("/manager/administrator")
public class ManagerAdministratorController {

	// Services ------------------------------------------------------------------
	@Autowired
	private ManagerService	managerService;


	// Methods ------------------------------------------------------------------

	// (REGISTRO) Creaci�n de un manager
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ActorForm actorForm = new ActorForm();

		res = this.createFormModelAndView(actorForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo artist
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ActorForm actorForm, final BindingResult binding) {
		ModelAndView res;
		Manager manager;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(actorForm);
			System.out.println(binding.getAllErrors());
		} else
			try {

				manager = this.managerService.reconstruct(actorForm);
				this.managerService.saveForm(manager);
				res = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(actorForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("El formato del tel�fono es incorrecto") || oops.getLocalizedMessage().contains("The format of the phone is incorrect"))
					res.addObject("phoneError", "phoneError");
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicate", "duplicate");
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions"))
					res.addObject("terms", "terms");
				if (oops.getLocalizedMessage().contains("Passwords do not match"))
					res.addObject("passwordMatch", "pass");
			}

		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final ActorForm actorForm) {
		ModelAndView res;

		res = this.createFormModelAndView(actorForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final ActorForm actorForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("manager/administrator/create");
		res.addObject("actorForm", actorForm);
		res.addObject("message", message);

		return res;
	}

}
