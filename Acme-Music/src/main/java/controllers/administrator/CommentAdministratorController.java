
package controllers.administrator;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import services.CommentService;
import controllers.AbstractController;
import domain.Comment;

@Controller
@RequestMapping("/comment/administrator")
public class CommentAdministratorController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private AdministratorService	administratorService;
	@Autowired
	private CommentService			commentService;


	// Methods --------------------------------------------

	// List of all written comments to actors, for administrator
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	public ModelAndView listAll() {
		ModelAndView res;
		Collection<Comment> comments = new ArrayList<>();

		comments = this.commentService.getAllWrittenCommentsToActors();

		res = new ModelAndView("comment/administrator/listAll");
		res.addObject("createdComments", comments);
		res.addObject("requestURI", "comment/administrator/listAll.do");

		return res;
	}

	// Ban comment
	@RequestMapping(value = "/ban", method = RequestMethod.GET)
	public ModelAndView ban(@RequestParam final int commentId) {
		ModelAndView res;

		this.administratorService.ban(this.commentService.findOne(commentId));

		res = new ModelAndView("redirect:/comment/administrator/listAll.do");

		return res;
	}

	// Unban comment
	@RequestMapping(value = "/unban", method = RequestMethod.GET)
	public ModelAndView unban(@RequestParam final int commentId) {
		ModelAndView res;

		this.administratorService.unban(this.commentService.findOne(commentId));

		res = new ModelAndView("redirect:/comment/administrator/listAll.do");

		return res;
	}
}
