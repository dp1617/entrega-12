/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ParametersService;
import controllers.AbstractController;
import domain.Parameters;

@Controller
@RequestMapping("/parameters/administrator")
public class ParametersAdministratorController extends AbstractController {

	// Services

	@Autowired
	private ParametersService	parametersService;


	// Constructors -----------------------------------------------------------

	public ParametersAdministratorController() {
		super();
	}

	// Methods ------------------------------------------------------------------	

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		Parameters parameters;

		parameters = this.parametersService.getParameters();

		res = new ModelAndView("parameters/administrator/edit");
		res.addObject("parameters", parameters);

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Parameters parameters, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createEditModelAndView(parameters);
		else
			try {
				this.parametersService.save(parameters);
				res = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				res = this.createEditModelAndView(parameters, "parameters.commit.error");
			}
		return res;
	}
	// Ancillary methods-------------------------------------
	protected ModelAndView createEditModelAndView(final Parameters parameters) {
		ModelAndView res;
		res = this.createEditModelAndView(parameters, null);
		return res;
	}

	protected ModelAndView createEditModelAndView(final Parameters parameters, final String message) {
		ModelAndView res;

		res = new ModelAndView("parameters/administrator/edit");
		res.addObject("parameters", parameters);
		res.addObject("message", message);

		return res;
	}
}
