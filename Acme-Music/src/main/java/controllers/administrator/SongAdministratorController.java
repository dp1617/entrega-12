/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import services.SongService;
import controllers.AbstractController;

@Controller
@RequestMapping("/song")
public class SongAdministratorController extends AbstractController {

	// Services

	@Autowired
	private SongService				songService;
	@Autowired
	private AdministratorService	administratorService;


	// Constructors -----------------------------------------------------------

	public SongAdministratorController() {
		super();
	}

	// Methods ------------------------------------------------------------------	

	@RequestMapping(value = "/administrator/explicit", method = RequestMethod.GET)
	public ModelAndView explicit(@RequestParam final int songId) {
		ModelAndView res;

		this.songService.makeExplicit(songId);

		res = new ModelAndView("redirect:/song/all.do");

		return res;
	}

	@RequestMapping(value = "/administrator/notExplicit", method = RequestMethod.GET)
	public ModelAndView notExplicit(@RequestParam final int songId) {
		ModelAndView res;

		this.songService.makeNotExplicit(songId);

		res = new ModelAndView("redirect:/song/all.do");

		return res;
	}

	// Ban song
	@RequestMapping(value = "/ban", method = RequestMethod.GET)
	public ModelAndView ban(@RequestParam final int songId) {
		ModelAndView res;

		this.administratorService.ban(this.songService.findOne(songId));

		res = new ModelAndView("redirect:/song/all.do");

		return res;
	}

	// Unban song
	@RequestMapping(value = "/unban", method = RequestMethod.GET)
	public ModelAndView unban(@RequestParam final int songId) {
		ModelAndView res;

		this.administratorService.unban(this.songService.findOne(songId));

		res = new ModelAndView("redirect:/song/all.do");

		return res;
	}

}
