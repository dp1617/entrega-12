
package controllers.manager;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.BillboardService;
import domain.Album;
import domain.Billboard;

@Controller
@RequestMapping("/billboard/manager")
public class ManagerBillboardController {

	// Constructors -----------------------------------------------------------

	public ManagerBillboardController() {
		super();
	}


	//Services
	@Autowired
	private BillboardService	billboardService;


	// Creation--------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		final Billboard billboard;
		final Collection<Album> albums = this.billboardService.getAlbumsManagerArtist();
		billboard = this.billboardService.create();
		res = new ModelAndView("billboard/manager/create");
		res.addObject("billboard", billboard);
		res.addObject("albums", albums);
		res.addObject("requestURI", "billboard/manager/create.do");

		return res;
	}
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Billboard billboard, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = new ModelAndView("billboard/manager/create");
			result.addObject("billboard", billboard);
			result.addObject("requestURI", "billboard/manager/create.do");
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.billboardService.save(billboard);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				result = new ModelAndView("billboard/manager/create");
				result.addObject("billboard", billboard);
				result.addObject("message", "billboard.commit.error");
				result.addObject("requestURI", "billboard/manager/create.do");
			}

		return result;
	}

}
