
package controllers.manager;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Comment;
import domain.Manager;
import services.ManagerService;

@Controller
@RequestMapping("/manager")
public class ManagerController {

	// Services

	@Autowired
	private ManagerService	managerService;


	// Constructors -----------------------------------------------------------

	public ManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<Manager> allManagers = new ArrayList<>();

		allManagers = this.managerService.findAll();

		result = new ModelAndView("manager/all");
		result.addObject("managers", allManagers);
		result.addObject("requestURI", "manager/all.do");

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView save() {
		ModelAndView result;
		Manager manager;

		manager = this.managerService.findByPrincipal();

		result = this.createEditModelAndView(manager);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Manager manager, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createEditModelAndView(manager);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.managerService.save(manager);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(manager, "manager.commit.error");
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("El formato del tel�fono es incorrecto") || oops.getLocalizedMessage().contains("The format of the phone is incorrect"))
					result.addObject("phoneError", "phoneError");
			}
		return result;
	}
	
	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public ModelAndView details1(@RequestParam final int managerId) {
		ModelAndView result;
		Manager manager;
		Collection<Comment> aux = new ArrayList<>();
		final Collection<Comment> comments = new ArrayList<>();

		manager = this.managerService.findOne(managerId);

		aux = manager.getReceivedComments();
		for (final Comment c : aux)
			if (!c.getBanned())
				comments.add(c);

		result = new ModelAndView("manager/details");
		result.addObject("manager", manager);
		result.addObject("comments", comments);
		result.addObject("requestURI", "manager/details.do");

		return result;
	}

	//Creacion ModelAndView para el edit
	protected ModelAndView createEditModelAndView(final Manager manager) {
		ModelAndView result;

		result = this.createEditModelAndView(manager, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Manager manager, final String message) {
		ModelAndView result;

		result = new ModelAndView("manager/edit");

		result.addObject("manager", manager);
		result.addObject("message", message);

		return result;
	}
}
