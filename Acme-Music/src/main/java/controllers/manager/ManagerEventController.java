
package controllers.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Event;
import domain.Manager;
import services.ManagerService;

@Controller
@RequestMapping("/manager/event")
public class ManagerEventController {

	// Services

	@Autowired
	private ManagerService managerService;


	// Constructors -----------------------------------------------------------

	public ManagerEventController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<Event> events = new ArrayList<>();
		final Collection<Event> aux = new ArrayList<>();
		final Manager manager = this.managerService.findByPrincipal();

		events = manager.getArtist().getEvents();
		final Date date = new Date();

		for (final Event e : events)
			if (e.getEndDate().after(date))
				aux.add(e);

		result = new ModelAndView("manager/event/all");
		result.addObject("events", events);
		result.addObject("aux", aux);
		result.addObject("requestURI", "manager/event/all.do");

		return result;
	}
}
