
package controllers.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Event;
import domain.EventsCalendar;
import domain.Manager;
import services.EventsCalendarService;
import services.ManagerService;

@Controller
@RequestMapping("/manager/eventsCalendar")
public class ManagerEventCalendarController {

	// Services

	@Autowired
	private ManagerService			managerService;
	@Autowired
	private EventsCalendarService	eventsCalendarService;


	// Constructors -----------------------------------------------------------

	public ManagerEventCalendarController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<EventsCalendar> eventsCalendar = new ArrayList<>();
		final Collection<EventsCalendar> aux = new ArrayList<>();
		final Manager manager = this.managerService.findByPrincipal();

		eventsCalendar = manager.getEventsCalendars();
		final Date date = new Date();

		for (final EventsCalendar e : eventsCalendar)
			if (e.getMonthAndYear().after(date))
				aux.add(e);

		result = new ModelAndView("manager/eventsCalendar/list");
		result.addObject("eventsCalendar", eventsCalendar);
		result.addObject("aux", aux);
		result.addObject("requestURI", "manager/eventsCalendar/list.do");

		return result;
	}

	// Creaci�n de un evento
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final EventsCalendar eventsCalendar = new EventsCalendar();
		final Collection<Event> events = new ArrayList<Event>();

		eventsCalendar.setManager(this.managerService.findByPrincipal());
		eventsCalendar.setEvents(events);

		res = this.createFormModelAndView(eventsCalendar);
		return res;
	}

	// Guardar en la base de datos el nuevo evento
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final EventsCalendar eventsCalendar, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(eventsCalendar);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.eventsCalendarService.save(eventsCalendar);

				res = new ModelAndView("redirect:/manager/eventsCalendar/list.do");

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(eventsCalendar);
				System.out.println(oops.getLocalizedMessage());
			}

		return res;
	}

	// Delete--------------------------------------
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int eventCalendarId) {
		ModelAndView res;
		EventsCalendar eventsCalendar;

		eventsCalendar = this.eventsCalendarService.findOne(eventCalendarId);
		this.eventsCalendarService.delete(eventsCalendar);

		res = new ModelAndView("redirect:/manager/eventsCalendar/list.do");
		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final EventsCalendar eventsCalendar) {
		ModelAndView res;

		res = this.createFormModelAndView(eventsCalendar, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final EventsCalendar eventsCalendar, final String message) {
		ModelAndView res;

		res = new ModelAndView("manager/eventsCalendar/create");
		res.addObject("eventsCalendar", eventsCalendar);
		res.addObject("message", message);

		return res;
	}
}
