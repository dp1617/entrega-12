/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.PlaylistService;
import services.SongService;
import services.UserService;
import domain.Actor;
import domain.Playlist;
import domain.Song;
import domain.User;

@Controller
@RequestMapping("/playlist")
public class PlaylistController extends AbstractController {

	// Services

	@Autowired
	private PlaylistService	playlistService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private UserService		userService;
	@Autowired
	private SongService		songService;


	// Constructors -----------------------------------------------------------

	public PlaylistController() {
		super();
	}

	// Methods ------------------------------------------------------------------	

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<Playlist> allPlaylists = new ArrayList<>();
		Collection<Playlist> res = new ArrayList<>();

		allPlaylists = this.playlistService.findAll();

		try {
			res = this.playlistService.filter(allPlaylists);
		} catch (final Throwable oops) {
			res = this.playlistService.findAll();
		}

		result = new ModelAndView("playlist/all");
		result.addObject("playlists", res);
		result.addObject("requestURI", "playlist/all.do");

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam final String keyword) {
		ModelAndView result;
		Collection<Playlist> foundPlaylists = new ArrayList<>();
		Collection<Playlist> res = new ArrayList<>();

		foundPlaylists = this.playlistService.searchPlaylistByKeyword(keyword);

		try {
			res = this.playlistService.filter(foundPlaylists);
		} catch (final Throwable oops) {
			res = this.playlistService.searchPlaylistByKeyword(keyword);
		}

		result = new ModelAndView("playlist/all");
		result.addObject("playlists", res);
		result.addObject("requestURI", "playlist/all.do");

		return result;
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public ModelAndView details(@RequestParam final int playlistId) {
		ModelAndView result;
		Playlist playlist;
		final Actor actor;
		Integer aux1 = 0;
		Collection<Song> playlistSongs = new ArrayList<>();

		playlist = this.playlistService.findOne(playlistId);
		playlistSongs = this.playlistService.getPlaylistSongs(playlist);

		result = new ModelAndView("playlist/details");
		result.addObject("playlist", playlist);

		try {
			actor = this.actorService.findByPrincipal();
			if (actor instanceof User) {
				final User user = this.userService.findByPrincipal();
				if (user.getId() == playlist.getUser().getId())
					aux1 = 1;
			} else
				aux1 = 0;
		} catch (final Throwable oops) {
			aux1 = 0;
		}
		result.addObject("aux1", aux1);
		result.addObject("songs", this.songService.filterPrivate(playlistSongs));
		result.addObject("requestURI", "playlist/details.do");

		return result;
	}
}
