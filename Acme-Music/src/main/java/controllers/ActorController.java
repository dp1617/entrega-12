/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import domain.Actor;
import domain.Administrator;
import domain.Artist;
import domain.Manager;
import domain.User;

@Controller
@RequestMapping("/actor")
public class ActorController extends AbstractController {

	// Services

	@Autowired
	private ActorService	actorService;


	// Constructors -----------------------------------------------------------

	public ActorController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/unfollow", method = RequestMethod.GET)
	public ModelAndView unfollow(@RequestParam final int actorId) {
		ModelAndView result;
		Actor actor;

		actor = this.actorService.findOne(actorId);
		this.actorService.unfollow(actor);

		result = new ModelAndView("redirect:/actor/following.do");
		return result;
	}

	@RequestMapping(value = "/follow", method = RequestMethod.GET)
	public ModelAndView follow(@RequestParam final int actorId) {
		ModelAndView result;
		Actor actor;

		actor = this.actorService.findOne(actorId);
		this.actorService.follow(actor);

		result = new ModelAndView("redirect:/actor/following.do");
		return result;
	}

	@RequestMapping(value = "/following", method = RequestMethod.GET)
	public ModelAndView following() {
		ModelAndView result;
		Actor actor;
		final Collection<Actor> artistsImFollowing = new ArrayList<>();
		final Collection<Actor> usersImFollowing = new ArrayList<>();
		Collection<Actor> usersAndArtistsImFollowing = new ArrayList<>();

		actor = this.actorService.findByPrincipal();
		usersAndArtistsImFollowing = actor.getActorsImFollowing();

		for (final Actor a : usersAndArtistsImFollowing)
			if (a instanceof User)
				usersImFollowing.add(a);
			else if (a instanceof Artist)
				artistsImFollowing.add(a);

		result = new ModelAndView("actor/following");
		result.addObject("artists", artistsImFollowing);
		result.addObject("users", usersImFollowing);
		result.addObject("requestURI", "actor/following.do");

		return result;
	}

	@RequestMapping(value = "/followingMe", method = RequestMethod.GET)
	public ModelAndView followingMe() {
		ModelAndView result;
		Actor actor;
		final Collection<Actor> artistsFollowingMe = new ArrayList<>();
		final Collection<Actor> usersFollowingMe = new ArrayList<>();
		final Collection<Actor> administratorsFollowingMe = new ArrayList<>();
		final Collection<Actor> managersFollowingMe = new ArrayList<>();
		Collection<Actor> actorsFollowingMe = new ArrayList<>();

		actor = this.actorService.findByPrincipal();
		actorsFollowingMe = this.actorService.actorsFollowingMe(actor);

		Assert.isTrue(actor instanceof User || actor instanceof Artist, "No puedes tener seguidores");

		for (final Actor a : actorsFollowingMe)
			if (a instanceof User)
				usersFollowingMe.add(a);
			else if (a instanceof Artist)
				artistsFollowingMe.add(a);
			else if (a instanceof Administrator)
				administratorsFollowingMe.add(a);
			else if (a instanceof Manager)
				managersFollowingMe.add(a);

		result = new ModelAndView("actor/followingMe");
		result.addObject("artists", artistsFollowingMe);
		result.addObject("users", usersFollowingMe);
		result.addObject("administrators", administratorsFollowingMe);
		result.addObject("managers", managersFollowingMe);
		result.addObject("requestURI", "actor/followingMe.do");

		return result;
	}

	@RequestMapping(value = "/followers", method = RequestMethod.GET)
	public ModelAndView listFollowers(@RequestParam final int actorId) {
		ModelAndView result;
		Actor actor;
		final Collection<Actor> artistsFollowingMe = new ArrayList<>();
		final Collection<Actor> usersFollowingMe = new ArrayList<>();
		final Collection<Actor> administratorsFollowingMe = new ArrayList<>();
		final Collection<Actor> managersFollowingMe = new ArrayList<>();
		Collection<Actor> actorsFollowingMe = new ArrayList<>();

		actor = this.actorService.findOne(actorId);
		actorsFollowingMe = this.actorService.actorsFollowingMe(actor);

		for (final Actor a : actorsFollowingMe)
			if (a instanceof User)
				usersFollowingMe.add(a);
			else if (a instanceof Artist)
				artistsFollowingMe.add(a);
			else if (a instanceof Administrator)
				administratorsFollowingMe.add(a);
			else if (a instanceof Manager)
				managersFollowingMe.add(a);

		result = new ModelAndView("actor/followers");
		result.addObject("artists", artistsFollowingMe);
		result.addObject("users", usersFollowingMe);
		result.addObject("administrators", administratorsFollowingMe);
		result.addObject("managers", managersFollowingMe);
		result.addObject("requestURI", "actor/followingMe.do");

		return result;
	}

}
