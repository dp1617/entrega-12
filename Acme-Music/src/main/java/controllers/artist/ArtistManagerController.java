
package controllers.artist;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Artist;
import domain.Manager;
import services.ArtistService;
import services.ManagerService;

@Controller
@RequestMapping("/artist/manager")
public class ArtistManagerController {

	// Services

	@Autowired
	private ManagerService	managerService;
	@Autowired
	private ArtistService	artistService;


	// Constructors -----------------------------------------------------------

	public ArtistManagerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Hire a manager
	@RequestMapping(value = "/hire", method = RequestMethod.GET)
	public ModelAndView hire(@RequestParam final int managerId) {
		ModelAndView result;
		Artist artist;
		Manager manager;

		artist = this.artistService.findByPrincipal();
		manager = this.managerService.findOne(managerId);

		this.artistService.hireManager(artist, manager);

		Collection<Manager> allManagers = new ArrayList<>();

		allManagers = this.managerService.findAll();

		result = new ModelAndView("manager/all");
		result.addObject("managers", allManagers);
		result.addObject("requestURI", "manager/all.do");

		return result;
	}

	// Fire a manager
	@RequestMapping(value = "/fire", method = RequestMethod.GET)
	public ModelAndView fire(@RequestParam final int managerId) {
		ModelAndView result;
		Artist artist;
		Manager manager;

		artist = this.artistService.findByPrincipal();
		manager = this.managerService.findOne(managerId);

		this.artistService.fireManager(artist, manager);

		Collection<Manager> allManagers = new ArrayList<>();

		allManagers = this.managerService.findAll();

		result = new ModelAndView("manager/all");
		result.addObject("managers", allManagers);
		result.addObject("requestURI", "manager/all.do");

		return result;
	}
}
