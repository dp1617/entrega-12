
package controllers.artist;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AlbumService;
import services.ArtistService;
import services.SongService;
import domain.Album;
import domain.Artist;
import domain.Song;

@Controller
@RequestMapping("/song/artist")
public class ArtistSongController {

	// Constructors -----------------------------------------------------------

	public ArtistSongController() {
		super();
	}


	//Services
	@Autowired
	private AlbumService	albumService;
	@Autowired
	private SongService		songService;
	@Autowired
	private ArtistService	artistService;


	// Methods ------------------------------------------------------------------		

	// Listing --------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView res;
		final Collection<Song> songs = this.albumService.getSongsOfMyAlbums();
		res = new ModelAndView("song/artist/list");
		res.addObject("songs", songs);
		res.addObject("requestURI", "song/artist/list.do");

		return res;
	}
	// Creation--------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		Song song;
		Collection<Album> albums = new ArrayList<Album>();
		final Artist artist = this.artistService.findByPrincipal();
		albums = this.albumService.findAlbumsByArtist(artist);
		song = this.songService.create();
		res = new ModelAndView("song/artist/create");
		res.addObject("song", song);
		res.addObject("albums", albums);
		res.addObject("requestURI", "song/artist/create.do");

		return res;
	}

	// Edition--------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int songID) {
		ModelAndView res;
		Song song;
		Collection<Album> albums = new ArrayList<Album>();
		final Artist artist = this.artistService.findByPrincipal();
		albums = this.albumService.findAlbumsByArtist(artist);
		song = this.songService.findOne(songID);
		Assert.notNull(song);
		//res = this.createEditModelAndView(song);
		res = new ModelAndView("song/artist/edit");
		res.addObject("song", song);
		res.addObject("albums", albums);
		res.addObject("requestURI", "song/artist/edit.do");
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Song song, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = new ModelAndView("song/artist/edit");
			res.addObject("song", song);
			res.addObject("requestURI", "song/user/edit.do");
		} else
			try {
				this.songService.save(song);
				res = new ModelAndView("redirect:/song/artist/list.do");
			} catch (final Throwable oops) {
				res = new ModelAndView("song/artist/edit");
				res.addObject("song", song);
				res.addObject("message", "song.commit.error");
				res.addObject("requestURI", "song/artist/edit.do");
			}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final Song song, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = new ModelAndView("song/artist/edit");
			res.addObject("song", song);
			res.addObject("requestURI", "song/user/edit.do");
		} else
			try {
				this.songService.delete(song);
				res = new ModelAndView("redirect:/song/artist/list.do");
			} catch (final Throwable oops) {
				res = new ModelAndView("song/artist/edit");
				res.addObject("song", song);
				res.addObject("message", "song.commit.error");
				res.addObject("requestURI", "song/artist/edit.do");
			}
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Song song, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = new ModelAndView("song/artist/create");
			result.addObject("song", song);
			result.addObject("requestURI", "song/artist/create.do");
		} else
			try {
				this.songService.saveCreate(song);
				result = new ModelAndView("redirect:/song/artist/list.do");
			} catch (final Throwable oops) {
				result = new ModelAndView("song/artist/create");
				result.addObject("song", song);
				result.addObject("message", "song.commit.error");
				result.addObject("requestURI", "song/artist/create.do");
			}

		return result;
	}

	//	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	//	public ModelAndView delete(@Valid final Song song, final BindingResult binding) {
	//		ModelAndView result;
	//
	//		try {
	//			this.songService.delete(song);
	//			result = new ModelAndView("redirect:list.do");
	//		} catch (final Throwable oops) {
	//			result = this.createEditModelAndView(song, "song.commit.error2");
	//		}
	//		return result;
	//	}

}
