
package controllers.artist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Artist;
import domain.Event;
import services.ArtistService;

@Controller
@RequestMapping("/artist/event")
public class ArtistEventController {

	// Services

	@Autowired
	private ArtistService artistService;


	// Constructors -----------------------------------------------------------

	public ArtistEventController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<Event> events = new ArrayList<>();
		final Collection<Event> aux = new ArrayList<>();
		final Artist artist = this.artistService.findByPrincipal();

		events = artist.getEvents();

		final Date date = new Date();

		for (final Event e : events)
			if (e.getEndDate().after(date))
				aux.add(e);

		result = new ModelAndView("artist/event/all");
		result.addObject("events", events);
		result.addObject("aux", aux);
		result.addObject("requestURI", "artist/event/all.do");

		return result;
	}
}
