
package controllers.artist;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AlbumService;
import services.ArtistService;
import domain.Album;
import domain.Artist;

@Controller
@RequestMapping("/album/artist")
public class ArtistAlbumController {

	// Constructors -----------------------------------------------------------

	public ArtistAlbumController() {
		super();
	}


	//Services
	@Autowired
	private AlbumService	albumService;
	@Autowired
	private ArtistService	artistService;


	// Methods ------------------------------------------------------------------		

	// Listing --------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView res;
		final Artist artist = this.artistService.findByPrincipal();
		final Collection<Album> albums = this.albumService.findAlbumsByArtist(artist);
		res = new ModelAndView("album/artist/list");
		res.addObject("albums", albums);
		res.addObject("requestURI", "album/artist/list.do");

		return res;
	}
	// Creation--------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;
		final Album album;
		album = this.albumService.create();
		res = new ModelAndView("album/artist/create");
		res.addObject("album", album);
		res.addObject("requestURI", "album/artist/create.do");

		return res;
	}

	// Edition--------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int albumID) {
		ModelAndView res;
		Album album;
		album = this.albumService.findOne(albumID);
		Assert.notNull(album);
		res = new ModelAndView("album/artist/edit");
		res.addObject("album", album);
		res.addObject("requestURI", "album/artist/edit.do");
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Album album, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = new ModelAndView("album/artist/edit");
			res.addObject("album", album);
			res.addObject("requestURI", "album/artist/edit.do");
		} else
			try {
				this.albumService.save(album);
				res = new ModelAndView("redirect:/album/artist/list.do");
			} catch (final Throwable oops) {
				res = new ModelAndView("album/artist/edit");
				res.addObject("album", album);
				res.addObject("message", "album.commit.error");
				res.addObject("requestURI", "album/artist/edit.do");
			}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final Album album, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = new ModelAndView("album/artist/edit");
			res.addObject("soalbumng", album);
			res.addObject("requestURI", "album/artist/edit.do");
		} else
			try {
				this.albumService.delete(album);
				System.out.println(binding.getAllErrors());
				res = new ModelAndView("redirect:/album/artist/list.do");
			} catch (final Throwable oops) {
				res = new ModelAndView("album/artist/edit");
				res.addObject("album", album);
				res.addObject("message", "album.commit.error");
				res.addObject("requestURI", "album/artist/edit.do");
			}
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Album album, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = new ModelAndView("album/artist/create");
			result.addObject("album", album);
			result.addObject("requestURI", "album/artist/create.do");

		} else
			try {
				this.albumService.save(album);
				result = new ModelAndView("redirect:/album/artist/list.do");
			} catch (final Throwable oops) {
				result = new ModelAndView("album/artist/create");
				result.addObject("album", album);
				result.addObject("message", "album.commit.error");
				result.addObject("requestURI", "album/artist/create.do");
			}

		return result;
	}
}
