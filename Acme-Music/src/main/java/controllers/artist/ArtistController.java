/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.artist;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AlbumService;
import services.ArtistService;
import controllers.AbstractController;
import domain.Actor;
import domain.Album;
import domain.Artist;
import domain.Comment;
import forms.ActorForm;

@Controller
@RequestMapping("/artist")
public class ArtistController extends AbstractController {

	// Services

	@Autowired
	private AlbumService	albumService;
	@Autowired
	private ArtistService	artistService;
	@Autowired
	private ActorService	actorService;


	// Constructors -----------------------------------------------------------

	public ArtistController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/details1", method = RequestMethod.GET)
	public ModelAndView details1(@RequestParam final int artistId) {
		ModelAndView result;
		Artist artist;
		Integer auxFollow;
		Collection<Album> artistAlbums = new ArrayList<>();
		Collection<Actor> followers = new ArrayList<>();
		Collection<Comment> aux = new ArrayList<>();
		final Collection<Comment> comments = new ArrayList<>();

		artist = this.artistService.findOne(artistId);
		artistAlbums = this.albumService.getArtistAlbums(artist);
		followers = this.actorService.actorsFollowingMe(artist);

		aux = artist.getReceivedComments();
		for (final Comment c : aux)
			if (!c.getBanned())
				comments.add(c);

		auxFollow = 0;
		try {
			final Actor actor = this.actorService.findByPrincipal();
			if (actor.getActorsImFollowing().contains(artist))
				auxFollow = 2;
			else
				auxFollow = 1;
		} catch (final Throwable oops) {
			auxFollow = 0;
		}

		result = new ModelAndView("artist/details");
		result.addObject("comments", comments);
		result.addObject("artist", artist);
		result.addObject("auxFollow", auxFollow);
		result.addObject("albums", artistAlbums);
		result.addObject("followers", followers);
		result.addObject("requestURI", "artist/details.do");

		return result;
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public ModelAndView details(@RequestParam final int albumId) {
		ModelAndView result;
		Integer auxFollow;
		Album album;
		Artist artist;
		Collection<Actor> followers = new ArrayList<>();
		Collection<Album> artistAlbums = new ArrayList<>();
		Collection<Comment> aux = new ArrayList<>();
		final Collection<Comment> comments = new ArrayList<>();

		album = this.albumService.findOne(albumId);
		artist = album.getArtist();
		artistAlbums = this.albumService.getArtistAlbums(artist);
		followers = this.actorService.actorsFollowingMe(artist);

		aux = artist.getReceivedComments();
		for (final Comment c : aux)
			if (!c.getBanned())
				comments.add(c);

		try {
			final Actor actor = this.actorService.findByPrincipal();
			if (actor.getActorsImFollowing().contains(artist))
				auxFollow = 2;
			else
				auxFollow = 1;
		} catch (final Throwable oops) {
			auxFollow = 0;
		}

		result = new ModelAndView("artist/details");
		result.addObject("comments", comments);
		result.addObject("auxFollow", auxFollow);
		result.addObject("artist", artist);
		result.addObject("followers", followers);
		result.addObject("albums", artistAlbums);
		result.addObject("requestURI", "artist/details.do");

		return result;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<Artist> allArtists = new ArrayList<>();

		allArtists = this.artistService.findAll();

		try {
			final Actor a = this.actorService.findByPrincipal();
			if (a instanceof Actor)
				allArtists.remove(a);
		} catch (final Throwable oops) {
		}

		result = new ModelAndView("artist/all");
		result.addObject("artists", allArtists);
		result.addObject("requestURI", "artist/all.do");

		return result;
	}
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam final String keyword) {
		ModelAndView result;
		Collection<Artist> foundArtists = new ArrayList<>();

		foundArtists = this.artistService.searchArtistByKeyword(keyword);

		result = new ModelAndView("artist/all");
		result.addObject("artists", foundArtists);
		result.addObject("requestURI", "artist/all.do");

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView save() {
		ModelAndView result;
		Artist artist;

		artist = this.artistService.findByPrincipal();

		result = this.createEditModelAndView(artist);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Artist artist, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createEditModelAndView(artist);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.artistService.save(artist);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(artist, "artist.commit.error");
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("El formato del tel�fono es incorrecto") || oops.getLocalizedMessage().contains("The format of the phone is incorrect"))
					result.addObject("phoneError", "phoneError");
			}
		return result;
	}

	// (REGISTRO) Creaci�n de un artista
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ActorForm actorForm = new ActorForm();

		res = this.createFormModelAndView(actorForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo artist
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ActorForm actorForm, final BindingResult binding) {
		ModelAndView res;
		Artist artist;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(actorForm);
			System.out.println(binding.getAllErrors());
		} else
			try {

				artist = this.artistService.reconstruct(actorForm);
				this.artistService.saveForm(artist);
				res = new ModelAndView("redirect:/security/login.do");

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(actorForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("El formato del tel�fono es incorrecto") || oops.getLocalizedMessage().contains("The format of the phone is incorrect"))
					res.addObject("phoneError", "phoneError");
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicate", "duplicate");
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions"))
					res.addObject("terms", "terms");
				if (oops.getLocalizedMessage().contains("Passwords do not match"))
					res.addObject("passwordMatch", "pass");
			}

		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final ActorForm actorForm) {
		ModelAndView res;

		res = this.createFormModelAndView(actorForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final ActorForm actorForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("artist/create");
		res.addObject("actorForm", actorForm);
		res.addObject("message", message);

		return res;
	}

	//Creacion ModelAndView para el edit
	protected ModelAndView createEditModelAndView(final Artist artist) {
		ModelAndView result;

		result = this.createEditModelAndView(artist, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Artist artist, final String message) {
		ModelAndView result;

		result = new ModelAndView("artist/edit");

		result.addObject("artist", artist);
		result.addObject("message", message);

		return result;
	}

}
