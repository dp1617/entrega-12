/*
 * WelcomeController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import domain.Billboard;
import domain.Event;
import domain.Song;
import services.BillboardService;
import services.EventService;
import services.SongService;

@Controller
@RequestMapping("/welcome")
public class WelcomeController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public WelcomeController() {
		super();
	}


	//Services
	@Autowired
	private SongService			songService;
	@Autowired
	private BillboardService	billboardService;
	@Autowired
	private EventService		eventService;


	// Index ------------------------------------------------------------------

	@RequestMapping(value = "/index")
	public ModelAndView index() {
		ModelAndView result;
		SimpleDateFormat formatter;
		String moment;
		final Collection<Billboard> billboards = this.billboardService.findAll();
		Billboard billboard = null;
		if (!billboards.isEmpty())
			billboard = this.billboardService.showBillboard();
		Assert.notNull(billboard);

		final Collection<Song> songs = this.songService.getTenSongs();
		formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		moment = formatter.format(new Date());

		result = new ModelAndView("welcome/index");

		try {
			final Collection<Event> events = this.eventService.getLastEvents();
			result.addObject("events", events);
		} catch (final Throwable oops) {
			System.out.println(oops.getMessage());
		}
		result.addObject("moment", moment);
		result.addObject("songs", songs);

		result.addObject("billboard", billboard);

		return result;
	}
}
