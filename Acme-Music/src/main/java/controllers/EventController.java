
package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Actor;
import domain.Artist;
import domain.Event;
import domain.EventsCalendar;
import domain.Manager;
import domain.User;
import services.ActorService;
import services.EventService;
import services.EventsCalendarService;

@Controller
@RequestMapping("/event")
public class EventController {

	// Services

	@Autowired
	private EventService			eventService;
	@Autowired
	private EventsCalendarService	eventsCalendarService;
	@Autowired
	private ActorService			actorService;


	// Constructors -----------------------------------------------------------

	public EventController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// Creaci�n de un evento
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final Event event = new Event();

		if (this.actorService.findByPrincipal() instanceof Artist)
			event.setArtist((Artist) this.actorService.findByPrincipal());
		else if (this.actorService.findByPrincipal() instanceof Manager)
			event.setArtist(((Manager) this.actorService.findByPrincipal()).getArtist());

		final Collection<User> users = new ArrayList<User>();
		event.setUsers(users);
		event.setActualParticipants(0);

		res = this.createFormModelAndView(event);
		return res;
	}

	// Guardar en la base de datos el nuevo evento
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView create(@Valid final Event event, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(event);
			System.out.println(binding.getAllErrors());
		} else
			try {
				final Actor actor = this.actorService.findByPrincipal();
				this.eventService.setCalendarEvent(event, actor);

				if (this.actorService.findByPrincipal() instanceof User)
					res = new ModelAndView("redirect:/user/event/all.do");
				else if (this.actorService.findByPrincipal() instanceof Artist)
					res = new ModelAndView("redirect:/artist/event/all.do");
				else
					res = new ModelAndView("redirect:/manager/event/all.do");

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(event);
				System.out.println(oops.getLocalizedMessage());
			}

		return res;
	}

	// Edition--------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int eventId) {
		ModelAndView res;
		Event event;

		event = this.eventService.findOne(eventId);

		res = new ModelAndView("event/edit");
		res.addObject("event", event);
		res.addObject("requestURI", "event/edit.do");
		return res;
	}

	// Save Edition--------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Event event, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = new ModelAndView("event/edit");
			res.addObject("event", event);
			res.addObject("requestURI", "event/edit.do");
		} else
			try {
				this.eventService.save(event);

				if (this.actorService.findByPrincipal() instanceof User)
					res = new ModelAndView("redirect:/user/event/all.do");
				else if (this.actorService.findByPrincipal() instanceof Artist)
					res = new ModelAndView("redirect:/artist/event/all.do");
				else
					res = new ModelAndView("redirect:/manager/event/all.do");
			} catch (final Throwable oops) {
				res = new ModelAndView("event/edit");
				res.addObject("event", event);
				res.addObject("message", "event.commit.error");
				res.addObject("requestURI", "event/edit.do");
			}
		return res;
	}

	// Delete

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final Event event, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = new ModelAndView("event/edit");
			res.addObject("event", event);
			res.addObject("requestURI", "event/edit.do");
		} else
			try {
				this.eventService.delete(event);

				if (this.actorService.findByPrincipal() instanceof User)
					res = new ModelAndView("redirect:/user/event/all.do");
				else if (this.actorService.findByPrincipal() instanceof Artist)
					res = new ModelAndView("redirect:/artist/event/all.do");
				else
					res = new ModelAndView("redirect:/manager/event/all.do");

			} catch (final Throwable oops) {
				res = new ModelAndView("event/edit");
				res.addObject("event", event);
				res.addObject("message", "event.commit.error");
				res.addObject("requestURI", "event/edit.do");
			}
		return res;
	}

	// List by event calendar

	@RequestMapping(value = "/listByCalendar", method = RequestMethod.GET)
	public ModelAndView listByCalendar(@RequestParam final int eventCalendarId) {
		ModelAndView result;
		final EventsCalendar eventsCalendar = this.eventsCalendarService.findOne(eventCalendarId);
		final Collection<Event> events;
		final Collection<Event> aux = new ArrayList<>();

		events = eventsCalendar.getEvents();
		final Date date = new Date();

		for (final Event e : events)
			if (e.getEndDate().after(date))
				aux.add(e);

		result = new ModelAndView("event/listByCalendar");
		result.addObject("events", events);
		result.addObject("aux", aux);
		result.addObject("requestURI", "event/listByCalendar.do");

		return result;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final Event event) {
		ModelAndView res;

		res = this.createFormModelAndView(event, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final Event event, final String message) {
		ModelAndView res;

		res = new ModelAndView("event/create");
		res.addObject("event", event);
		res.addObject("message", message);

		return res;
	}

}
