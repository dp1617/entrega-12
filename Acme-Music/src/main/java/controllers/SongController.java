/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.PlaylistService;
import services.SongService;
import services.UserService;
import domain.Administrator;
import domain.Playlist;
import domain.Song;
import domain.User;

@Controller
@RequestMapping("/song")
public class SongController extends AbstractController {

	// Services

	@Autowired
	private SongService		songService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private UserService		userService;
	@Autowired
	private PlaylistService	playlistService;


	// Constructors -----------------------------------------------------------

	public SongController() {
		super();
	}

	// Methods ------------------------------------------------------------------	
	@RequestMapping(value = "/details", method = RequestMethod.POST, params = "save")
	public ModelAndView addTo(@Valid final Song song) {
		ModelAndView res;

		try {
			System.out.println(song.getPlaylists());
			this.playlistService.addSongToPlaylist(song);
			res = new ModelAndView("redirect:/playlist/user/myPlaylists.do");
		} catch (final Throwable oops) {
			res = new ModelAndView("redirect:/playlist/user/myPlaylists.do");
			System.out.println(oops);
		}

		return res;
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public ModelAndView details(@RequestParam final int songId) {
		ModelAndView result;
		Integer auxFavorite;
		final Collection<Playlist> aux = new ArrayList<>();
		Collection<Playlist> playlists = new ArrayList<>();
		Song song;

		song = this.songService.findOne(songId);

		try {
			final User user = this.userService.findByPrincipal();
			if (this.songService.getFavoritesSongs(user).contains(song))
				auxFavorite = 1;
			else
				auxFavorite = 2;
			playlists = user.getPlaylists();
			for (final Playlist pl : playlists)
				if (!this.playlistService.getPlaylistSongs(pl).contains(song))
					aux.add(pl);
		} catch (final Throwable oops) {
			auxFavorite = 0;
		}

		this.songService.generalSave(song);

		result = new ModelAndView("song/user/details");

		final String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

		final Pattern compiledPattern = Pattern.compile(pattern);
		if (song.getYouTubeURL() != null) {
			final Matcher matcher = compiledPattern.matcher(song.getYouTubeURL());

			if (matcher.find())
				System.out.println(matcher.group());
			result.addObject("youTubeURL", matcher.group());
		}

		result.addObject("auxFavorite", auxFavorite);
		result.addObject("song", song);
		result.addObject("playlists", aux);
		result.addObject("requestURI", "song/user/details.do");

		return result;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		Collection<Song> allSongs = new ArrayList<>();
		Collection<Song> res = new ArrayList<>();

		allSongs = this.songService.findAll();

		try {
			if (this.actorService.findByPrincipal() instanceof Administrator)
				res = this.songService.filter(allSongs);
			else
				res = this.songService.filterBanned(allSongs);

		} catch (final Throwable oops) {
			res = this.songService.filterBannedNotAuth(allSongs);
		}

		result = new ModelAndView("song/all");
		result.addObject("songs", this.songService.filterPrivate(res));
		result.addObject("requestURI", "song/all.do");

		return result;
	}

	//	@RequestMapping(value = "/allAdmin", method = RequestMethod.GET)
	//	public ModelAndView allAdmin() {
	//		ModelAndView result;
	//		Collection<Song> allSongs = new ArrayList<>();
	//		Collection<Song> res = new ArrayList<>();
	//
	//		allSongs = this.songService.findAll();
	//
	//		try {
	//			res = this.songService.filter(allSongs);
	//
	//		} catch (final Throwable oops) {
	//			res = this.songService.findAll();
	//		}
	//
	//		result = new ModelAndView("song/allAdmin");
	//		result.addObject("songs", this.songService.filterPrivate(res));
	//		result.addObject("requestURI", "song/allAdmin.do");
	//
	//		return result;
	//	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam final String keyword) {
		ModelAndView result;
		Collection<Song> foundSongs = new ArrayList<>();
		Collection<Song> res = new ArrayList<>();

		foundSongs = this.songService.searchSongByKeyword(keyword);

		try {
			res = this.songService.filter(foundSongs);

		} catch (final Throwable oops) {
			res = this.songService.searchSongByKeyword(keyword);
		}

		result = new ModelAndView("song/all");
		result.addObject("songs", this.songService.filterPrivate(res));
		result.addObject("requestURI", "song/all.do");

		return result;
	}

	// Other methods ---------------------------------------------------------

	protected ModelAndView createDetailsModelAndView(final Song song) {
		return this.createDetailsModelAndView(song, null);
	}
	protected ModelAndView createDetailsModelAndView(final Song song, final String message) {
		ModelAndView result;

		result = new ModelAndView("song/user/details");
		result.addObject("song");
		result.addObject("message", message);

		return result;
	}

}
