
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Event extends DomainEntity {

	private String	name;
	private Date	startDate;
	private Date	endDate;
	private String	place;
	private Integer	maxParticipants;
	private Integer	actualParticipants;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getName() {
		return this.name;
	}
	public void setName(final String name) {
		this.name = name;
	}

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getStartDate() {
		return this.startDate;
	}
	public void setStartDate(final Date startDate) {
		this.startDate = startDate;
	}

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getEndDate() {
		return this.endDate;
	}
	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPlace() {
		return this.place;
	}
	public void setPlace(final String place) {
		this.place = place;
	}

	@NotNull
	@Min(1)
	public Integer getMaxParticipants() {
		return this.maxParticipants;
	}
	public void setMaxParticipants(final Integer maxParticipants) {
		this.maxParticipants = maxParticipants;
	}

	@NotNull
	@Min(0)
	public Integer getActualParticipants() {
		return this.actualParticipants;
	}
	public void setActualParticipants(final Integer actualParticipants) {
		this.actualParticipants = actualParticipants;
	}


	// Relationships --------------------------------------------------------

	private EventsCalendar		eventsCalendar;
	private Collection<User>	users;
	private Artist				artist;


	@Valid
	@ManyToOne(optional = false)
	public EventsCalendar getEventsCalendar() {
		return this.eventsCalendar;
	}
	public void setEventsCalendar(final EventsCalendar eventsCalendar) {
		this.eventsCalendar = eventsCalendar;
	}

	@Valid
	@ManyToMany(mappedBy = "events")
	public Collection<User> getUsers() {
		return this.users;
	}
	public void setUsers(final Collection<User> users) {
		this.users = users;
	}

	@Valid
	@ManyToOne(optional = false)
	public Artist getArtist() {
		return this.artist;
	}
	public void setArtist(final Artist artist) {
		this.artist = artist;
	}

}
