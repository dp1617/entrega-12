
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Album extends DomainEntity {

	private String	title;
	private String	picture;
	private Date	dateOfRelease;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}
	public void setTitle(final String title) {
		this.title = title;
	}

	@URL
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPicture() {
		return this.picture;
	}
	public void setPicture(final String picture) {
		this.picture = picture;
	}

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/mm/yyyy")
	public Date getDateOfRelease() {
		return this.dateOfRelease;
	}
	public void setDateOfRelease(final Date dateOfRelease) {
		this.dateOfRelease = dateOfRelease;
	}


	// Relationships ----------------------------------------------------------

	private Artist				artist;
	private Collection<Song>	songs;
	private Billboard			billboard;


	@Valid
	@ManyToOne(optional = false)
	public Artist getArtist() {
		return this.artist;
	}
	public void setArtist(final Artist artist) {
		this.artist = artist;
	}

	@Valid
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "album")
	public Collection<Song> getSongs() {
		return this.songs;
	}
	public void setSongs(final Collection<Song> songs) {
		this.songs = songs;
	}

	@Valid
	@OneToOne(cascade = CascadeType.ALL, optional = false, mappedBy = "album")
	public Billboard getBillboard() {
		return this.billboard;
	}
	public void setBillboard(final Billboard billboard) {
		this.billboard = billboard;
	}

}
