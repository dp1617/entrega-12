
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class EventsCalendar extends DomainEntity {

	private Date monthAndYear;


	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "MM/yyyy")
	public Date getMonthAndYear() {
		return this.monthAndYear;
	}
	public void setMonthAndYear(final Date monthAndYear) {
		this.monthAndYear = monthAndYear;
	}


	// Relationships ----------------------------------------------------

	private Manager				manager;
	private Collection<Event>	events;


	@Valid
	@ManyToOne(optional = false)
	public Manager getManager() {
		return this.manager;
	}
	public void setManager(final Manager manager) {
		this.manager = manager;
	}

	@Valid
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "eventsCalendar")
	public Collection<Event> getEvents() {
		return this.events;
	}
	public void setEvents(final Collection<Event> events) {
		this.events = events;
	}

}
