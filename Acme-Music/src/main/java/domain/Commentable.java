
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Access(AccessType.PROPERTY)
public abstract class Commentable extends DomainEntity {

	// Relationships -----------------------------------------

	private Collection<Comment>	receivedComments;


	@Valid
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "commentable")
	public Collection<Comment> getReceivedComments() {
		return this.receivedComments;
	}
	public void setReceivedComments(final Collection<Comment> receivedComments) {
		this.receivedComments = receivedComments;
	}

}
