
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Manager extends Actor {

	// Relationships -------------------------------------------------------

	private Artist						artist;
	private Collection<EventsCalendar>	eventsCalendars;


	@Valid
	@OneToOne(optional = true)
	public Artist getArtist() {
		return this.artist;
	}
	public void setArtist(final Artist artist) {
		this.artist = artist;
	}

	@Valid
	@OneToMany(mappedBy = "manager")
	public Collection<EventsCalendar> getEventsCalendars() {
		return this.eventsCalendars;
	}
	public void setEventsCalendars(final Collection<EventsCalendar> eventsCalendars) {
		this.eventsCalendars = eventsCalendars;
	}

}
