
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;

import security.UserAccount;

@Entity
@Access(AccessType.PROPERTY)
public abstract class Actor extends Commentable {

	private String	name;
	private String	nickname;
	private String	email;
	private String	phone;
	private String	picture;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getName() {
		return this.name;
	}
	public void setName(final String name) {
		this.name = name;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getNickname() {
		return this.nickname;
	}
	public void setNickname(final String nickname) {
		this.nickname = nickname;
	}

	@NotBlank
	@Email
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getEmail() {
		return this.email;
	}
	public void setEmail(final String email) {
		this.email = email;
	}

	@Pattern(regexp = "^((\\+\\d{1,3})?\\d{9}$)")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPhone() {
		return this.phone;
	}
	public void setPhone(final String phone) {
		this.phone = phone;
	}

	@URL
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPicture() {
		return this.picture;
	}
	public void setPicture(final String picture) {
		this.picture = picture;
	}


	// Relationships -------------------------------------------

	private Collection<Comment>	writtenComments;
	private Collection<Actor>	actorsImFollowing;
	private UserAccount			userAccount;
	private Collection<Actor>	actorsFollowingMe;


	@Valid
	@ManyToMany(mappedBy = "actorsImFollowing")
	public Collection<Actor> getActorsFollowingMe() {
		return this.actorsFollowingMe;
	}
	public void setActorsFollowingMe(final Collection<Actor> actorsFollowingMe) {
		this.actorsFollowingMe = actorsFollowingMe;
	}

	@Valid
	@ManyToMany(cascade = CascadeType.ALL)
	public Collection<Actor> getActorsImFollowing() {
		return this.actorsImFollowing;
	}
	public void setActorsImFollowing(final Collection<Actor> actorsImFollowing) {
		this.actorsImFollowing = actorsImFollowing;
	}

	@Valid
	@OneToMany(mappedBy = "actor")
	public Collection<Comment> getWrittenComments() {
		return this.writtenComments;
	}
	public void setWrittenComments(final Collection<Comment> writtenComments) {
		this.writtenComments = writtenComments;
	}

	@Valid
	@OneToOne(cascade = CascadeType.ALL, optional = false)
	public UserAccount getUserAccount() {
		return this.userAccount;
	}
	public void setUserAccount(final UserAccount userAccount) {
		this.userAccount = userAccount;
	}

}
