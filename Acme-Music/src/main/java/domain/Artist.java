
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Artist extends Actor {

	// Relationships ------------------------------------------------

	private Collection<Album>	albums;
	private Collection<Event>	events;
	private Manager				manager;


	@Valid
	@OneToMany(mappedBy = "artist")
	public Collection<Album> getAlbums() {
		return this.albums;
	}
	public void setAlbums(final Collection<Album> albums) {
		this.albums = albums;
	}

	@Valid
	@OneToMany(mappedBy = "artist")
	public Collection<Event> getEvents() {
		return this.events;
	}
	public void setEvents(final Collection<Event> events) {
		this.events = events;
	}

	@Valid
	@OneToOne(cascade = CascadeType.ALL, optional = true, mappedBy = "artist")
	public Manager getManager() {
		return this.manager;
	}
	public void setManager(final Manager manager) {
		this.manager = manager;
	}

}
