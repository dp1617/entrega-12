
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class Playlist extends DomainEntity {

	private String	title;
	private Integer	totalMinutes;
	private Integer	totalSeconds;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}
	public void setTitle(final String title) {
		this.title = title;
	}

	@NotNull
	@Min(0)
	public Integer getTotalMinutes() {
		return this.totalMinutes;
	}
	public void setTotalMinutes(final Integer totalMinutes) {
		this.totalMinutes = totalMinutes;
	}

	@NotNull
	@Min(0)
	public Integer getTotalSeconds() {
		return this.totalSeconds;
	}
	public void setTotalSeconds(final Integer totalSeconds) {
		this.totalSeconds = totalSeconds;
	}


	// Relationships ------------------------------------------------

	private User				user;
	private Collection<Song>	songs;


	@Valid
	@ManyToOne(optional = true)
	public User getUser() {
		return this.user;
	}
	public void setUser(final User user) {
		this.user = user;
	}

	@Valid
	@ManyToMany
	public Collection<Song> getSongs() {
		return this.songs;
	}
	public void setSongs(final Collection<Song> songs) {
		this.songs = songs;
	}

}
