
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class Billboard extends DomainEntity {

	private String	picture;
	private String	description;
	private Boolean	validated;


	@URL
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getPicture() {
		return this.picture;
	}
	public void setPicture(final String picture) {
		this.picture = picture;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}
	public void setDescription(final String description) {
		this.description = description;
	}

	@NotNull
	public Boolean getValidated() {
		return this.validated;
	}
	public void setValidated(final Boolean validated) {
		this.validated = validated;
	}


	// Relationships -----------------------------------------------

	private Album	album;


	@Valid
	@OneToOne(optional = false)
	public Album getAlbum() {
		return this.album;
	}
	public void setAlbum(final Album album) {
		this.album = album;
	}

}
