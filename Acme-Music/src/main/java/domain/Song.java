
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class Song extends Commentable {

	private String	name;
	private String	genre;
	private Integer	minutes;
	private Integer	seconds;
	private Boolean	explicit;
	private Boolean	banned;
	private String	soundCloudURL;
	private String	youTubeURL;
	private Integer	visualizations;
	private Boolean	private_;
	private Integer	favs;


	@Min(0)
	public Integer getFavs() {
		return this.favs;
	}

	public void setFavs(final Integer favs) {
		this.favs = favs;
	}
	@URL
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSoundCloudURL() {
		return this.soundCloudURL;
	}
	public void setSoundCloudURL(final String soundCloudURL) {
		this.soundCloudURL = soundCloudURL;
	}

	@URL
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getYouTubeURL() {
		return this.youTubeURL;
	}
	public void setYouTubeURL(final String youTubeURL) {
		this.youTubeURL = youTubeURL;
	}

	@Min(0)
	public Integer getVisualizations() {
		return this.visualizations;
	}

	public void setVisualizations(final Integer visualizations) {
		this.visualizations = visualizations;
	}

	@NotNull
	public Boolean getPrivate_() {
		return this.private_;
	}

	public void setPrivate_(final Boolean private_) {
		this.private_ = private_;
	}
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getName() {
		return this.name;
	}
	public void setName(final String name) {
		this.name = name;
	}

	@NotNull
	@Min(0)
	public Integer getMinutes() {
		return this.minutes;
	}
	public void setMinutes(final Integer minutes) {
		this.minutes = minutes;
	}

	@NotNull
	@Min(0)
	public Integer getSeconds() {
		return this.seconds;
	}
	public void setSeconds(final Integer seconds) {
		this.seconds = seconds;
	}
	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getGenre() {
		return this.genre;
	}
	public void setGenre(final String genre) {
		this.genre = genre;
	}

	@NotNull
	public Boolean getExplicit() {
		return this.explicit;
	}
	public void setExplicit(final Boolean explicit) {
		this.explicit = explicit;
	}

	@NotNull
	public Boolean getBanned() {
		return this.banned;
	}
	public void setBanned(final Boolean banned) {
		this.banned = banned;
	}


	// Relationships ----------------------------------------------------

	private Collection<Playlist>	playlists;
	private Album					album;
	private Collection<User>		users;


	@Valid
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "songs", cascade = CascadeType.ALL)
	public Collection<Playlist> getPlaylists() {
		return this.playlists;
	}
	public void setPlaylists(final Collection<Playlist> playlists) {
		this.playlists = playlists;
	}

	@Valid
	@ManyToOne(optional = true)
	public Album getAlbum() {
		return this.album;
	}
	public void setAlbum(final Album album) {
		this.album = album;
	}

	@Valid
	@ManyToMany(mappedBy = "favoritesSongs")
	public Collection<User> getUsers() {
		return this.users;
	}
	public void setUsers(final Collection<User> users) {
		this.users = users;
	}

}
