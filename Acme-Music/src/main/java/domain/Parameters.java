
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Access(AccessType.PROPERTY)
public class Parameters extends DomainEntity {

	private Collection<String>	genres;


	@NotEmpty
	@ElementCollection
	public Collection<String> getGenres() {
		return this.genres;
	}
	public void setGenres(final Collection<String> genres) {
		this.genres = genres;
	}

}
