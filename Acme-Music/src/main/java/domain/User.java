
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class User extends Actor {

	// Relationhips ----------------------------------------------

	private Collection<Playlist>	playlists;
	private Collection<Event>		events;
	private Collection<Song>		favoritesSongs;


	@Valid
	@OneToMany(mappedBy = "user")
	public Collection<Playlist> getPlaylists() {
		return this.playlists;
	}
	public void setPlaylists(final Collection<Playlist> playlists) {
		this.playlists = playlists;
	}

	@Valid
	@ManyToMany
	public Collection<Event> getEvents() {
		return this.events;
	}
	public void setEvents(final Collection<Event> events) {
		this.events = events;
	}

	@Valid
	@ManyToMany
	public Collection<Song> getFavoritesSongs() {
		return this.favoritesSongs;
	}
	public void setFavoritesSongs(final Collection<Song> favoritesSongs) {
		this.favoritesSongs = favoritesSongs;
	}

}
