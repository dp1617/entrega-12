<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if test="${requestURI.contains('all')}">
	<form method="POST" action="song/search.do" id="search" name="search">
		<input type="text" id="keyword" name="keyword" /> <input
			type="submit" value="<spring:message code="song.search"/>" />
	</form>
</jstl:if>

<display:table name="songs" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<jstl:set var="rowColor" value="none" />
	<jstl:if test="${row.explicit == true}">
		<jstl:set var="rowColor" value="background-color:#FE2E2E" />
	</jstl:if>

	<jstl:if
		test="${requestURI.contains('favorites') || requestURI.contains('all') || requestURI.contains('album') || requestURI.contains('listByEvent')}">

		<%-- <display:column title="${editHeader}">
				<input type="button" name="edit"
					value="<spring:message code="song.edit" />"
					onclick="javascript: window.location.replace('song/artist/edit.do?songID=${row.id}');" />
			</display:column> --%>

		<display:column title="${commentsHeader}" sortable="false" style="rowColor">
			<a href="comment/songComments.do?songID=${row.id}"><spring:message code="song.comments"/> </a>
		</display:column>

		<spring:message code="song.name" var="nameHeader" />
		<display:column title="${nameHeader}" sortable="true" style="rowColor">
			<a href="song/details.do?songId=${row.id}"><spring:message
					text="${row.name}" /> </a>
		</display:column>

		<spring:message code="song.duration" var="minutesHeader" />
		<display:column title="${minutesHeader}" style="rowColor"
			sortable="true">${row.minutes}:${row.seconds}</display:column>

		<spring:message code="song.genre" var="genreHeader" />
		<display:column property="genre" title="${genreHeader}"
			sortable="true" style="rowColor" />

		<spring:message code="song.comments" var="commentsHeader" />
		<display:column title="${commentsHeader}" sortable="true"
			style="rowColor">${row.getReceivedComments().size()}</display:column>

		<spring:message code="song.visualizations" var="visualizationsHeader" />
		<display:column property="visualizations"
			title="${visualizationsHeader}" sortable="true" style="rowColor" />

		<security:authorize access="hasRole('ADMIN')">
			<spring:message code="song.explicit" var="explicitHeader" />
			<display:column title="${explicitHeader}">
				<jstl:if test="${row.getExplicit() == false}">
					<acme:button href="song/administrator/explicit.do?songId=${row.id}"
						name="explicit" code="song.explicit" />
				</jstl:if>
			</display:column>

			<spring:message code="song.notExplicit" var="notExplicitHeader" />
			<display:column title="${notExplicitHeader}">
				<jstl:if test="${row.getExplicit() == true}">
					<acme:button
						href="song/administrator/notExplicit.do?songId=${row.id}"
						name="notExplicit" code="song.notExplicit" />
				</jstl:if>
			</display:column>


			<display:column title="${banHeader}" sortable="false">
				<jstl:if test="${row.banned == false}">
					<input type="submit" name="ban"
						value="<spring:message code="song.ban" />"
						onclick="javascript: window.location.replace('song/ban.do?songId=${row.id}');" />
				</jstl:if>
			</display:column>

			<display:column title="${unbanHeader}" sortable="false">
				<jstl:if test="${row.banned == true}">
					<input type="submit" name="ban"
						value="<spring:message code="song.unban" />"
						onclick="javascript: window.location.replace('song/unban.do?songId=${row.id}');" />
				</jstl:if>
			</display:column>
		</security:authorize>

	</jstl:if>

	<jstl:set var="rowColor" value="none" />
	<jstl:if test="${row.explicit == true}">
		<jstl:set var="rowColor" value="background-color:#FE2E2E" />
	</jstl:if>

	<jstl:if test="${requestURI.contains('list')}">

		<display:column title="${editHeader}">
			<input type="button" name="edit"
				value="<spring:message code="song.edit" />"
				onclick="javascript: window.location.replace('song/artist/edit.do?songID=${row.id}');" />
		</display:column>

		<spring:message code="song.name" var="nameHeader" />
		<display:column title="${nameHeader}" sortable="true" style="rowColor">
			<a href="song/details.do?songId=${row.id}"><spring:message
					text="${row.name}" /> </a>
		</display:column>

		<spring:message code="song.duration" var="minutesHeader" />
		<display:column title="${minutesHeader}" style="rowColor"
			sortable="true">${row.minutes}:${row.seconds}</display:column>

		<spring:message code="song.genre" var="genreHeader" />
		<display:column property="genre" title="${genreHeader}"
			sortable="true" style="rowColor" />

		<spring:message code="song.favs" var="favsHeader" />
		<display:column property="favs" title="${favsHeader}" sortable="true"
			style="rowColor" />

		<spring:message code="song.comments" var="commentsHeader" />
		<display:column title="${commentsHeader}" sortable="true"
			style="rowColor">${row.getReceivedComments().size()}</display:column>

		<spring:message code="song.visualizations" var="visualizationsHeader" />
		<display:column property="visualizations"
			title="${visualizationsHeader}" sortable="true" style="rowColor" />


	</jstl:if>

</display:table>

<p style="background-color: #FE2E2E" class="songalert">
	<spring:message code="song.alert" />
</p>
