<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="song/details.do" modelAttribute="song">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="name" />
	<form:hidden path="genre" />
	<form:hidden path="explicit" />
	<form:hidden path="banned" />
	<form:hidden path="minutes" />
	<form:hidden path="seconds" />
	<form:hidden path="private_" />

	<div class="song" style="text-align: center">
		<b><spring:message code="song.name" />:</b> ${song.name}<br> <b><spring:message
				code="song.album.title" />:</b> <a
			href="album/details.do?songId=${song.id}"> ${song.album.title}</a><br>
		<p />
		<jstl:if test="${not empty youTubeURL && song.private_ == false}">
			<div id="contentframe" style="pposition: absolute;">
				<iframe width="420" height="315"
					src="https://www.youtube.com/embed/${youTubeURL}"> </iframe>
			</div>
			<p />
		</jstl:if>

		<b><spring:message code="song.duration" />:</b>
		${song.minutes}:${song.seconds}<br> <b><spring:message
				code="song.genre" />:</b> ${song.genre}<br>
		<p />
		<security:authorize access="isAuthenticated()">
			<jstl:if test="${auxFavorite == 1}">
				<input type="button" name="delete"
					value="<spring:message code="song.favorite.delete" />"
					onclick="javascript: window.location.replace('song/user/favorites/delete.do?songId=${song.id}');" />
			</jstl:if>
			<jstl:if test="${auxFavorite == 2}">
				<input type="button" name="favorite"
					value="<spring:message code="song.favorite.add" />"
					onclick="javascript: window.location.replace('song/user/favorites/add.do?songId=${song.id}');" />
			</jstl:if>
		</security:authorize>

		<security:authorize access="hasRole('USER')">
			<acme:select items="${playlists}" itemLabel="title"
				code="song.addTo.select" path="playlists" id="playlists" />
			<acme:submit name="save" code="song.addTo" />
		</security:authorize>

		<p />
	</div>

	<jstl:if test="${not empty song.soundCloudURL && song.private_ == false}">
		<iframe id="sc-widget" width="100%" height="166"
			src="https://w.soundcloud.com/player/?url=${song.soundCloudURL}"></iframe>
	</jstl:if>

</form:form>