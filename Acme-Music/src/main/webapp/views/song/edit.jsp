<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${requestURI}" modelAttribute="song">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="explicit" />
	<form:hidden path="banned" />
	<form:hidden path="users" />
	<form:hidden path="favs" />
	<form:hidden path="playlists" />
	<form:hidden path="visualizations" />

	<acme:textbox code="song.name" path="name" />
	<acme:textbox code="song.genre" path="genre" />
	<acme:textbox code="song.minutes" path="minutes" />
	<acme:textbox code="song.seconds" path="seconds" />
	<acme:textbox code="song.soundCloudURL" path="soundCloudURL" />
	<acme:textbox code="song.youTubeURL" path="youTubeURL" />
	<acme:textbox code="song.private_" path="private_" placeholder="false, true" />
	
	<acme:select items="${albums}" itemLabel="title"
		code="song.album" path="album" />

	<br />
	
	<acme:submit name="save" code="song.save" />
	<jstl:if test="${song.id != 0}">
		<acme:submit name="delete" code="song.delete" />
	</jstl:if>
	<acme:cancel url="song/artist/list.do" code="song.cancel" />

</form:form>
