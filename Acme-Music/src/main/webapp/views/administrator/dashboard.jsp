<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<!-- QUERIES -->

<fieldset>
	<legend><b><spring:message code="administrator.avgMaxMinAlbumsPerArtist"/></b></legend>
	
	<display:table class="displaytag" name="avgMaxMinAlbumsPerArtist" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="avg" property="[0]"/>
  		<acme:column code="max" property="[1]"/>
  		<acme:column code="min" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.avgMaxMinSongsPerArtist"/></b></legend>
	
	<display:table class="displaytag" name="avgMaxMinSongsPerArtist" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="avg" property="[0]"/>
  		<acme:column code="max" property="[1]"/>
  		<acme:column code="min" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.ratioSongsVAlbums"/></b></legend>
	
	<spring:message code="administrator.ratioSongsVAlbums" /><br>
	<jstl:out value="${ratioSongsVAlbums}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.maxSongsPerAlbum"/></b></legend>
	
	<spring:message code="administrator.maxSongsPerAlbum" /><br>
	<jstl:out value="${maxSongsPerAlbum}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.maxFavsPerSong"/></b></legend>
	
	<spring:message code="administrator.maxFavsPerSong" /><br>
	<jstl:out value="${maxFavsPerSong}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.avgCommentsPerUser"/></b></legend>
	
	<spring:message code="administrator.avgCommentsPerUser" /><br>
	<jstl:out value="${avgCommentsPerUser}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.avgNumberSongsPerPlaylist"/></b></legend>
	
	<spring:message code="administrator.avgNumberSongsPerPlaylist" /><br>
	<jstl:out value="${avgNumberSongsPerPlaylist}"></jstl:out><br>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.actorsPostedMoreSevenPorcentOfComments"/></b></legend>
	
	<display:table name="actorsPostedMoreSevenPorcentOfComments" id="row"
	requestURI="${requestURI}" pagesize="10" class="displaytag">
	
	<jstl:out value="${actorsPostedMoreSevenPorcentOfComments}"></jstl:out>

</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.actorsPostedLessSevenPorcentOfComments"/></b></legend>
	
	<display:table name="actorsPostedLessSevenPorcentOfComments" id="row"
	requestURI="${requestURI}" pagesize="10" class="displaytag">
	
	<jstl:out value="${actorsPostedLessSevenPorcentOfComments}"></jstl:out>

</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.minMaxAvgAgeOfFollowersPerArtist"/></b></legend>
	
	<display:table class="displaytag" name="minMaxAvgAgeOfFollowersPerArtist" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="min" property="[0]"/>
  		<acme:column code="max" property="[1]"/>
  		<acme:column code="avg" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.minMaxAvgAgeOfAlbumsValidatePerArtist"/></b></legend>
	
	<display:table class="displaytag" name="minMaxAvgAgeOfAlbumsValidatePerArtist" requestURI="${requestURI}" id="row" keepStatus="true">
  		<acme:column code="min" property="[0]"/>
  		<acme:column code="max" property="[1]"/>
  		<acme:column code="avg" property="[2]"/>
</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.artistMoreFollowers"/></b></legend>
	
	<display:table name="artistMoreFollowers" id="row"
	requestURI="${requestURI}" pagesize="5" class="displaytag">
	
	<jstl:out value="${artistMoreFollowers}"></jstl:out>

</display:table>
</fieldset>

<fieldset>
	<legend><b><spring:message code="administrator.eventsOrderByUsers"/></b></legend>
	
	<display:table name="eventsOrderByUsers" id="row"
	requestURI="${requestURI}" pagesize="10" class="displaytag">
	
	<jstl:out value="${eventsOrderByUsers}"></jstl:out>

</display:table>
</fieldset>