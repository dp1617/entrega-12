<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if test="${requestURI.contains('following')}">

	<h3>
		<spring:message code="actor.artists" />
	</h3>
	<p />

	<display:table name="artists" id="row" requestURI="${requestURI}"
		pagesize="5" class="displaytag">

		<spring:message code="artist.name" var="nameHeader" />
		<display:column title="${nameHeader}" sortable="true">
			<a href="artist/details1.do?artistId=${row.id}"><spring:message
					text="${row.name}" /> </a>
		</display:column>

		<spring:message code="artist.nickname" var="nicknameHeader" />
		<display:column property="nickname" title="${nicknameHeader}"
			sortable="true" />

		<spring:message code="artist.email" var="emailHeader" />
		<display:column property="email" title="${emailHeader}"
			sortable="true" />

		<spring:message code="artist.phone" var="phoneHeader" />
		<display:column property="phone" title="${phoneHeader}"
			sortable="true" />

	</display:table>

	<p />

	<h3>
		<spring:message code="actor.users" />
	</h3>
	<p />

	<display:table name="users" id="row" requestURI="${requestURI}"
		pagesize="5" class="displaytag">

		<spring:message code="user.name" var="nameHeader" />
		<display:column title="${nameHeader}" sortable="true">
			<a href="user/details1.do?userId=${row.id}"><spring:message
					text="${row.name}" /> </a>
		</display:column>

		<spring:message code="user.nickname" var="nicknameHeader" />
		<display:column property="nickname" title="${nicknameHeader}"
			sortable="true" />

		<spring:message code="user.email" var="emailHeader" />
		<display:column property="email" title="${emailHeader}"
			sortable="true" />

		<spring:message code="user.phone" var="phoneHeader" />
		<display:column property="phone" title="${phoneHeader}"
			sortable="true" />
			
	</display:table>


</jstl:if>

<p />

<jstl:if test="${requestURI.contains('followingMe')}">

	<h3>
		<spring:message code="actor.managers" />
	</h3>
	<p />

	<display:table name="managers" id="row" requestURI="${requestURI}"
		pagesize="5" class="displaytag">

		<spring:message code="actor.name" var="nameHeader" />
		<display:column property="name" title="${nameHeader}" sortable="true" />

		<spring:message code="actor.nickname" var="nicknameHeader" />
		<display:column property="nickname" title="${nicknameHeader}"
			sortable="true" />

		<spring:message code="actor.email" var="emailHeader" />
		<display:column property="email" title="${emailHeader}"
			sortable="true" />

		<spring:message code="actor.phone" var="phoneHeader" />
		<display:column property="phone" title="${phoneHeader}"
			sortable="true" />

	</display:table>

	<p />

	<h3>
		<spring:message code="actor.administrators" />
	</h3>
	<p />

	<display:table name="administrators" id="row"
		requestURI="${requestURI}" pagesize="5" class="displaytag">

		<spring:message code="actor.name" var="nameHeader" />
		<display:column property="name" title="${nameHeader}" sortable="true" />

		<spring:message code="actor.nickname" var="nicknameHeader" />
		<display:column property="nickname" title="${nicknameHeader}"
			sortable="true" />

		<spring:message code="actor.email" var="emailHeader" />
		<display:column property="email" title="${emailHeader}"
			sortable="true" />

		<spring:message code="actor.phone" var="phoneHeader" />
		<display:column property="phone" title="${phoneHeader}"
			sortable="true" />

	</display:table>

</jstl:if>
