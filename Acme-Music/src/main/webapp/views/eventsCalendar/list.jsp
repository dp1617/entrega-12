<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="eventsCalendar" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="eventCalendar.monthAndYear" var="monthAndYearHeader" />
	<display:column property="monthAndYear" format="{0,date,MM/YYYY}" title="${monthAndYearHeader}" sortable="true" />

	<spring:message code="eventCalendar.events" var="eventsHeader" />
	<display:column title="${eventsHeader}" sortable="true">
		<a href="event/listByCalendar.do?eventCalendarId=${row.id}"><spring:message
				code="eventCalendar.see" /> </a>
	</display:column>
	
	<security:authorize access="hasRole('MANAGER')">	
		<display:column>
			<jstl:forEach var="eventsCalendar" items="${aux}">
				<jstl:if test="${eventsCalendar.id==row.id}">
					<a href="manager/eventsCalendar/delete.do?eventCalendarId=${row.id}"><spring:message code="event.delete"/></a>
				</jstl:if>
			</jstl:forEach>
		</display:column>
	</security:authorize>

</display:table>