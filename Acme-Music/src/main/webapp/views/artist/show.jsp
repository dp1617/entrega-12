<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div class="artist" style="text-align: center">

	<jstl:if test="${not empty artist.picture}">
		<img src="${artist.picture}" style="width: 250px; height: 250px;">
		<br>
		<p />
	</jstl:if>
	<b><spring:message code="artist.name" />:</b> ${artist.name}<br>
	<b><spring:message code="artist.nickname" />:</b> ${artist.nickname}<br>
	<b><spring:message code="artist.email" />:</b> ${artist.email}<br>
	<jstl:if test="${not empty artist.phone}">
		<b><spring:message code="artist.phone" />:</b> ${artist.phone}<br>
	</jstl:if>
	<p />
	
	<b><spring:message code="artist.followers" />:</b> ${followers.size()}
	<input type="button" name="followers"
		value="<spring:message code="artist.seeFollowers" />"
		onclick="javascript: window.location.replace('actor/followers.do?actorId=${artist.id}');" />
	
	<security:authorize access="isAuthenticated()">
		
		<jstl:if test="${auxFollow == 1}">
			<br/>
			<br/>
			<input type="button" name="follow"
				value="<spring:message code="artist.follow" />"
				onclick="javascript: window.location.replace('actor/follow.do?actorId=${artist.id}');" />
		</jstl:if>
		<jstl:if test="${auxFollow == 2}">
			<br/>
			<br/>
			<input type="button" name="unfollow"
				value="<spring:message code="artist.unfollow" />"
				onclick="javascript: window.location.replace('actor/unfollow.do?actorId=${artist.id}');" />
		</jstl:if>
	</security:authorize>


</div>

<b><spring:message code="artist.albums" /></b>
<p />

<display:table name="albums" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="album.title" var="titleHeader" />
	<display:column title="${titleHeader}" sortable="true" style="rowColor">
		<a href="album/details1.do?albumId=${row.id}"><spring:message
				text="${row.title}" /> </a>
	</display:column>

	<spring:message code="album.dateOfRelease" var="dateOfReleaseHeader" />
	<display:column property="dateOfRelease" title="${dateOfReleaseHeader}"
		style="rowColor" sortProperty="dateOfRelease"
		format="{0,date,dd/MM/yyyy}" sortable="true" />

</display:table>

<b><spring:message code="artist.comments" /></b>
<p />

<display:table name="comments" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="comment.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />

	<spring:message code="comment.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" />

	<spring:message code="comment.postedMoment" var="postedMomentHeader" />
	<display:column property="postedMoment" title="${postedMomentHeader}"
		style="rowColor" sortProperty="postedMoment"
		format="{0,date,dd/MM/yyyy HH:mm}" sortable="true" />

	<spring:message code="comment.text" var="textHeader" />
	<display:column property="text" title="${textHeader}" />

	<spring:message code="comment.stars" var="starsHeader" />
	<display:column property="stars" title="${starsHeader}" />

</display:table>


