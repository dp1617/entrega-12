<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Editar actor -->

<form:form action="artist/edit.do" modelAttribute="artist">

	<form:hidden path="id"/>
	<form:hidden path="version"/>
	<form:hidden path="userAccount"/>

	<!-- Campos obligatorios a rellenar -->

	<b><spring:message code="artist.PersonalData" /></b>

	<br />

	<acme:textbox code="artist.name" path="name" />

	<acme:textbox code="artist.nickname" path="nickname" />

	<acme:textbox code="artist.email" path="email" />
	
	<acme:textbox code="artist.phone" path="phone" />
	
	<acme:textbox code="artist.picture" path="picture" />

	<br />


	<!-- Acciones -->
	
	<acme:submit name="save" code="artist.save"/>
	
	<acme:cancel url="" code="artist.cancel"/>

</form:form>

<br>

<!-- Errores -->

<jstl:if test="${phoneError != null}">
	<span class="message"><spring:message code="${phoneError}" /></span>
</jstl:if>
