<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div class="user" style="text-align: center">

	<jstl:if test="${not empty user.picture}">
		<img src="${user.picture}" style="width: 250px; height: 250px;">
		<br>
		<p />
	</jstl:if>
	<b><spring:message code="user.name" />:</b> ${user.name}<br> <b><spring:message
			code="user.nickname" />:</b> ${user.nickname}<br> <b><spring:message
			code="user.email" />:</b> ${user.email}<br>
	<jstl:if test="${not empty user.phone}">
		<b><spring:message code="user.phone" />:</b> ${user.phone}<br>
	</jstl:if>
	<p />

	<security:authorize access="isAuthenticated()">
		<jstl:if test="${auxFollow == 1}">
			<input type="button" name="follow"
				value="<spring:message code="user.follow" />"
				onclick="javascript: window.location.replace('actor/follow.do?actorId=${user.id}');" />
		</jstl:if>
		<jstl:if test="${auxFollow == 2}">
			<input type="button" name="unfollow"
				value="<spring:message code="user.unfollow" />"
				onclick="javascript: window.location.replace('actor/unfollow.do?actorId=${user.id}');" />
		</jstl:if>
	</security:authorize>


</div>

<b><spring:message code="user.playlists" /></b>
<p />

<display:table name="playlists" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="playlist.title" var="titleHeader" />
	<display:column title="${titleHeader}" sortable="true">
		<a href="playlist/details.do?playlistId=${row.id}"><spring:message
				text="${row.title}" /> </a>
	</display:column>

	<spring:message code="playlist.totalDuration" var="totalDurationHeader" />
	<display:column title="${totalDurationHeader}" sortable="true">${row.totalMinutes}:${row.totalSeconds}</display:column>



</display:table>

<b><spring:message code="user.comments" /></b>
<p />

<display:table name="comments" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="comment.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />

	<spring:message code="comment.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" />

	<spring:message code="comment.postedMoment" var="postedMomentHeader" />
	<display:column property="postedMoment" title="${postedMomentHeader}"
		style="rowColor" sortProperty="postedMoment"
		format="{0,date,dd/MM/yyyy HH:mm}" sortable="true" />

	<spring:message code="comment.text" var="textHeader" />
	<display:column property="text" title="${textHeader}" />

	<spring:message code="comment.stars" var="starsHeader" />
	<display:column property="stars" title="${starsHeader}" />

</display:table>

