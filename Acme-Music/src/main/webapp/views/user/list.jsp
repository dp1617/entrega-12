<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if test="${requestURI.contains('all')}">
	<form method="POST" action="user/search.do" id="search" name="search">
		<input type="text" id="keyword" name="keyword" /> <input
			type="submit" value="<spring:message code="user.search"/>" />
	</form>
</jstl:if>

<display:table name="users" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<jstl:if test="${requestURI.contains('all')}">

		<spring:message code="user.name" var="nameHeader" />
		<display:column title="${nameHeader}" sortable="true">
			<a href="user/details1.do?userId=${row.id}"><spring:message
					text="${row.name}" /> </a>
		</display:column>

		<spring:message code="user.nickname" var="nicknameHeader" />
		<display:column property="nickname" title="${nicknameHeader}"
			sortable="true" />

		<spring:message code="user.email" var="emailHeader" />
		<display:column property="email" title="${emailHeader}"
			sortable="true" />

		<spring:message code="user.phone" var="phoneHeader" />
		<display:column property="phone" title="${phoneHeader}"
			sortable="true" />

	</jstl:if>

</display:table>
