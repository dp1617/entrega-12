<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Creaci�n de user (como usuario no autentificado) -->

<form:form action="user/create.do" modelAttribute="actorForm">

	<!-- Campos obligatorios a rellenar -->

	<b><spring:message code="user.PersonalData" /></b>

	<br />

	<acme:textbox code="user.name" path="name" />

	<acme:textbox code="user.nickname" path="nickname" />

	<acme:textbox code="user.email" path="email" />
	
	<acme:textbox code="user.phone" path="phone" placeholder="+XXYYYYYYYYY"/>
	
	<acme:textbox code="user.picture" path="picture" placeholder="+URL"/>

	<br />

	<!-- Usuario y contrase�a -->

	<b><spring:message code="user.LoginData" /></b>

	<br />

	<acme:textbox code="user.username" path="username" />

	<acme:password code="user.password" path="password" />

	<acme:password code="user.secondPassword" path="secondPassword" />

	<br />

	<!-- Aceptar para continuar -->

	<form:label path="checkBox">
		<spring:message code="user.checkBox" />
	</form:label>
	<form:checkbox path="checkBox" />
	<a href="misc/terms.do"> <spring:message
			code="user.moreInfo" />
	</a>
	<form:errors class="error" path="checkBox" />

	<br />
	<br />

	<!-- Acciones -->
	
	<acme:submit name="save" code="user.signIn"/>
	
	<acme:cancel url="" code="user.cancel"/>

</form:form>

<br>

<!-- Errores -->

<jstl:if test="${phoneError != null}">
	<span class="message"><spring:message code="${phoneError}" /></span>
</jstl:if>

<jstl:if test="${passwordMatch != null}">
	<span class="message"><spring:message code="${pass}" /></span>
</jstl:if>

<jstl:if test="${duplicate != null}">
	<span class="message"><spring:message code="${duplicate}" /></span>
</jstl:if>

<jstl:if test="${terms != null}">
	<span class="message"><spring:message code="${terms}" /></span>
</jstl:if>
