<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="albums" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<jstl:if
		test="${requestURI.contains('list')}">

		<display:column title="${editHeader}">
				<input type="button" name="edit"
					value="<spring:message code="album.edit" />"
					onclick="javascript: window.location.replace('album/artist/edit.do?albumID=${row.id}');" />
			</display:column>
		
		<spring:message code="album.artist.name" var="nameHeader" />
		<display:column property="artist.name" title="${nameHeader}" sortable="true" style="rowColor">
		</display:column>
		
		<spring:message code="album.title" var="titleHeader" />
		<display:column property="title" title="${titleHeader}" sortable="true" style="rowColor">
		</display:column>

		<fmt:formatDate var="dateOfRelease" value="${row.dateOfRelease}"
			pattern="dd/MM/yyyy HH:mm" />
		<spring:message code="album.dateOfRelease" var="dateOfReleaseHeader" />
		<display:column value="${dateOfRelease}" title="${dateOfReleaseHeader}"
			sortable="true" />
		
		
		<spring:message code="album.picture" var="pictureHeader" />
			<display:column title="${pictureHeader}" sortable="false">
				<IMG src="${row.picture}" width="100" height="100">
			</display:column>
			

	</jstl:if>

</display:table>


