<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div class="album" style="text-align: center">
	<b><spring:message code="album.title" />:</b> ${album.title}<br>
	<p />
	<b><spring:message code="album.artist.name" />:</b> <a
		href="artist/details.do?albumId=${album.id}"> ${album.artist.name}</a><br>
	<p />
	<img src="${album.picture}" style="width: 250px; height: 250px;"><br>
	<p />
	<b><spring:message code="album.dateOfRelease" />:</b> ${dateOfRelease}<br>
</div>

<b><spring:message code="album.songs" /></b>
<p />

<display:table name="songs" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<jstl:set var="rowColor" value="none" />
	<jstl:if test="${row.explicit == true}">
		<jstl:set var="rowColor" value="background-color:#FE2E2E" />
	</jstl:if>

	<spring:message code="song.name" var="nameHeader" />
	<display:column title="${nameHeader}" sortable="true" style="rowColor">
		<a href="song/details.do?songId=${row.id}"><spring:message
				text="${row.name}" /> </a>
	</display:column>

	<spring:message code="song.duration" var="minutesHeader" />
	<display:column title="${minutesHeader}" style="rowColor"
		sortable="true">${row.minutes}:${row.seconds}</display:column>

	<spring:message code="song.genre" var="genreHeader" />
	<display:column property="genre" title="${genreHeader}" sortable="true"
		style="rowColor" />

	<spring:message code="song.comments" var="commentsHeader" />
	<display:column title="${commentsHeader}" sortable="true"
		style="rowColor">${row.getReceivedComments().size()}</display:column>

	<spring:message code="song.visualizations" var="visualizationsHeader" />
	<display:column property="visualizations"
		title="${visualizationsHeader}" sortable="true" style="rowColor" />

</display:table>

<p style="background-color: #FE2E2E" class="songalert">
	<spring:message code="song.alert" />
</p>
