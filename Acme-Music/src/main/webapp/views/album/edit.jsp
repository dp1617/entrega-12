<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${requestURI}" modelAttribute="album">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="songs" />
	<form:hidden path="artist" />
	<form:hidden path="billboard" />
	<form:hidden path="dateOfRelease" />

	<acme:textbox code="album.title" path="title" />
	<acme:textbox code="album.picture" path="picture" />

	<br />
	
	<acme:submit name="save" code="album.save" />
	<jstl:if test="${album.id != 0}">
		<acme:submit name="delete" code="album.delete" />
	</jstl:if>
	<acme:cancel url="album/artist/list.do" code="album.cancel" />

</form:form>
