<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${requestURI}" modelAttribute="playlist">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="totalMinutes" />
	<form:hidden path="totalSeconds" />
	<form:hidden path="user" />
	<form:hidden path="songs" />

	<acme:textbox code="playlist.title" path="title" />
	
	<acme:submit name="save" code="playlist.save" />

	<acme:cancel url="playlist/user/myPlaylists.do" code="playlist.cancel" />

</form:form>
