<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if test="${requestURI.contains('all')}">
	<form method="POST" action="playlist/search.do" id="search"
		name="search">
		<input type="text" id="keyword" name="keyword" /> <input
			type="submit" value="<spring:message code="playlist.search"/>" />
	</form>
</jstl:if>

<display:table name="playlists" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<jstl:if test="${requestURI.contains('myPlaylists.do')}">
		<security:authorize access="hasRole('USER')">
		<display:column title="${editHeader}">
				<input type="button" name="delete"
					value="<spring:message code="playlist.delete" />"
					onclick="javascript: window.location.replace('playlist/user/delete.do?playlistId=${row.id}');" />
			</display:column>
			
			<display:column title="${editHeader}">
				<input type="button" name="edit"
					value="<spring:message code="playlist.edit" />"
					onclick="javascript: window.location.replace('playlist/user/edit.do?playlistId=${row.id}');" />
			</display:column>
		</security:authorize>
	</jstl:if>

	<jstl:if
		test="${requestURI.contains('all') || requestURI.contains('my')}">

		<spring:message code="playlist.title" var="titleHeader" />
		<display:column title="${titleHeader}"
			sortable="true">
			<a href="playlist/details.do?playlistId=${row.id}"><spring:message
					text="${row.title}" /> </a>
		</display:column>

		<spring:message code="playlist.totalDuration"
			var="totalDurationHeader" />
		<display:column title="${totalDurationHeader}" sortable="true">${row.totalMinutes}:${row.totalSeconds}</display:column>


	</jstl:if>

</display:table>
