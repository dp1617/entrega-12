<%--
 * index.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<jstl:if test="${!empty billboard}">
	<img src="${billboard.picture}" WIDTH=350 HEIGHT=350 />
</jstl:if>
<br />
<br />

<security:authorize access="isAuthenticated()">
	<h2>
		<spring:message code="top" />
	</h2>
	<display:table name="songs" id="row" requestURI="${requestURI}"
		pagesize="5" class="displaytag">



		<spring:message code="song.name" var="nameHeader" />
		<display:column title="${nameHeader}" sortable="true" style="rowColor">
			<a href="song/details.do?songId=${row.id}"><spring:message
					text="${row.name}" /> </a>
		</display:column>

		<spring:message code="song.favs" var="favsHeader" />
		<display:column property="favs" title="${favsHeader}" sortable="true"
			style="rowColor" />


	</display:table>
</security:authorize>

<security:authorize access="hasRole('USER')">
	<h2>
		<spring:message code="events.last" />
	</h2>
	<display:table name="events" id="row" requestURI="${requestURI}"
		pagesize="5" class="displaytag">

		<spring:message code="event.artistName" var="artistNameHeader" />
		<display:column title="${artistNameHeader}" sortable="true">
			<a href="artist/details1.do?artistId=${row.artist.id}"><spring:message
					text="${row.artist.name}" /> </a>
		</display:column>

		<spring:message code="event.name" var="nameHeader" />
		<display:column property="name" title="${nameHeader}" sortable="true" />

		<spring:message code="event.startDate" var="startDateHeader" />
		<display:column property="startDate" title="${startDateHeader}"
			sortable="true" format="{0,date,dd/MM/YYYY HH:mm}" />

		<spring:message code="event.endDate" var="endDateHeader" />
		<display:column property="endDate" title="${endDateHeader}"
			sortable="true" format="{0,date,dd/MM/YYYY HH:mm}" />

		<spring:message code="event.place" var="placeHeader" />
		<display:column property="place" title="${placeHeader}"
			sortable="true" />

		<spring:message code="event.maxParticipants"
			var="maxParticipantsHeader" />
		<display:column property="maxParticipants"
			title="${maxParticipantsHeader}" />

		<spring:message code="event.actualParticipants"
			var="actualParticipantsHeader" />
		<display:column property="actualParticipants"
			title="${actualParticipantsHeader}" />

		<spring:message code="event.seeUsers" var="seeUsersHeader" />
		<display:column title="${seeUsersheader}">
			<input type="submit" name="accept"
				value="<spring:message code="event.seeUsers" />"
				onclick="javascript: window.location.replace('user/event/joined.do?eventID=${row.id}');" />
		</display:column>

		<security:authorize access="hasRole('USER')">
			<display:column title="${acceptHeader}" sortable="false">

				<jstl:if test="${!row.getUsers().contains(user)}">
					<input type="submit" name="join"
						value="<spring:message code="event.join" />"
						onclick="javascript: window.location.replace('user/event/joinEvent.do?eventID=${row.id}');" />
				</jstl:if>

				<jstl:if test="${row.getUsers().contains(user)}">
					<spring:message code="event.join.error2" />
				</jstl:if>

			</display:column>
		</security:authorize>

	</display:table>
</security:authorize>

<p>
	<spring:message code="welcome.greeting.current.time" />
	${moment}
</p>
