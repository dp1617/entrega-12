<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${requestURI}" modelAttribute="billboard">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="validated" />

	<acme:textbox code="billboard.picture" path="picture" />
	<acme:textbox code="billboard.description" path="description" />
	
	<acme:select items="${albums}" itemLabel="title"
		code="billboard.album" path="album" />

	<br />
	
	<acme:submit name="save" code="billboard.save" />
	<!-- <acme:cancel url="song/artist/list.do" code="song.cancel" />-->

</form:form>
