<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="billboards" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">


	<spring:message code="billboard.picture" var="pictureHeader" />
	<display:column property="picture" title="${pictureHeader}"
		sortable="true" />

	<spring:message code="billboard.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}"
		sortable="true" />

	<spring:message code="billboard.album" var="albumHeader" />
	<display:column property="album" title="${albumHeader}"
		sortable="true" />
		
	<spring:message code="billboard.delete" var="deleteHeader" />
  	<display:column title="${deleteHeader}">
			<acme:button
				href="billboard/administrator/delete.do?billboardId=${row.id}"
				name="delete" code="billboard.delete" />
	</display:column>
	
	<spring:message code="billboard.validate" var="validateHeader" />
  	<display:column title="${validateHeader}">
  		<jstl:if test="${row.getValidated() == false}">
				<acme:button
					href="billboard/administrator/validate.do?billboardId=${row.id}"
					name="validate" code="billboard.validate" />
		</jstl:if>
	</display:column>

</display:table>
