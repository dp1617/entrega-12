<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n -->

<form:form action="comment/create.do" modelAttribute="comment">

	<!-- Atributos -->

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="actor" />
	<form:hidden path="postedMoment" />
	<form:hidden path="banned" />

	<jsp:useBean id="commentService" class="services.CommentService"
		scope="page" />

	<table style="width: 50%" class="tablasforms">
		<tr>
			<td><form:label path="commentable">
					<spring:message code="comment.commentable" />
				</form:label></td>
			<td><form:select id="commentable" path="commentable">
					<jstl:forEach var="c" items="${commentables}">
						<jstl:if test="${commentService.checkCommentable(c)==false}">
							<form:option label="${c.name}" value="${c.id}" />
						</jstl:if>
					</jstl:forEach>

					<form:option label="- - - - - - - - - - - - - - - - -" value="" />

					<jstl:forEach var="c" items="${commentables}">
						<jstl:if test="${commentService.checkCommentable(c)==true}">
							<form:option label="${c.album.artist.name} - ${c.name}"
								value="${c.id}" />
						</jstl:if>
					</jstl:forEach>
				</form:select> <form:errors path="commentable" cssClass="error" /></td>
		</tr>
		<tr>
			<td><acme:textbox code="comment.title" path="title" /></td>
			<td><acme:textbox code="comment.stars" path="stars" /></td>
		</tr>
		<tr>
		<td><acme:textarea code="comment.text" path="text"/></td>
		</tr>
	</table>

	<!-- Acciones -->

	<acme:submit name="post" code="comment.post" />
	<acme:cancel url="comment/sent.do" code="comment.cancel" />

</form:form>