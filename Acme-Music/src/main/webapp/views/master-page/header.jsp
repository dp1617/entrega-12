<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>


<div>
	<br> <br> <br> <br>
</div>

<div>
	<ul id="jMenu">

		<li><a class="fNiv" href="/Acme-Music/"> <img
				src="images/logo.png" alt="Acme Music Co., Inc." width="150px"
				align="left" />
		</a></li>

		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message
						code="master.page.login" /></a></li>
			<li><a class="fNiv" href="artist/create.do"><spring:message
						code="master.page.artist.create" /></a></li>
			<li><a class="fNiv" href="user/create.do"><spring:message
						code="master.page.user.create" /></a></li>
		</security:authorize>

		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv" href="manager/administrator/create.do"><spring:message
						code="master.page.manager.create" /></a></li>
			<li><a class="fNiv" href="administrator/dashboard.do"><spring:message
						code="master.page.administrator.dashboard" /></a></li>
			<li><a class="fNiv" href="parameters/administrator/edit.do"><spring:message
						code="master.page.parameters.edit" /></a></li>
			<li><a class="fNiv" href="billboard/administrator/list.do"><spring:message
						code="master.page.billboard.list" /></a></li>
		</security:authorize>

		<li><a class="fNiv"> <spring:message
					code="master.page.user.song" />
		</a>
			<ul>
				<li class="arrow"></li>
				<li><a href="song/all.do"><spring:message
							code="master.page.song.all" /> </a></li>
				<security:authorize access="hasRole('ARTIST')">
					<li><a href="song/artist/list.do"><spring:message
								code="master.page.artist.songs" /> </a></li>
					<li><a href="song/artist/create.do"><spring:message
								code="master.page.artist.create.song" /> </a></li>
				</security:authorize>
				<security:authorize access="hasRole('USER')">
					<li><a href="song/user/favorites.do"><spring:message
								code="master.page.user.song.favorites" /> </a></li>
				</security:authorize>

			</ul></li>
		<security:authorize access="hasRole('ARTIST')">
			<li><a class="fNiv"> <spring:message
						code="master.page.artist.albums" />
			</a>
				<ul>
					<li class="arrow"></li>
					<li><a href="album/artist/list.do"><spring:message
								code="master.page.album.list" /> </a></li>
					<li><a href="album/artist/create.do"><spring:message
								code="master.page.album.create" /> </a></li>
				</ul></li>
		</security:authorize>
			
			<security:authorize access="hasRole('MANAGER')">
			<li><a class="fNiv"> <spring:message
					code="master.page.manager.billboards" />
		</a>
			<ul>
				<li class="arrow"></li>
				<li><a href="billboard/manager/create.do"><spring:message
							code="master.page.billboard.create" /> </a></li>
			</ul>
			</li>
			</security:authorize>
			
		<li><a class="fNiv"> <spring:message
					code="master.page.playlist" />
		</a>
			<ul>
				<li class="arrow"></li>
				<li><a href="playlist/all.do"><spring:message
							code="master.page.playlist.all" /> </a></li>

				<security:authorize access="hasRole('USER')">

					<li><a href="playlist/user/myPlaylists.do"><spring:message
								code="master.page.playlist.user.myPlaylists" /> </a></li>
					<li><a href="playlist/user/create.do"><spring:message
								code="master.page.playlist.user.create" /> </a></li>
				</security:authorize>
			</ul></li>

		<security:authorize access="hasRole('USER')">
			<li><a class="fNiv"> <spring:message
						code="master.page.events" />
			</a>
				<ul>
					<li class="arrow"></li>
					<li><a href="user/event/all.do"><spring:message
								code="master.page.events.all" /> </a></li>
					<li><a href="user/event/byArtists.do"><spring:message
								code="master.page.events.byArtists" /> </a></li>
				</ul></li>
		</security:authorize>
		
		<security:authorize access="hasRole('MANAGER')">
			<li><a class="fNiv"> <spring:message
						code="master.page.eventsCalendar" />
			</a>
				<ul>
					<li class="arrow"></li>
					<li><a href="manager/eventsCalendar/list.do"><spring:message
								code="master.page.eventsCalendar.list" /> </a></li>
					<li><a href="manager/eventsCalendar/create.do"><spring:message
								code="master.page.eventsCalendar.create" /> </a></li>
				</ul></li>
		</security:authorize>

		<li><a class="fNiv"> <spring:message
					code="master.page.artist" />
		</a>
			<ul>
				<li class="arrow"></li>
				<li><a href="artist/all.do"><spring:message
							code="master.page.artist.all" /> </a></li>
			</ul></li>
			
		<li><a class="fNiv"> <spring:message
					code="master.page.user" />
		</a>
			<ul>
				<li class="arrow"></li>
				<li><a href="user/all.do"><spring:message
							code="master.page.user.all" /> </a></li>
			</ul></li>

		<security:authorize access="hasRole('ARTIST')">
			<li><a class="fNiv"> <spring:message
						code="master.page.manager" />
			</a>
				<ul>
					<li class="arrow"></li>
					<li><a href="manager/all.do"><spring:message
								code="master.page.manager.all" /> </a></li>
				</ul></li>
		</security:authorize>

		<security:authorize access="hasAnyRole('ARTIST','MANAGER')">
			<li><a class="fNiv"> <spring:message
						code="master.page.events" />
			</a>
				<ul>
					<li class="arrow"></li>
					<security:authorize access="hasRole('ARTIST')">
						<li><a href="artist/event/all.do"><spring:message
									code="master.page.events.all" /> </a></li>
					</security:authorize>
					<security:authorize access="hasRole('MANAGER')">
						<li><a href="manager/event/all.do"><spring:message
									code="master.page.events.all" /> </a></li>
					</security:authorize>
					<li><a href="event/create.do"><spring:message
								code="master.page.events.create" /> </a></li>
				</ul></li>
		</security:authorize>

		<security:authorize access="isAuthenticated()">
			<li><a class="fNiv"> <spring:message
						code="master.page.artists.users" />
			</a>
				<ul>
					<li class="arrow"></li>
					<li><a href="actor/following.do"><spring:message
								code="master.page.actor.following" /></a></li>
					<security:authorize access="hasAnyRole('USER','ARTIST')">
						<li><a href="actor/followingMe.do"><spring:message
									code="master.page.actor.followingMe" /></a></li>
					</security:authorize>
				</ul></li>

			<li><a class="fNiv"><spring:message
						code="master.page.comments" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="comment/sent.do"><spring:message
								code="master.page.comments.sent" /></a></li>
					<li><a href="comment/received.do"><spring:message
								code="master.page.comments.received" /></a></li>
					<li><a href="comment/create.do"><spring:message
								code="master.page.comments.create" /></a></li>
					<security:authorize access="hasRole('ADMIN')">
						<li><a href="comment/administrator/listAll.do"><spring:message
									code="master.page.comments.listAll" /></a></li>
					</security:authorize>
				</ul></li>

			<li><a class="fNiv"> <spring:message
						code="master.page.profile" /> (<security:authentication
						property="principal.username" />)
			</a>
				<ul>
					<li class="arrow"></li>
					<security:authorize access="hasRole('ARTIST')">
						<li><a href="artist/edit.do"><spring:message
									code="master.page.actor.edit" /></a></li>
					</security:authorize>
					<security:authorize access="hasRole('MANAGER')">
						<li><a href="manager/edit.do"><spring:message
									code="master.page.actor.edit" /></a></li>
					</security:authorize>
					<security:authorize access="hasRole('USER')">
						<li><a href="user/edit.do"><spring:message
									code="master.page.actor.edit" /></a></li>
					</security:authorize>
					<li><a href="j_spring_security_logout"><spring:message
								code="master.page.logout" /> </a></li>
				</ul></li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>