<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Creaci�n de manager (como administrador) -->

<form:form action="manager/administrator/create.do" modelAttribute="actorForm">

	<!-- Campos obligatorios a rellenar -->

	<b><spring:message code="manager.PersonalData" /></b>

	<br />

	<acme:textbox code="manager.name" path="name" />

	<acme:textbox code="manager.nickname" path="nickname" />

	<acme:textbox code="manager.email" path="email" />
	
	<acme:textbox code="manager.phone" path="phone" placeholder="+XXYYYYYYYYY"/>
	
	<acme:textbox code="manager.picture" path="picture" placeholder="URL"/>

	<br />

	<!-- Usuario y contrase�a -->

	<b><spring:message code="manager.LoginData" /></b>

	<br />

	<acme:textbox code="manager.username" path="username" />

	<acme:password code="manager.password" path="password" />

	<acme:password code="manager.secondPassword" path="secondPassword" />

	<br />
	

	<!-- Aceptar para continuar -->

	<form:label path="checkBox">
		<spring:message code="manager.checkBox" />
	</form:label>
	<form:checkbox path="checkBox" />
	<a href="misc/terms.do"> <spring:message
			code="manager.moreInfo" />
	</a>
	<form:errors class="error" path="checkBox" />

	<br />
	<br />

	<!-- Acciones -->
	
	<acme:submit name="save" code="manager.signIn"/>
	
	<acme:cancel url="" code="manager.cancel"/>

</form:form>

<br>

<!-- Errores -->

<jstl:if test="${phoneError != null}">
	<span class="message"><spring:message code="${phoneError}" /></span>
</jstl:if>

<jstl:if test="${passwordMatch != null}">
	<span class="message"><spring:message code="${pass}" /></span>
</jstl:if>

<jstl:if test="${duplicate != null}">
	<span class="message"><spring:message code="${duplicate}" /></span>
</jstl:if>

<jstl:if test="${terms != null}">
	<span class="message"><spring:message code="${terms}" /></span>
</jstl:if>
