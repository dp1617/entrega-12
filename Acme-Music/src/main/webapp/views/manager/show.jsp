<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div class="artist" style="text-align: center">

	<jstl:if test="${not empty manager.picture}">
		<img src="${manager.picture}" style="width: 250px; height: 250px;">
		<br>
		<p />
	</jstl:if>
	<b><spring:message code="manager.name" />:</b> ${manager.name}<br>
	<b><spring:message code="manager.nickname" />:</b> ${manager.nickname}<br>
	<b><spring:message code="manager.email" />:</b> ${manager.email}<br>
	<jstl:if test="${not empty manager.phone}">
		<b><spring:message code="manager.phone" />:</b> ${manager.phone}<br>
	</jstl:if>
	<p />

</div>

<b><spring:message code="manager.comments" /></b>
<p />

<display:table name="comments" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="comment.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />

	<spring:message code="comment.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" />

	<spring:message code="comment.postedMoment" var="postedMomentHeader" />
	<display:column property="postedMoment" title="${postedMomentHeader}"
		style="rowColor" sortProperty="postedMoment"
		format="{0,date,dd/MM/yyyy HH:mm}" sortable="true" />

	<spring:message code="comment.text" var="textHeader" />
	<display:column property="text" title="${textHeader}" />

	<spring:message code="comment.stars" var="starsHeader" />
	<display:column property="stars" title="${starsHeader}" />

</display:table>


