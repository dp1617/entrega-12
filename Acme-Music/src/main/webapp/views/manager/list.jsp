<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="managers" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">


	<spring:message code="manager.name" var="nameHeader" />
	<display:column title="${nameHeader}" sortable="true">
		<a href="manager/details.do?managerId=${row.id}"><spring:message
				text="${row.name}" /> </a>
	</display:column>

	<spring:message code="manager.nickname" var="nicknameHeader" />
	<display:column property="nickname" title="${nicknameHeader}"
		sortable="true" />

	<spring:message code="manager.email" var="emailHeader" />
	<display:column property="email" title="${emailHeader}"
		sortable="true" />

	<spring:message code="manager.phone" var="phoneHeader" />
	<display:column property="phone" title="${phoneHeader}"
		sortable="true" />
		
	<jsp:useBean id="loginService" class="security.LoginService"
			scope="page" />
	
	<display:column title="">
		<jstl:if test="${row.getArtist() == null}">
				<acme:button href="artist/manager/hire.do?managerId=${row.id}"
			name="register" code="manager.hire" />
		</jstl:if>
	</display:column>
	
	<display:column title="">
		<jstl:if test="${row.getArtist() != null}">
			<jstl:if test="${loginService.getPrincipal().id == row.getArtist().getUserAccount().id}">
					<acme:button href="artist/manager/fire.do?managerId=${row.id}"
				name="register" code="manager.fire" />
			</jstl:if>
		</jstl:if>
	</display:column>

</display:table>
