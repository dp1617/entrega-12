<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Editar actor -->

<form:form action="manager/edit.do" modelAttribute="manager">

	<form:hidden path="id"/>
	<form:hidden path="version"/>
	<form:hidden path="userAccount"/>

	<!-- Campos obligatorios a rellenar -->

	<b><spring:message code="manager.PersonalData" /></b>

	<br />

	<acme:textbox code="manager.name" path="name" />

	<acme:textbox code="manager.nickname" path="nickname" />

	<acme:textbox code="manager.email" path="email" />
	
	<acme:textbox code="manager.phone" path="phone" />
	
	<acme:textbox code="manager.picture" path="picture" />

	<br />


	<!-- Acciones -->
	
	<acme:submit name="save" code="manager.save"/>
	
	<acme:cancel url="" code="manager.cancel"/>

</form:form>

<br>

<!-- Errores -->

<jstl:if test="${phoneError != null}">
	<span class="message"><spring:message code="${phoneError}" /></span>
</jstl:if>
