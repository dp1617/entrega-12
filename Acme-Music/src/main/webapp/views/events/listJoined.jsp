<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="event" id="row2" class="displaytag">
	<spring:message code="event.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" sortable="false" />

	<spring:message code="event.startDate" var="startDateHeader" />
	<display:column property="startDate" title="${startDateHeader}"
		sortable="true" format="{0,date,dd/MM/YYYY HH:mm}" />

	<spring:message code="event.endDate" var="endDateHeader" />
	<display:column property="endDate" title="${endDateHeader}"
		sortable="true" format="{0,date,dd/MM/YYYY HH:mm}" />

	<spring:message code="event.place" var="placeHeader" />
	<display:column property="place" title="${placeHeader}" sortable="true" />

	<spring:message code="event.maxParticipants"
		var="maxParticipantsHeader" />
	<display:column property="maxParticipants"
		title="${maxParticipantsHeader}" />

	<spring:message code="event.actualParticipants"
		var="actualParticipantsHeader" />
	<display:column property="actualParticipants"
		title="${actualParticipantsHeader}" />
</display:table>

<display:table name="users" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="user.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" sortable="true"
		href="user/details1.do?userId=${row.id}" />

	<spring:message code="user.nickname" var="nicknameHeader" />
	<display:column property="nickname" title="${nicknameHeader}"
		sortable="true" />

	<spring:message code="user.email" var="emailHeader" />
	<display:column property="email" title="${emailHeader}" sortable="true" />

	<spring:message code="user.phone" var="phoneHeader" />
	<display:column property="phone" title="${phoneHeader}" sortable="true" />

</display:table>