<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="events" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="event.artistName" var="artistNameHeader" />
	<display:column title="${artistNameHeader}" sortable="true">
		<a href="artist/details1.do?artistId=${row.artist.id}"><spring:message
				text="${row.artist.name}" /> </a>
	</display:column>

	<spring:message code="event.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" sortable="true" />

	<spring:message code="event.startDate" var="startDateHeader" />
	<display:column property="startDate" title="${startDateHeader}"
		sortable="true" format="{0,date,dd/MM/YYYY HH:mm}" />

	<spring:message code="event.endDate" var="endDateHeader" />
	<display:column property="endDate" title="${endDateHeader}"
		sortable="true" format="{0,date,dd/MM/YYYY HH:mm}" />

	<spring:message code="event.place" var="placeHeader" />
	<display:column property="place" title="${placeHeader}" sortable="true" />

	<spring:message code="event.maxParticipants"
		var="maxParticipantsHeader" />
	<display:column property="maxParticipants"
		title="${maxParticipantsHeader}" />

	<spring:message code="event.actualParticipants"
		var="actualParticipantsHeader" />
	<display:column property="actualParticipants"
		title="${actualParticipantsHeader}" />
	
	<security:authorize access="hasAnyRole('ARTIST','MANAGER')">	
		<display:column>
			<jstl:forEach var="event" items="${aux}">
				<jstl:if test="${event.id==row.id}">
					<a href="event/edit.do?eventId=${row.id}"><spring:message code="event.edit"/></a>
				</jstl:if>
			</jstl:forEach>
		</display:column>
	</security:authorize>

	<spring:message code="event.seeUsers" var="seeUsersHeader" />
	<display:column title="${seeUsersheader}">
		<input type="submit" name="accept"
			value="<spring:message code="event.seeUsers" />"
			onclick="javascript: window.location.replace('user/event/joined.do?eventID=${row.id}');" />
	</display:column>

	<security:authorize access="hasRole('USER')">
		<display:column title="${acceptHeader}" sortable="false">

			<jstl:if test="${!row.getUsers().contains(user)}">
				<input type="submit" name="join"
					value="<spring:message code="event.join" />"
					onclick="javascript: window.location.replace('user/event/joinEvent.do?eventID=${row.id}');" />
			</jstl:if>
			
			<jstl:if test="${row.getUsers().contains(user)}">
				<spring:message code="event.join.error2" />	
			</jstl:if>
			
		</display:column>
	</security:authorize>

</display:table>