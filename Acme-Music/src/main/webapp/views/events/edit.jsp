<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${requestURI}" modelAttribute="event">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="actualParticipants" />
	<form:hidden path="eventsCalendar" />
	<form:hidden path="users" />
	<form:hidden path="artist" />

	<acme:textbox code="event.name" path="name" />
	<acme:textbox code="event.startDate" path="startDate" />	
	<acme:textbox code="event.endDate" path="endDate" />	
	<acme:textbox code="event.place" path="place" />	
	<acme:textbox code="event.maxParticipants" path="maxParticipants" />
	
	<acme:submit name="save" code="event.save" />

	<jstl:if test="${event.id != 0}">
		<acme:submit name="delete" code="event.delete" />
	</jstl:if>
	
	<security:authorize access="hasRole('ARTIST')">
		<acme:cancel url="/artist/event/all.do" code="event.cancel" />
	</security:authorize>
	<security:authorize access="hasRole('MANAGER')">
		<acme:cancel url="/manager/event/all.do" code="event.cancel" />
	</security:authorize>
	

</form:form>
