
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class BillboardServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private BillboardService	billboardService;
	@Autowired
	private ActorService		actorService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is authenticated as admin must be able to:
	 * List all billboards.
	 * 
	 * En este caso se llevar� a cabo la acci�n de listar todos los billboards del sistema.
	 * 
	 * Los errores ocurrir�n si se intenta acceder a la lista sin ser admin.
	 */
	public void listBillboards(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.actorService.checkIfAdministrator();

			// Visualizamos el listado de eventos
			this.billboardService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as admin must be able to:
	 * Delete billboards.
	 * 
	 * En este caso se llevar� a cabo la acci�n de borrar un billboard del sistema.
	 * 
	 * Los errores ocurrir�n si:
	 * � Se intenta borrar un billboard sin ser admin.
	 * � Se intenta borrar una billboard que no existe
	 */
	public void deleteBillboard(final String username, final int billboardId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.actorService.checkIfAdministrator();

			// Eliminamos la bilboard
			this.billboardService.deleteBillboard(billboardId);

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as admin must be able to:
	 * Validate billboards.
	 * 
	 * En este caso se llevar� a cabo la acci�n de validar un billboard del sistema.
	 * 
	 * Los errores ocurrir�n si:
	 * � Se intenta validar una billboard sin ser admin.
	 * � Se intenta validar una billboard que no existe
	 * � Se intenta validar una billboard ya validada
	 */
	public void validateBillboard(final String username, final int billboardId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.actorService.checkIfAdministrator();

			// Validamos la bilboard
			this.billboardService.validateBillboard(billboardId);

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListBillboards() {
		final Object testingData[][] = {
			// Visualizar listado como artist -> false
			{
				"artist1", IllegalArgumentException.class
			},
			// Visualizar listado como manager -> false
			{
				"manager1", IllegalArgumentException.class
			},
			// Visualizar listado como user -> false
			{
				"user1", IllegalArgumentException.class
			},
			// Visualizar listado como no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
			// Visualizar listado como admin -> true
			{
				"admin", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listBillboards((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverDeleteBillboard() {
		final Object testingData[][] = {
			// Eliminar billboard como artist -> false
			{
				"artist1", 123, IllegalArgumentException.class
			},
			// Eliminar billboard como manager -> false
			{
				"manager1", 123, IllegalArgumentException.class
			},
			// Eliminar billboard como user -> false
			{
				"user1", 123, IllegalArgumentException.class
			},
			// Eliminar billboard como no autentificado -> false
			{
				null, 123, IllegalArgumentException.class
			},
			// Eliminar billboard como admin -> true
			{
				"admin", 123, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteBillboard((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverValidateBillboard() {
		final Object testingData[][] = {
			// Validar billboard como artist -> false
			{
				"artist1", 133, IllegalArgumentException.class
			},
			// Validar billboard como manager -> false
			{
				"manager1", 133, IllegalArgumentException.class
			},
			// Validar billboard como user -> false
			{
				"user1", 133, IllegalArgumentException.class
			},
			// Validar billboard como no autentificado -> false
			{
				null, 133, IllegalArgumentException.class
			},
			//			// Validar billboard ya validada -> false
			//			{
			//				"admin", 128, IllegalArgumentException.class
			//			},
			// Validar billboard como admin -> true
			{
				"admin", 133, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteBillboard((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
