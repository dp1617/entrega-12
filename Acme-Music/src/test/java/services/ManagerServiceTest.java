
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Manager;
import forms.ActorForm;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ManagerServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ManagerService	managerService;
	@Autowired
	private ActorService	actorService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is authenticated as admin must be able to:
	 * Register in the system a new manager.
	 * 
	 * En este caso de uso se llevara a cabo el registro de un manager en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario esta autentificado pero no como admin
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerManager(final String username, final String newUsername, final String name, final String nickname, final String email, final String phone, final String picture, final String password, final String secondPassword,
		final Boolean checkBox, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que este autentificado como administrador
			this.actorService.checkIfAdministrator();

			// Inicializamos los atributos para la creaci�n
			final ActorForm actor = new ActorForm();

			actor.setName(name);
			actor.setNickname(nickname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPicture(picture);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Comprobamos atributos
			this.managerService.checkAttributes(actor);

			//Reconsturimos
			final Manager manager = this.managerService.reconstruct(actor);

			//Guardamos
			this.managerService.saveForm(manager);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * "An actor who is authenticated as a manager must be able to:
	 * - Edit his or her profile."
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de editar un perfil.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona autentificada no es un manager
	 * � La persona no est� autentificada
	 * � Alguno de los atributos no tiene el formato correcto o no es v�lido
	 * � La Id del user no existe
	 */
	public void editManager(final String username, final String name, final String nickname, final String email, final String phone, final String picture, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que sea un artist
			this.managerService.checkIfManager();

			// Inicializamos los atributos para la edici�n
			Manager manager;
			manager = this.managerService.findByPrincipal();

			manager.setName(name);
			manager.setNickname(nickname);
			manager.setEmail(email);
			manager.setPhone(phone);
			manager.setPicture(picture);

			//Comprobamos atributos
			this.managerService.checkAttributes(manager);

			//Guardamos
			this.managerService.save(manager);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void registerManager() {

		final Object testingData[][] = {
			// Creaci�n de manager como autentificado (1) -> false
			{
				"artist1", "managerTest1", "managerTest1", "managerTest1", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager como autentificado (2) -> false
			{
				"user1", "managerTest2", "managerTest2", "managerTest2", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager como autentificado (3) -> false
			{
				"manager1", "managerTest3", "managerTest3", "managerTest3", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con phone incorrecto -> false
			{
				null, "managerTest4", "managerTest4", "managerTest4", "email@domain.com", "23423423432423423", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con picture incorrecto -> false
			{
				null, "managerTest5", "managerTest5", "managerTest5", "email@domain.com", "678432123", "notanurl", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager sin aceptar t�rminos -> false
			{
				null, "managerTest6", "managerTest6", "managerTest6", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", false, IllegalArgumentException.class
			},
			// Creaci�n de manager con usuario no �nico -> false
			{
				null, "manager1", "managerTest7", "managerTest7", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con contrase�as no coincidentes -> false
			{
				null, "managerTest8", "managerTest8", "managerTest8", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password2", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con todo correcto -> true
			{
				"admin", "managerTest9", "managerTest9", "managerTest9", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerManager((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (Boolean) testingData[i][9], (Class<?>) testingData[i][10]);
	}

	@Test
	public void editManager() {

		final Object testingData[][] = {
			// Edici�n de user como autentificado (1) -> false
			{
				"admin", "managerTest1", "managerTest1", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de user como autentificado (2) -> false
			{
				"artist1", "managerTest2", "managerTest2", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de user como autentificado (3) -> false
			{
				"user1", "managerTest3", "managerTest3", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de user como no autentificado -> false
			{
				null, "managerTest7", "managerTest7", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de user con todo correcto -> true
			{
				"manager1", "managerTest6", "managerTest6", "email@domain.com", "678432123", "https://www.picture.com", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.editManager((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}
}
