
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Event;
import domain.EventsCalendar;
import domain.Manager;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class EventsCalendarServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private ManagerService			managerService;
	@Autowired
	private EventsCalendarService	eventsCalendarService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is authenticated as a manager must be able to: Manage his or her actors calendar events,
	 * which includes creating, listing, deleting and adding events from them.
	 * 
	 * En este caso de prueba se llevara a cabo la acci�n de listar los calendarios de eventos de un manager.
	 * 
	 * Para forzar el fallo se daran los siguientes casos:
	 * 
	 * - El usuario no esta autentificado
	 * - El usuario autentificado no es un manager
	 */
	public void list(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			final Manager manager = this.managerService.findByPrincipal();

			@SuppressWarnings("unused")
			final Collection<EventsCalendar> eventsCalendar = manager.getEventsCalendars();

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage his or her actors calendar events,
	 * which includes creating, listing, deleting and adding events from them.
	 * 
	 * En este caso de prueba se llevara a cabo la acci�n de crear un calendario de eventos.
	 * 
	 * Para forzar el fallo se daran los siguientes casos:
	 * 
	 * - El usuario no esta autentificado
	 * - El usuario autentificado no es un manager
	 * - Los atributos son incorrectos
	 */
	@SuppressWarnings("deprecation")
	public void create(final String username, final String monthAndYear, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			final Manager manager = this.managerService.findByPrincipal();

			final EventsCalendar eventsCalendar = new EventsCalendar();

			eventsCalendar.setMonthAndYear(new Date(monthAndYear));
			eventsCalendar.setManager(manager);
			eventsCalendar.setEvents(new ArrayList<Event>());

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a manager must be able to: Manage his or her actors calendar events,
	 * which includes creating, listing, deleting and adding events from them.
	 * 
	 * En este caso de prueba se llevara a cabo la acci�n de borrar un calendario de eventos.
	 * 
	 * Para forzar el fallo se daran los siguientes casos:
	 * 
	 * - El usuario no esta autentificado
	 * - El usuario autentificado no es un manager
	 * - El id del calendario no existe
	 * - El id del calendario no pertenece al manager logueado
	 */
	public void delete(final String username, final int eventsCalendarId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.managerService.checkIfManager();

			final EventsCalendar eventsCalendar = this.eventsCalendarService.findOne(eventsCalendarId);

			Assert.isTrue(eventsCalendar.getManager().getId() == this.managerService.findByPrincipal().getId());

			this.eventsCalendarService.delete(eventsCalendar);

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void listDriver() {
		final Object testingData[][] = {
			// Listar calendarios logueado como manager -> true
			{
				"manager1", null
			},
			// Listar calendarios logueado como manager -> true
			{
				"manager2", null
			},
			// Listar calendarios sin loguearse -> false
			{
				null, IllegalArgumentException.class
			},
			// Listar calendarios logueado como artist -> false
			{
				"artist1", IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.list((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void createDriver() {
		final Object testingData[][] = {
			// Crear un calendario logueado como manager -> true
			{
				"manager1", "01/02/2018 00:00", null
			},
			// Crear un calendario sin loguearse -> false
			{
				null, "01/02/2018 00:00", IllegalArgumentException.class
			},
			// Crear un calendario logueado como artist -> false
			{
				"artist1", "01/02/2018 00:00", IllegalArgumentException.class
			},
			// Crear un calendario con parametros incorrectos -> false
			{
				"artist1", null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.create((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void deleteDriver() {
		final Object testingData[][] = {
			// Borrar un calendario logueado como manager -> true
			{
				"manager2", 166, null
			},
			// Borrar un calendario sin loguearse -> false
			{
				null, 166, IllegalArgumentException.class
			},
			// Borrar un calendario logueado como artist -> false
			{
				"artist1", 166, IllegalArgumentException.class
			},
			// Borrar un calendario con un id que no existe -> false
			{
				"manager2", 8432, IllegalArgumentException.class
			},
			// Borrar un calendario que no pertenece al manager logueado -> false
			{
				"manager2", 167, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.delete((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
