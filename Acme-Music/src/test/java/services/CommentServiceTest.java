
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Comment;
import domain.Commentable;
import domain.Song;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private CommentService			commentService;
	@Autowired
	private ActorService			actorService;
	@Autowired
	private SongService				songService;
	@Autowired
	private AdministratorService	administratorService;


	// Templates --------------------------------------------------------------

	/*
	 * Post a comment.
	 * 
	 * En este caso de uso se crear� un comentario. Se forzar� el error cuando se intente
	 * realizar con un usuario no autentificado, falta alg�n atributo a completar,
	 * se crea un comentario hacia uno mismo o el n� de estrellas no es correcto.
	 */
	public void createCommentTest(final String username, final String commentable, final String title, final String text, final int stars, final Class<?> expected) {

		Class<?> caught = null;

		try {

			final Commentable c = this.actorService.findActorByUsername(commentable);

			this.authenticate(username);
			this.actorService.checkIfActor();

			final Comment comment = new Comment();

			comment.setActor(this.actorService.findByPrincipal());
			comment.setCommentable(c);
			comment.setTitle(title);
			comment.setText(text);
			comment.setStars(stars);

			this.commentService.save(comment);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Post a comment.
	 * 
	 * En este caso de uso se listaran los comentarios creados hacia actores y anouncements.
	 * Para forzar el error se acceder� con un usuario no autentificado y se comprobar�
	 * que el filtro de comentarios baneados funciona correctamente.
	 */
	public void listCommentSentTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Collection<Comment> commentsSongs = new ArrayList<>();
			Collection<Comment> comments = new ArrayList<>();

			comments = this.commentService.getAllWrittenCommentsToActorByAnActor();
			commentsSongs = this.commentService.getAllWrittenCommentsToSongsByAnActor();

			Assert.notNull(commentsSongs);
			Assert.notNull(comments);

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();

		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * Post a comment.
	 * 
	 * En este caso de uso se listaran los comentarios que ha recibido un actor por
	 * parte de otros actores. Para forzar el error se intenar� acceder con un usuario
	 * no autentificado y se comprobar� que el filtro de comentarios baneados funciona
	 * correctamente.
	 */
	public void listCommentReceivedTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			Actor actor;

			this.authenticate(username);

			Collection<Comment> comments = new ArrayList<>();
			Collection<Comment> createdComments = new ArrayList<>();

			actor = this.actorService.findByPrincipal();
			comments = actor.getReceivedComments();
			createdComments = this.commentService.filterCommentsWithBan(comments);

			Assert.notNull(createdComments.size());

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();

		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * Post a comment.
	 * 
	 * En este caso de uso se listaran los comentarios que ha recibido un song.
	 * Para forzar el error se intentar� acceder con un usuario no autentificado,
	 * utilizando una ID no v�lida.
	 */
	public void listCommentSongReceivedTest(final String username, final int songId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			Song song;
			this.authenticate(username);
			this.actorService.checkIfActor();

			song = this.songService.findOne(songId);
			final Collection<Comment> comments = song.getReceivedComments();

			Assert.notNull(comments.size());

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();

		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * Post a comment.
	 * 
	 * En este caso de uso se listaran los comentarios creados por otros actores hacia otros actores.
	 * Para forzar el error se intentar� acceder con un usuario distinto a administrador
	 * y se comprobar� el buen funcionamiento del filtro de comentarios baneados.
	 */
	public void listCommentSendToActorsByActorsTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			Collection<Comment> comments = new ArrayList<>();
			comments = this.commentService.getAllWrittenCommentsToActors();
			Assert.notNull(comments);
			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();

		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an administrator must be able to: Ban or unban songs, comments,
	 * users and artists if they considered so. If its a song or a comment what is banned, they wont
	 * be shown in any place; if its a user or and artists, they wont be able to access to the system
	 * and will disappear wherever they were; if its an actor, that means all his managers are also banned.
	 * 
	 * En este caso de prueba se realizara la accion de banear un comentario.
	 * 
	 * Para forzar el error se daran las siguientes situaciones:
	 * 
	 * - El usuario no esta autentficado
	 * - El usuario autentificado no es un admin
	 * - El id del comentario no existe
	 * - Se intenta banear un comentario ya baneado
	 */
	public void banComment(final String username, final int commentId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			// Autentificamos al actor
			this.authenticate(username);

			this.actorService.checkIfAdministrator();

			final Comment comment = this.commentService.findOne(commentId);
			this.administratorService.ban(comment);

			// Desautentificamos
			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an administrator must be able to: Ban or unban songs, comments,
	 * users and artists if they considered so. If its a song or a comment what is banned, they wont
	 * be shown in any place; if its a user or and artists, they wont be able to access to the system
	 * and will disappear wherever they were; if its an actor, that means all his managers are also banned.
	 * 
	 * En este caso de prueba se realizara la accion de desbanear un comentario.
	 * 
	 * Para forzar el error se daran las siguientes situaciones:
	 * 
	 * - El usuario no esta autentficado
	 * - El usuario autentificado no es un admin
	 * - El id del comentario no existe
	 * - Se intenta desbanear un comentario no baneado
	 */
	public void unBanComment(final String username, final int commentId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			// Autentificamos al actor
			this.authenticate(username);

			this.actorService.checkIfAdministrator();

			final Comment comment = this.commentService.findOne(commentId);
			this.administratorService.unban(comment);

			// Desautentificamos
			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ---------------------------------------------------------------

	@Test
	public void driverCreateComment() {
		final Object testingData[][] = {
			// Create comment con user y atributos validos -> true
			{
				"user1", "user2", "Titulo test", "Text test", 4, null
			},
			// Create comment con campos mal puestos -> false
			{
				"user1", "user2", "Titulo test", "Text test", 34, IllegalArgumentException.class
			},
			// Create comment con usuario no autentificado -> false
			{
				null, "user1", "Titulo test", "Text test", 7, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.createCommentTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (int) testingData[i][4], (Class<?>) testingData[i][5]);

	}

	@Test
	public void driverListCommentSent() {
		final Object testingData[][] = {
			// List de comentarios enviados con user -> true
			{
				"user1", null
			},
			// List de comentarios enviados con usuario no autentificado -> true
			{
				null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.listCommentSentTest((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	@Test
	public void driverListCommentReceived() {
		final Object testingData[][] = {
			// List de comentarios recibidos con user -> true
			{
				"user2", null
			},
			// List de comentarios recibidos con usuario no autentificado -> true
			{
				null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.listCommentReceivedTest((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	@Test
	public void driverListCommentSongReceived() {
		final Object testingData[][] = {
			// Lista de comentarios que ha recibido un song -> true
			{
				"artist1", 119, null
			},
			// Lista de comentarios que ha recibido un song con usuario no autentificado -> true
			{
				null, 120, IllegalArgumentException.class
			},
			// Lista de comentarios que ha recibido un song con un id que no existe -> false
			{
				"artist1", 9999, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.listCommentSongReceivedTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);

	}

	@Test
	public void driverListCommentSendToActorsByActors() {
		final Object testingData[][] = {
			// Lista de comentarios con admin -> true
			{
				"admin", null
			},
			// Lista de comentarios con usuario no autentificado -> true
			{
				null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.listCommentSendToActorsByActorsTest((String) testingData[i][0], (Class<?>) testingData[i][1]);

	}

	@Test
	public void banCommentDriver() {

		final Object testingData[][] = {
			// Banear un comentario logueado como admin -> true
			{
				"admin", 171, null
			},
			// Banear un comentario sin loguearse -> false
			{
				null, 171, IllegalArgumentException.class
			},
			// Banear un comentario logueado como artist -> false
			{
				"artist1", 171, IllegalArgumentException.class
			},
			// Banear un comentario con un id que no existe -> false
			{
				"admin", 7569, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.banComment((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
