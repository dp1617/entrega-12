
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.User;
import forms.ActorForm;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private UserService		userService;
	@Autowired
	private PlaylistService	playlistService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is not authenticated/authentificated must be able to:
	 * Search for a user using a single keyword that must appear verbatim
	 * in his or her name or nickname and navigate to her or his playlists.
	 * 
	 * En este caso se llevar� a cabo la acci�n de listar todos los usuarios del sistema
	 * y mediante un buscador introducir una o varias palabras clave y mostrar
	 * por pantalla los resultados respecto a esa/s palabra/s clave/s. Todos los actores
	 * pueden acceder a ello, algunas acciones que pueden provocar el error:
	 * 
	 * � La id del usuario no existe a la hora de navegar hacia las playlists del usuario.
	 */
	public void searchUser(final String username, final String keyword, final int userId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Listamos todos los usuarios
			this.userService.findAll();

			// Introducimos la "keyword" y mostramos los resultados
			this.userService.searchUsersByKeyword(keyword);

			// Nos dirigimos al usuario deseado
			final User u = this.userService.findOne(userId);

			// Mostramos sus playlists
			this.playlistService.getMyPlaylists(u);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is not authenticated must be able to:
	 * Register in the system as an user.
	 * 
	 * En este caso de uso se llevara a cabo el registro de un user en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerUser(final String username, final String newUsername, final String name, final String nickname, final String email, final String phone, final String picture, final String password, final String secondPassword,
		final Boolean checkBox, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			final ActorForm actor = new ActorForm();

			actor.setName(name);
			actor.setNickname(nickname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPicture(picture);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Comprobamos atributos
			this.userService.checkAttributes(actor);

			//Reconsturimos
			final User user = this.userService.reconstruct(actor);

			//Guardamos
			this.userService.saveForm(user);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * "An actor who is authenticated as a user must be able to:
	 * - Edit his or her profile."
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de editar un perfil.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona autentificada no es un user
	 * � La persona no est� autentificada
	 * � Alguno de los atributos no tiene el formato correcto o no es v�lido
	 * � La Id del user no existe
	 */
	public void editUser(final String username, final String name, final String nickname, final String email, final String phone, final String picture, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que sea un artist
			this.userService.checkIfUser();

			// Inicializamos los atributos para la edici�n
			User user;
			user = this.userService.findByPrincipal();

			user.setName(name);
			user.setNickname(nickname);
			user.setEmail(email);
			user.setPhone(phone);
			user.setPicture(picture);

			//Comprobamos atributos
			this.userService.checkAttributes(user);

			//Guardamos
			this.userService.save(user);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void searchUser() {

		final Object testingData[][] = {
			// B�squeda como no autentificado -> true
			{
				null, "Oliver", 104, null
			},
			// B�squeda como admin -> true
			{
				"admin", "Liber", 105, null
			},
			// B�squeda como manager -> true
			{
				"manager1", "Liber", 105, null
			},
			// B�squeda como artista -> true
			{
				"artist1", "Liber", 105, null
			},
			// B�squeda como usuario -> true
			{
				"user1", "Oliver", 104, null
			},
			// B�squeda con id inexistente -> false
			{
				"user1", "Oliver", 43242, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.searchUser((String) testingData[i][0], (String) testingData[i][1], (int) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void registerUser() {

		final Object testingData[][] = {
			// Creaci�n de user como autentificado (1) -> false
			{
				"admin", "userTest1", "userTest1", "userTest1", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de user como autentificado (2) -> false
			{
				"user1", "userTest2", "userTest2", "userTest2", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de user como autentificado (3) -> false
			{
				"manager1", "userTest3", "userTest3", "userTest3", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de user con phone incorrecto -> false
			{
				null, "userTest4", "userTest4", "userTest4", "email@domain.com", "23423423432423423", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de user con picture incorrecto -> false
			{
				null, "userTest5", "userTest5", "userTest5", "email@domain.com", "678432123", "notanurl", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de user sin aceptar t�rminos -> false
			{
				null, "userTest6", "userTest6", "userTest6", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", false, IllegalArgumentException.class
			},
			// Creaci�n de user con usuario no �nico -> false
			{
				null, "user1", "userTest7", "userTest7", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de user con contrase�as no coincidentes -> false
			{
				null, "userTest8", "userTest8", "userTest8", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password2", true, IllegalArgumentException.class
			},
			// Creaci�n de user con todo correcto -> true
			{
				null, "userTest9", "userTest9", "userTest9", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerUser((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (Boolean) testingData[i][9], (Class<?>) testingData[i][10]);
	}

	@Test
	public void editUser() {

		final Object testingData[][] = {
			// Edici�n de user como autentificado (1) -> false
			{
				"admin", "userTest1", "userTest1", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de user como autentificado (2) -> false
			{
				"artist1", "userTest2", "userTest2", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de user como autentificado (3) -> false
			{
				"manager1", "userTest3", "userTest3", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de user como no autentificado -> false
			{
				null, "userTest7", "userTest7", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de user con todo correcto -> true
			{
				"user1", "userTest6", "userTest6", "email@domain.com", "678432123", "https://www.picture.com", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.editUser((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}
}
