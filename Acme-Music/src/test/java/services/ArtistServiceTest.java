
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Administrator;
import domain.Album;
import domain.Artist;
import domain.Comment;
import domain.Manager;
import domain.User;
import forms.ActorForm;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ArtistServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ArtistService	artistService;

	@Autowired
	private AlbumService	albumService;

	@Autowired
	private ActorService	actorService;

	@Autowired
	private ManagerService	managerService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is not authenticated/authentificated must be able to:
	 * Search for an artist using a single keyword that must appear verbatim
	 * in his or her name or nickname.
	 * 
	 * En este caso se llevar� a cabo la acci�n de listar todos los artistas del sistema
	 * y mediante un buscador introducir una o varias palabras clave y mostrar
	 * por pantalla los resultados respecto a esa/s palabra/s clave/s. Todos los actores
	 * pueden acceder a ello, no existen restricciones en la palabra clave y no participa
	 * ninguna id por lo que no existen situaciones que puedan forzar el error.
	 */
	public void searchArtist(final String username, final String keyword, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Listamos todas los artistas
			this.artistService.findAll();

			// Introducimos la "keyword" y mostramos los resultados
			this.artistService.searchArtistByKeyword(keyword);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is not authenticated must be able to:
	 * Register in the system as an artist.
	 * 
	 * En este caso de uso se llevara a cabo el registro de un artist en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerArtist(final String username, final String newUsername, final String name, final String nickname, final String email, final String phone, final String picture, final String password, final String secondPassword,
		final Boolean checkBox, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			final ActorForm actor = new ActorForm();

			actor.setName(name);
			actor.setNickname(nickname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPicture(picture);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Comprobamos atributos
			this.artistService.checkAttributes(actor);

			//Reconsturimos
			final Artist artist = this.artistService.reconstruct(actor);

			//Guardamos
			this.artistService.saveForm(artist);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * "An actor who is authenticated as a artist must be able to:
	 * - Edit his or her profile."
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de editar un perfil.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona autentificada no es un artist
	 * � La persona no est� autentificada
	 * � Alguno de los atributos no tiene el formato correcto o no es v�lido
	 * � La Id del artist no existe
	 */
	public void editArtist(final String username, final String name, final String nickname, final String email, final String phone, final String picture, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que sea un artist
			this.artistService.checkIfArtist();

			// Inicializamos los atributos para la edici�n
			Artist artist;
			artist = this.artistService.findByPrincipal();

			artist.setName(name);
			artist.setNickname(nickname);
			artist.setEmail(email);
			artist.setPhone(phone);
			artist.setPicture(picture);

			//Comprobamos atributos
			this.artistService.checkAttributes(artist);

			//Guardamos
			this.artistService.save(artist);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is not authenticated/authentificated must be able to: Browse the catalogue of artists,
	 * display their profiles, see his or her followers and navigate to the albums that he or she have authored.
	 * 
	 * En esta caso de prueba se llevara a cabo la acci�n de listar todos los artistas del sistema. Todos los actores pueden
	 * acceder y no intervienen ids por lo que no hay situaciones que puedan forzar el fallo
	 */
	public void list(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Listamos todas los artistas
			this.artistService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is not authenticated/authentificated must be able to: Browse the catalogue of artists,
	 * display their profiles, see his or her followers and navigate to the albums that he or she have authored.
	 * 
	 * En esta caso de prueba se llevara a cabo la accion de mostrar el perfil de un artist con todos sus albumes. Todos los actores pueden
	 * acceder.
	 * 
	 * Para fozar el fallo se pueden dar los siguientes casos:
	 * 
	 * - El id del artista indicado no existe
	 */
	public void display(final String username, final int artistId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Artist artist;
			@SuppressWarnings("unused")
			Integer auxFollow;
			@SuppressWarnings("unused")
			Collection<Album> artistAlbums = new ArrayList<>();
			@SuppressWarnings("unused")
			Collection<Actor> followers = new ArrayList<>();
			Collection<Comment> aux = new ArrayList<>();
			final Collection<Comment> comments = new ArrayList<>();

			artist = this.artistService.findOne(artistId);
			Assert.notNull(artist);
			artistAlbums = this.albumService.getArtistAlbums(artist);
			followers = this.actorService.actorsFollowingMe(artist);

			aux = artist.getReceivedComments();
			for (final Comment c : aux)
				if (!c.getBanned())
					comments.add(c);

			auxFollow = 0;

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is not authenticated/authentificated must be able to: Browse the catalogue of artists,
	 * display their profiles, see his or her followers and navigate to the albums that he or she have authored.
	 * 
	 * En esta caso de prueba se llevara a cabo la accion de ver los seguidores de un artista. Todos los actores tienen acceso
	 * 
	 * Para fozar el fallo se pueden dar los siguientes casos:
	 * 
	 * - El id del artista indicado no existe
	 */
	public void seeFollowers(final String username, final int actorId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Actor actor;
			final Collection<Actor> artistsFollowingMe = new ArrayList<>();
			final Collection<Actor> usersFollowingMe = new ArrayList<>();
			final Collection<Actor> administratorsFollowingMe = new ArrayList<>();
			final Collection<Actor> managersFollowingMe = new ArrayList<>();
			Collection<Actor> actorsFollowingMe = new ArrayList<>();

			actor = this.actorService.findOne(actorId);
			Assert.notNull(actor);
			actorsFollowingMe = this.actorService.actorsFollowingMe(actor);

			for (final Actor a : actorsFollowingMe)
				if (a instanceof User)
					usersFollowingMe.add(a);
				else if (a instanceof Artist)
					artistsFollowingMe.add(a);
				else if (a instanceof Administrator)
					administratorsFollowingMe.add(a);
				else if (a instanceof Manager)
					managersFollowingMe.add(a);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an artist must be able to: Hire or fire managers
	 * 
	 * En esta caso de prueba se llevara a cabo la accion de contratar a un manager.
	 * 
	 * Para fozar el fallo se pueden dar los siguientes casos:
	 * 
	 * - El id del artista indicado no existe
	 * - El actor logueado no es un artista
	 * - El artista contrata a un manager que ya esta contratado por otro artista
	 * - El artista contrata a un manager cuando ya tiene uno contratado
	 */
	public void hire(final String username, final int artistId, final int managerId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			this.artistService.checkIfArtist();

			Artist artist;
			Manager manager;

			artist = this.artistService.findOne(artistId);
			manager = this.managerService.findOne(managerId);

			Assert.isTrue(artist.getManager() == null);
			Assert.isTrue(manager.getArtist() == null);

			this.artistService.hireManager(artist, manager);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an artist must be able to: Hire or fire managers
	 * 
	 * En esta caso de prueba se llevara a cabo la accion de despedir a un manager.
	 * 
	 * Para fozar el fallo se pueden dar los siguientes casos:
	 * 
	 * - El id del artista indicado no existe
	 * - El actor logueado no es un artista
	 * - El artista despide a un manager que ya esta contratado por otro artista
	 * - El manager despedido no es el mismo que tiene contratado el artist
	 */
	public void fire(final String username, final int artistId, final int managerId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			this.artistService.checkIfArtist();

			Artist artist;
			Manager manager;

			artist = this.artistService.findOne(artistId);
			manager = this.managerService.findOne(managerId);

			Assert.isTrue(artist.getManager() != null);
			Assert.isTrue(manager.getArtist() != null);
			Assert.isTrue(manager.getArtist().getId() == artist.getId());

			this.artistService.fireManager(artist, manager);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void searchArtist() {

		final Object testingData[][] = {
			// B�squeda como no autentificado -> true
			{
				null, "Serj", null
			},
			// B�squeda como admin -> true
			{
				"admin", "Serj", null
			},
			// B�squeda como manager -> true
			{
				"manager1", "Serj", null
			},
			// B�squeda como artista -> true
			{
				"artist1", "Brandon", null
			},
			// B�squeda como usuario -> true
			{
				"user1", "serj000", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.searchArtist((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void registerArtist() {

		final Object testingData[][] = {
			// Creaci�n de artist como autentificado (1) -> false
			{
				"admin", "artistTest1", "artistTest1", "artistTest1", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de artist como autentificado (2) -> false
			{
				"artist1", "artistTest2", "artistTest2", "artistTest2", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de artist como autentificado (3) -> false
			{
				"manager1", "artistTest3", "artistTest3", "artistTest3", "email@domain.com", "678432123", "https://www.picture.com", "password1", "password1", true, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerArtist((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (Boolean) testingData[i][9], (Class<?>) testingData[i][10]);
	}

	@Test
	public void editArtist() {

		final Object testingData[][] = {
			// Edici�n de artist como autentificado (1) -> false
			{
				"admin", "artistTest1", "artistTest1", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de artist como autentificado (2) -> false
			{
				"user1", "artistTest2", "artistTest2", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de artist como autentificado (3) -> false
			{
				"manager1", "artistTest3", "artistTest3", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			// Edici�n de artist como no autentificado -> false
			{
				null, "artistTest7", "artistTest7", "email@domain.com", "678432123", "https://www.picture.com", IllegalArgumentException.class
			},
			//			// Edici�n de artist con phone incorrecto -> false
			//			{
			//				"artist1", "artistTest4", "artistTest4", "email@domain.com", "35353534543534535", "https://www.picture.com", IllegalArgumentException.class
			//			},
			//			// Edici�n de artist con picture incorrecto -> false
			//			{
			//				"artist1", "artistTest5", "artistTest5", "email@domain.com", "678432123", "notanurl", IllegalArgumentException.class
			//			},
			// Edici�n de artist con todo correcto -> true
			{
				"artist1", "artistTest6", "artistTest6", "email@domain.com", "678432123", "https://www.picture.com", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.editArtist((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	@Test
	public void listDriver() {

		final Object testingData[][] = {
			// B�squeda como no autentificado -> true
			{
				null, null
			},
			// B�squeda como admin -> true
			{
				"admin", null
			},
			// B�squeda como manager -> true
			{
				"manager1", null
			},
			// B�squeda como artista -> true
			{
				"artist1", null
			},
			// B�squeda como usuario -> true
			{
				"user1", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.list((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void displayDriver() {

		final Object testingData[][] = {
			// B�squeda como no autentificado -> true
			{
				null, 115, null
			},
			// B�squeda como admin -> true
			{
				"admin", 115, null
			},
			// B�squeda como manager -> true
			{
				"manager1", 115, null
			},
			// B�squeda como artista -> true
			{
				"artist1", 115, null
			},
			// B�squeda como usuario -> true
			{
				"user1", 115, null
			},
			// B�squeda con un id que no existe -> true
			{
				"user1", 4578, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.display((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void seeFollowersDriver() {

		final Object testingData[][] = {
			// B�squeda como no autentificado -> true
			{
				null, 103, null
			},
			// B�squeda como admin -> true
			{
				"admin", 103, null
			},
			// B�squeda como manager -> true
			{
				"manager1", 103, null
			},
			// B�squeda como artista -> true
			{
				"artist1", 103, null
			},
			// B�squeda como usuario -> true
			{
				"user1", 103, null
			},
			// B�squeda con un id que no existe -> true
			{
				"user1", 4578, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.seeFollowers((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void hireDriver() {

		final Object testingData[][] = {
			// Contratar logueado como artista -> true
			{
				"artist4", 115, 117, null
			},
			// Id de un artista que no existe -> false
			{
				"artist4", 1543, 117, IllegalArgumentException.class
			},
			// El actor logueado no es un artista -> false
			{
				"manager1", 115, 117, IllegalArgumentException.class
			},
			// Contratar a un manager que ya esta contratado por otro artist -> false
			{
				"artist4", 115, 109, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.hire((String) testingData[i][0], (int) testingData[i][1], (int) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void fireDriver() {

		final Object testingData[][] = {
			// Despedir logueado como artista -> true
			{
				"artist2", 106, 109, null
			},
			// Id de un artista que no existe -> false
			{
				"artist2", 1543, 109, IllegalArgumentException.class
			},
			// El actor logueado no es un artista -> false
			{
				"manager1", 106, 109, IllegalArgumentException.class
			},
			// Despedir a un manager que no esta contratado -> false
			{
				"artist2", 117, 109, IllegalArgumentException.class
			},
			// Despedir a un manager que no es el que esta contratado -> false
			{
				"artist4", 113, 109, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.fire((String) testingData[i][0], (int) testingData[i][1], (int) testingData[i][2], (Class<?>) testingData[i][3]);
	}
}
