
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Playlist;
import domain.Song;
import domain.User;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class PlaylistServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private PlaylistService	playlistService;
	@Autowired
	private UserService		userService;
	@Autowired
	private SongService		songService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is authenticated as a user must be able to: Manage his playlist,
	 * which include create, listing, modify and delete them. A playlist can be
	 * empty in the moment of the creation.
	 * 
	 * En este caso se llevar� a cabo la acci�n de "listing", por la cual
	 * el actor autentificado como usuario podr� listar todas sus playlist, y adem�s,
	 * al hacer click sobre el nombre de cualquiera de ellas podr� visualizar todas las
	 * canciones que esta contiene. Los siguientes casos ocasionan error en este caso de uso:
	 * 
	 * � No hay actor autentificado
	 * � El actor autentificado no es un usuario
	 */
	public void listMyPlaylists(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que la persona autentificada es User
			this.playlistService.checkIfUser();

			// Inicializamos las playlist del usuario, son las que se mostrar�an
			final User user = this.userService.findByPrincipal();
			user.getPlaylists();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a user must be able to: Manage his playlist,
	 * which include create, listing, modify and delete them. A playlist can be
	 * empty in the moment of the creation.
	 * 
	 * En este caso se llevar� a cabo la acci�n de "edit", por al cual el actor
	 * autentificado como usuario podr� editar una de sus playlist. Se puede forza el error
	 * con las siguientes situaciones:
	 * 
	 * � No hay actor autentificado
	 * � El actor autentificado no es un usuario
	 * � El usuario autentificado no es el creador de la playlist
	 * � La id de la playlist no existe
	 */
	public void editPlaylist(final String username, final Integer playlistId, final String title, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que la persona autentificada es User
			this.playlistService.checkIfUser();

			// Inicializamos la playlist a editar
			final Playlist pl = this.playlistService.findOne(playlistId);

			// Inicializamos atributos a editar y se guarda
			pl.setTitle(title);

			this.playlistService.save(pl);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a user must be able to: Manage his playlist,
	 * which include create, listing, modify and delete them. A playlist can be
	 * empty in the moment of the creation.
	 * 
	 * En este caso se llevar� a cabo la acci�n de "edit", por al cual el actor
	 * autentificado como usuario podr� editar una de sus playlist. Se puede forza el error
	 * con las siguientes situaciones:
	 * 
	 * � No hay actor autentificado
	 * � El actor autentificado no es un usuario
	 * � El usuario autentificado no es el creador de la playlist
	 * � La id de la playlist no existe
	 */
	public void deletePlaylist(final String username, final Integer playlistId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que la persona autentificada es User
			this.playlistService.checkIfUser();

			// Inicializamos la playlist a eliminar
			final Playlist pl = this.playlistService.findOne(playlistId);

			// Eliminamos
			this.playlistService.delete(pl);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a user must be able to: Manage his playlist,
	 * which include create, listing, modify and delete them. A playlist can be
	 * empty in the moment of the creation.
	 * 
	 * En este caso se llevar� a cabo la acci�n de "create", por al cual el actor
	 * autentificado como usuario podr� crear una playlist nueva. Esta playlist estar� vac�a
	 * en un principio.
	 * 
	 * � No hay actor autentificado
	 * � El actor autentificado no es un usuario
	 */
	public void createPlaylist(final String username, final String title, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que la persona autentificada es User
			this.playlistService.checkIfUser();

			// Inicializamos la playlist a crear
			final Playlist pl = this.playlistService.create();
			pl.setTitle(title);

			// Guardamos
			this.playlistService.save(pl);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a user must be able to: Delete songs
	 * from one of his or her playlists.
	 * 
	 * En este caso se llevar� a cabo la acci�n de eliminar una caci�n de una de las
	 * playlist de un actor autenficado como usuario. Puede forzar el error:
	 * 
	 * � No hay actor autentificado
	 * � El actor autentificado no es un usuario
	 * � La id de la canci�n no existe
	 */
	public void addSongToPlaylist(final String username, final int songId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que la persona autentificada es User
			this.playlistService.checkIfUser();

			// Inicializamos la canci�n a a�adir
			final Song s = this.songService.findOne(songId);

			// A�adimos la canci�n a la playlist (autom�ticamente se deduce cual es la playlist)
			this.playlistService.addSongToPlaylist(s);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a user must be able to: Delete songs
	 * from one of his or her playlists.
	 * 
	 * En este caso se llevar� a cabo la acci�n de eliminar una caci�n de una de las
	 * playlist de un actor autenficado como usuario. Puede forzar el error:
	 * 
	 * � No hay actor autentificado
	 * � El actor autentificado no es un usuario
	 * � La id de la canci�n o de la playlist no existen
	 * � El usuario autentificado no es el creador de la playlist
	 * � Eliminar canci�n no existente en la playlist
	 */
	public void deleteSongFromPlaylist(final String username, final int playlistId, final int songId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que la persona autentificada es User
			this.playlistService.checkIfUser();

			// Inicializamos la canci�n y la playlist
			final Song s = this.songService.findOne(songId);
			final Playlist pl = this.playlistService.findOne(playlistId);

			// Eliminamos la canci�n
			this.playlistService.deleteSong(pl, s);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated must be able to: search playlists by a keyword
	 * which must appear in its titles
	 * 
	 * En este caso se llevar� a cabo la acci�n listar todas las canciones del sistema
	 * (menos las del usuario autentificado, si es que es un usuario) y a continuaci�n
	 * buscar una canci�n por una o varias palabras mediante un buscador, mostrandose el resultado
	 * de la b�squeda a continuaci�n. No existen situaciones en la que se pueda forzar el error, puesto que
	 * todos los actores pueden realizar esta acci�n y no intervienen ids ni hay restricciones en la
	 * palabra clave utilizada en la b�squeda.
	 */
	public void searchPlaylist(final String username, final String keyword, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Listamos todas las playlist (menos las de usuario autentificiado, si es que lo es)
			this.playlistService.findAll();

			// Introducimos la "keyword" y mostramos los resultados
			this.playlistService.searchPlaylistByKeyword(keyword);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListMyPlaylists() {

		final Object testingData[][] = {
			// Acceder a listado de playlist propias como no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
			// Acceder a listado de playlist propias como admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// Acceder a listado de playlist propias como manager -> false
			{
				"manager1", IllegalArgumentException.class
			},
			// Acceder a listado de playlist propias como artist -> false
			{
				"artist1", IllegalArgumentException.class
			},
			// Acceder a listado de playlist como user -> true
			{
				"user1", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listMyPlaylists((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverEditPlaylist() {

		final Object testingData[][] = {
			// Editar playlist como no autentificado -> false
			{
				null, 161, "Example title", IllegalArgumentException.class
			},
			// Editar playlist como admin -> false
			{
				"admin", 161, "Example title", IllegalArgumentException.class
			},
			// Editar playlist como manager -> false
			{
				"manager1", 161, "Example title", IllegalArgumentException.class
			},
			// Editar playlist como artist -> false
			{
				"artist1", 161, "Example title", IllegalArgumentException.class
			},
			// Editar playlist como id inexistente -> false
			{
				"user1", 2222, "Example title", IllegalArgumentException.class
			},
			// Editar playlist user distinto al creador de la playlist -> false
			{
				"user3", 161, "Example title", IllegalArgumentException.class
			},
			// Editar playlist como user con todo correcto -> true
			{
				"user1", 161, "Example title", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.editPlaylist((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void driverDeletePlaylist() {

		final Object testingData[][] = {
			// Eliminar playlist como no autentificado -> false
			{
				null, 161, IllegalArgumentException.class
			},
			// Eliminar playlist como admin -> false
			{
				"admin", 161, IllegalArgumentException.class
			},
			// Eliminar playlist como manager -> false
			{
				"manager1", 161, IllegalArgumentException.class
			},
			// Eliminar playlist como artist -> false
			{
				"artist1", 161, IllegalArgumentException.class
			},
			// Eliminar playlist como user -> true
			{
				"user1", 161, null
			},
			// Eliminar playlist como con id inexistente -> false
			{
				"user1", 2222, IllegalArgumentException.class
			},
			// Eliminar playlist como user no creador de la misma -> false
			{
				"user2", 161, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.deletePlaylist((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverCreatePlaylist() {

		final Object testingData[][] = {
			// Crear playlist como no autentificado -> false
			{
				null, "New title", IllegalArgumentException.class
			},
			// Crear playlist como admin -> false
			{
				"admin", "New title", IllegalArgumentException.class
			},
			// Crear playlist como manager -> false
			{
				"manager1", "New title", IllegalArgumentException.class
			},
			// Crear playlist como artist -> false
			{
				"artist1", "New title", IllegalArgumentException.class
			},
			// Crear playlist como user con todo correcto -> true
			{
				"user1", "New title", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createPlaylist((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverAddSongToPlaylist() {

		final Object testingData[][] = {
			// A�adir canci�n a playlist como no autentificado -> false
			{
				null, 153, IllegalArgumentException.class
			},
			// A�adir canci�n a playlist como admin -> false
			{
				"admin", 153, IllegalArgumentException.class
			},
			// A�adir canci�n a playlist como manager -> false
			{
				"manager1", 153, IllegalArgumentException.class
			},
			// A�adir canci�n a playlist como artist -> false
			{
				"artist1", 153, IllegalArgumentException.class
			},
			// A�adir canci�n a playlist usuario -> true
			{
				"user1", 153, null
			},
			// A�adir canci�n a playlist con id de canci�n inexistente -> false
			{
				"user1", 32133, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.addSongToPlaylist((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void driverDeleteSongFromPlaylist() {

		final Object testingData[][] = {
			// Eliminar canci�n de playlist como no autentificado -> false
			{
				null, 161, 119, IllegalArgumentException.class
			},
			// Eliminar canci�n de playlist como admin -> false
			{
				"admin", 161, 119, IllegalArgumentException.class
			},
			// Eliminar canci�n de playlist como manager -> false
			{
				"manager1", 161, 119, IllegalArgumentException.class
			},
			// Eliminar canci�n de playlist como artist -> false
			{
				"artist1", 161, 119, IllegalArgumentException.class
			},
			// Eliminar canci�n de playlist usuario -> true
			{
				"user1", 161, 119, null
			},
			// Eliminar canci�n de playlist que no est� en la playlist -> true
			{
				"user1", 161, 156, IllegalArgumentException.class
			},
			// Eliminar canci�n como usuario no creador de la playlist -> false
			{
				"user3", 161, 119, IllegalArgumentException.class
			},
			// Eliminar canci�n con id de canci�n inexistente -> false
			{
				"user1", 161, 5555, IllegalArgumentException.class
			},
			// Eliminar canci�n con id de playlist inexistente -> false
			{
				"user1", 63412, 119, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteSongFromPlaylist((String) testingData[i][0], (Integer) testingData[i][1], (Integer) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void searchPlaylists() {

		final Object testingData[][] = {
			// B�squeda como no autentificado -> true
			{
				null, "My best", null
			},
			// B�squeda como admin -> true
			{
				"admin", "My best", null
			},
			// B�squeda como manager -> true
			{
				"manager1", "My best", null
			},
			// B�squeda como artista -> true
			{
				"artist1", "My best", null
			},
			// B�squeda como usuario -> true
			{
				"user1", "My best", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.searchPlaylist((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
