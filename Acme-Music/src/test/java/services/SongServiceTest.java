
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Song;
import domain.User;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class SongServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private SongService				songService;
	@Autowired
	private UserService				userService;
	@Autowired
	private AlbumService			albumService;
	@Autowired
	private ArtistService			artistService;
	@Autowired
	private ActorService			actorService;
	@Autowired
	private AdministratorService	administratorService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is not authenticated/authentificated must be able to:
	 * Search for a song by a single keyword that must be contained in its name or genre.
	 * 
	 * En este caso se llevar� a cabo la acci�n de listar todas las canciones del sistema
	 * y mediante un buscador introducir una o varias palabras clave y mostrar
	 * por pantalla los resultados respecto a esa/s palabra/s clave/s. Todos los actores
	 * pueden acceder a ello, no existen restricciones en la palabra clave y no participa
	 * ninguna id por lo que no existen situaciones que puedan forzar el error.
	 */
	public void searchSong(final String username, final String keyword, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Listamos todas las playlist (menos las de usuario autentificiado, si es que lo es)
			this.songService.findAll();

			// Introducimos la "keyword" y mostramos los resultados
			this.songService.searchSongByKeyword(keyword);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a user must be able to: List the songs
	 * that he or she has marked as favorite.
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de visualizar las canciones favoritas
	 * de un actor autentificado como usuario. Varias situaciones en las que se fuerza el error son:
	 * 
	 * � No hay actor autentificado
	 * � El actor autentificado no es un usuario
	 */
	public void listFavoriteSongs(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobar que se esta autentificado como User
			this.userService.checkIfUser();

			// Inicializar las canciones del usuario autentificado
			final User user = this.userService.findByPrincipal();
			this.songService.getFavoritesSongs(user);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a user must be able to: List the songs
	 * that he or she has create.
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de visualizar las canciones de un solo artist
	 * de un actor autentificado como usuario. Varias situaciones en las que se fuerza el error son:
	 * 
	 * � No hay actor autentificado
	 * � El actor autentificado no es un artist
	 */
	public void listMySongs(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobar que se esta autentificado como Artist

			// Inicializar las canciones del usuario autentificado
			this.albumService.getSongsOfMyAlbums();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	//Crear canci�n como artist, fallar� al intentarlo como user, manager, admin o no autenticado.
	public void createSong(final String username, final String name, final String genre, final Integer minutes, final Integer seconds, final boolean explicit, final boolean banned, final String soundCloudURL, final String youTubeURL,
		final Integer visualizations, final boolean private_, final Integer favs, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si es artist
			this.artistService.checkIfArtist();

			// Creamos un songo e inicializamos atributos y salvamos
			// Al crear un songo automaticamente se genera un fee
			final Song song = this.songService.create();

			song.setName(name);
			song.setGenre(genre);
			song.setMinutes(minutes);
			song.setSeconds(seconds);
			song.setExplicit(explicit);
			song.setBanned(banned);
			song.setSoundCloudURL(soundCloudURL);
			song.setYouTubeURL(youTubeURL);
			song.setPrivate_(private_);
			song.setFavs(favs);
			song.setVisualizations(visualizations);

			this.songService.saveCreate(song);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a user must be able to: Mark as favorite a song of
	 * some artist, or stop marking it like favorite.
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de marcar favorita una canci�n, en el caso
	 * de que "status" sea true, y la de eliminar de favoritos una canci�n, en el caso de que
	 * "status" sea false. S�lo los actores autentificados como usuarios pueden acceder a esta acci�n.
	 * Varias situaciones donde el error puede mostrarse son:
	 * 
	 * � No hay actor autentificado
	 * � El actor autentificado no es un usuario
	 * � La id de la canci�n no existe
	 * � En el caso de marcar favorita una canci�n, la canci�n ya estaba marcada como tal
	 * � En el caso de eliminar una cacni�n de favoritos, la canci�n no estaba en favoritos
	 */
	public void favoriteSong(final String username, final int songId, final Boolean status, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que el actor autentificado es un usuario
			this.userService.checkIfUser();

			// Inicializamos la canci�n con la que vamos a tratar
			final Song s = this.songService.findOne(songId);

			// Si status es true, la a�adimos a favoritos
			if (status)
				this.songService.addFavorite(s);

			// Si status es false, la eliminamos de favoritos
			if (!status)
				this.songService.deleteFavorite(s);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a admin must be able to: Mark as explicit a song of
	 * some artist, or stop marking it like explicit.
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de marcar como expl�cita una canci�n, en el caso
	 * de que "status" sea true, y la de marcar como no expl�cita una canci�n, en el caso de que
	 * "status" sea false. S�lo los actores autentificados como administradores pueden acceder a esta acci�n.
	 * 
	 * Deber� fallar cuando:
	 * � El actor no sea un administrador
	 * � La canci�n ya sea expl�cita o no expl�cita
	 * � La id no sea de una canci�n
	 */
	public void makeSongExplicitOrNotExplicit(final String username, final int songId, final Boolean status, final Class<?> expected) {

		Class<?> caught = null;

		try {

			// Autentificamos al actor
			this.authenticate(username);

			// Si status es true, la ponemos como expl�cita
			if (status)
				this.songService.makeExplicit(songId);

			// Si status es false, la ponemos como no expl�cita
			if (!status)
				this.songService.makeNotExplicit(songId);

			// Desautentificamos
			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an administrator must be able to: Ban or unban songs, comments,
	 * users and artists if they considered so. If its a song or a comment what is banned, they wont
	 * be shown in any place; if its a user or and artists, they wont be able to access to the system
	 * and will disappear wherever they were; if its an actor, that means all his managers are also banned.
	 * 
	 * En este caso de prueba se realizara la accion de banear una cancion.
	 * 
	 * Para forzar el error se daran las siguientes situaciones:
	 * 
	 * - El usuario no esta autentficado
	 * - El usuario autentificado no es un admin
	 * - El id de la cancion no existe
	 * - Se intenta banear una cancion ya baneada
	 */
	public void banSong(final String username, final int songId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			// Autentificamos al actor
			this.authenticate(username);

			this.actorService.checkIfAdministrator();

			final Song song = this.songService.findOne(songId);
			this.administratorService.ban(song);

			// Desautentificamos
			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an administrator must be able to: Ban or unban songs, comments,
	 * users and artists if they considered so. If its a song or a comment what is banned, they wont
	 * be shown in any place; if its a user or and artists, they wont be able to access to the system
	 * and will disappear wherever they were; if its an actor, that means all his managers are also banned.
	 * 
	 * En este caso de prueba se realizara la accion de desbanear una cancion.
	 * 
	 * Para forzar el error se daran las siguientes situaciones:
	 * 
	 * - El usuario no esta autentficado
	 * - El usuario autentificado no es un admin
	 * - El id de la cancion no existe
	 * - Se intenta desbanear una cancion no baneada
	 */
	public void unBanSong(final String username, final int songId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			// Autentificamos al actor
			this.authenticate(username);

			this.actorService.checkIfAdministrator();

			final Song song = this.songService.findOne(songId);

			this.administratorService.unban(song);

			// Desautentificamos
			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an artist must be able to: Manage his or her songs, which includes authoring,
	 * listing, modifying and deleting them. A song can have no album at first.
	 * 
	 * En este caso de prueba se llevara a cabo la accion de borrar una cancion.
	 * 
	 * Para forzar el error se daran las siguientes situaciones:
	 * 
	 * - El usuario no esta autentificado
	 * - El usuario autentificado no es un artist
	 * - El id de la cancion no existe
	 * - La cancion no pertenece al artista logueado
	 */
	public void deleteSong(final String username, final int songId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si es artist
			this.artistService.checkIfArtist();

			// Creamos un songo e inicializamos atributos y salvamos
			// Al crear un songo automaticamente se genera un fee
			final Song song = this.songService.findOne(songId);

			this.songService.delete(song);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ------------------------------------------------------------------------------

	@Test
	public void createSong() {

		final Object testingData[][] = {
			// Crear song con usuario no autentificado -> false
			{
				null, "name", "genre", 3, 4, false, false, "https://soundcloud.com", "https://youtube.com", 2, false, 4, IllegalArgumentException.class
			},
			// Crear cancion siendo user, false
			{
				"user1", "name", "genre", 3, 4, false, false, "https://soundcloud.com", "https://youtube.com", 2, false, 4, IllegalArgumentException.class
			},
			// Crear cancion siendo manager, false
			{
				"manager1", "name", "genre", 3, 4, false, false, "https://soundcloud.com", "https://youtube.com", 2, false, 4, IllegalArgumentException.class
			},
			// Crear cancion siendo admin, false
			{
				"admin", "name", "genre", 3, 4, false, false, "https://soundcloud.com", "https://youtube.com", 2, false, 4, IllegalArgumentException.class
			},
			// Crear cancion siendo artist, true
			{
				"artist1", "name", "genre", 3, 4, false, false, "https://soundcloud.com", "https://youtube.com", 2, false, 4, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createSong((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Integer) testingData[i][3], (Integer) testingData[i][4], (Boolean) testingData[i][5], (Boolean) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (Integer) testingData[i][9], (Boolean) testingData[i][10], (Integer) testingData[i][11], (Class<?>) testingData[i][12]);
	}

	@Test
	public void listMySongs() {

		final Object testingData[][] = {
			// B�squeda como no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
			// B�squeda como admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// B�squeda como manager -> false
			{
				"manager1", IllegalArgumentException.class
			},
			// B�squeda como artista -> true
			{
				"artist1", null
			},
			// B�squeda como usuario (1) -> true
			{
				"user1", IllegalArgumentException.class
			},
			// B�squeda como usuario (2) -> true
			{
				"user2", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listMySongs((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void listFavoriteSongs() {

		final Object testingData[][] = {
			// B�squeda como no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
			// B�squeda como admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// B�squeda como manager -> false
			{
				"manager1", IllegalArgumentException.class
			},
			// B�squeda como artista -> false
			{
				"artist1", IllegalArgumentException.class
			},
			// B�squeda como usuario (1) -> true
			{
				"user1", null
			},
			// B�squeda como usuario (2) -> true
			{
				"user2", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listFavoriteSongs((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void searchSong() {

		final Object testingData[][] = {
			// B�squeda como no autentificado -> true
			{
				null, "Empty Walls", null
			},
			// B�squeda como admin -> true
			{
				"admin", "Empty Walls", null
			},
			// B�squeda como manager -> true
			{
				"manager1", "OTHER", null
			},
			// B�squeda como artista -> true
			{
				"artist1", "Empty Walls", null
			},
			// B�squeda como usuario -> true
			{
				"user1", "OTHER", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.searchSong((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void favoriteSong() {

		final Object testingData[][] = {
			// Marcar como favorita una canci�n como no autentificado -> false
			{
				null, 157, true, IllegalArgumentException.class
			},
			// Marcar como favorita una canci�n como no admin -> false
			{
				"admin", 157, true, IllegalArgumentException.class
			},
			// Marcar como favorita una canci�n como artist -> false
			{
				"artist1", 157, true, IllegalArgumentException.class
			},
			// Marcar como favorita una canci�n como manager -> false
			{
				"manager1", 157, true, IllegalArgumentException.class
			},
			// Marcar como favorita estando ya marcada como favorita -> false
			{
				"user1", 119, true, IllegalArgumentException.class
			},
			// Marcar como favorita una canci�n con id no existente -> false
			{
				"user1", 23812, true, IllegalArgumentException.class
			},
			// Marcar como favorita una canci�n con todo correcto -> true
			{
				"user1", 157, true, null
			},
			// Desmarcar como favorita una canci�n como no autentificado -> false
			{
				null, 120, false, IllegalArgumentException.class
			},
			// Desmarcar como favorita una canci�n como no admin -> false
			{
				"admin", 120, false, IllegalArgumentException.class
			},
			// Desmarcar como favorita una canci�n como artist -> false
			{
				"artist1", 120, false, IllegalArgumentException.class
			},
			// Desmarcar como favorita una canci�n como manager -> false
			{
				"manager1", 120, false, IllegalArgumentException.class
			},
			// Desmarcar como favorita estando no estando marcada como favorita -> false
			{
				"user1", 159, false, IllegalArgumentException.class
			},
			// Desmarcar como favorita una canci�n con id no existente -> false
			{
				"user1", 32131, false, IllegalArgumentException.class
			},
			// Desmarcar como favorita una canci�n con todo correcto -> true
			{
				"user1", 120, false, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.favoriteSong((String) testingData[i][0], (int) testingData[i][1], (Boolean) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void makeSongExplicitOrNotExplicit() {

		final Object testingData[][] = {
			// Marcar como expl�cita una canci�n como no autentificado -> false
			{
				null, 152, true, IllegalArgumentException.class
			},
			// Marcar como expl�cita una canci�n como user -> false
			{
				"user1", 152, true, IllegalArgumentException.class
			},
			// Marcar como expl�cita una canci�n como artist -> false
			{
				"artist1", 152, true, IllegalArgumentException.class
			},
			// Marcar como expl�cita una canci�n como manager -> false
			{
				"manager1", 152, true, IllegalArgumentException.class
			},
			// Marcar como expl�cita estando ya marcada como expl�cita -> false
			{
				"admin", 160, true, IllegalArgumentException.class
			},
			// Marcar como expl�cita una canci�n con id no existente -> false
			{
				"admin", 23812, true, IllegalArgumentException.class
			},
			// Marcar como expl�cita una canci�n con todo correcto -> true
			{
				"admin", 152, true, null
			},
			// Marcar como no expl�cita una canci�n como no autentificado -> false
			{
				null, 160, false, IllegalArgumentException.class
			},
			// Marcar como no expl�cita una canci�n como user -> false
			{
				"user1", 160, false, IllegalArgumentException.class
			},
			// Marcar como no expl�cita una canci�n como artist -> false
			{
				"artist1", 160, false, IllegalArgumentException.class
			},
			// Marcar como no expl�cita una canci�n como manager -> false
			{
				"manager1", 160, false, IllegalArgumentException.class
			},
			// Marcar como no expl�cita estando no estando marcada como no expl�cita -> false
			{
				"admin", 153, false, IllegalArgumentException.class
			},
			// Marcar como no expl�cita una canci�n con id no existente -> false
			{
				"admin", 32131, false, IllegalArgumentException.class
			},
			// Marcar como no expl�cita una canci�n con todo correcto -> true
			{
				"admin", 160, false, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.makeSongExplicitOrNotExplicit((String) testingData[i][0], (int) testingData[i][1], (Boolean) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void banSongDriver() {

		final Object testingData[][] = {
			// Banear una cancion logueado como admin -> true
			{
				"admin", 119, null
			},
			// Banear una cancion sin loguearse -> false
			{
				null, 119, IllegalArgumentException.class
			},
			// Banear una cancion logueado como artist -> false
			{
				"artist1", 119, IllegalArgumentException.class
			},
			// Banear una cancion con un id que no existe -> false
			{
				"admin", 7569, IllegalArgumentException.class
			},
			// Banear una cancion ya baneada -> false
			{
				"admin", 120, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.banSong((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void unBanSongDriver() {

		final Object testingData[][] = {
			// Desbanear una cancion logueado como admin -> true
			{
				"admin", 120, null
			},
			// Desbanear una cancion sin loguearse -> false
			{
				null, 120, IllegalArgumentException.class
			},
			// Desbanear una cancion logueado como artist -> false
			{
				"artist1", 120, IllegalArgumentException.class
			},
			// Desbanear una cancion con un id que no existe -> false
			{
				"admin", 7569, IllegalArgumentException.class
			},
			// Desbanear una cancion no baneada -> false
			{
				"admin", 124, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.unBanSong((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void deleteDriver() {

		final Object testingData[][] = {
			// Borrar una cancion logueado como artist -> true
			{
				"artist2", 119, null
			},
			// Borrar una cancion sin loguearse -> false
			{
				null, 119, IllegalArgumentException.class
			},
			// Borrar una cancion logueado como artist -> false
			{
				"manager1", 119, IllegalArgumentException.class
			},
			// Borrar una cancion con un id que no existe -> false
			{
				"artist2", 7569, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteSong((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
