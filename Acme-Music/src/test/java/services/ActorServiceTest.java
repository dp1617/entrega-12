
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Artist;
import domain.User;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ActorServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ActorService	actorService;
	@Autowired
	private AlbumService	albumService;
	@Autowired
	private ArtistService	artistService;
	@Autowired
	private UserService		userService;


	// Templates --------------------------------------------------------------

	/*
	 * An actor who is authenticated as a user must be able to: Follow/Unfollow an artist
	 * or a user that he or she is not currently following.
	 * 
	 * En este caso se llevar� a cabo la acci�n seguir (follow=true) a un usuario (idArtist==null && idUser!=null)
	 * o a un artista (idArtist!=null && idUser==null) por parte de un actor autentificado.
	 * Tambi�n se testear� la acci�n de dejar de seguir a un usuario o artista(follow=false).
	 * Varias situaciones pueden llevar al error:
	 * 
	 * � No hay actor autentificado
	 * � La id del usuario no existe
	 * � La id del artista no existe
	 * � Un usuario se sigue a s� mismo
	 * � Un artiste se sigue a s� mismo
	 * � A la hora de seguir, el usuario/artista ya estaba siendo seguido
	 * � A la hora de dejar de seguir, el usuario/artista no estaba siendo seguido
	 */
	public void followUnfollowActor(final String username, final Integer userId, final Integer artistId, final Boolean follow, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que est� autentificado
			Assert.isTrue(username != null);

			// Si se quiere seguir a un artista
			if (artistId != null && userId == null && follow == true) {
				final Actor a = this.actorService.findOne(artistId);
				this.actorService.follow(a);
			}

			// Si se quiere seguir a un usuario
			if (artistId == null && userId != null && follow == true) {
				final Actor a = this.actorService.findOne(userId);
				this.actorService.follow(a);
			}

			// Si se quiere dejar de seguir a un artista
			if (artistId != null && userId == null && follow == false) {
				final Actor a = this.actorService.findOne(artistId);
				this.actorService.unfollow(a);
			}

			// Si se quiere dejar de seguir a un usuario
			if (artistId == null && userId != null && follow == false) {
				final Actor a = this.actorService.findOne(userId);
				this.actorService.unfollow(a);
			}

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated must be able to: List the artists/users that he or she
	 * is currently following and navigate to their albums in the case of artist and to the
	 * playlists in the case of users.
	 * 
	 * En este caso se llevar� a cabo la acci�n de listar los usuarios y artistas que un actor
	 * autentificado como usuario sigue, adem�s de navegar a las playlists si se trata de un usuario
	 * y de navegar a los �lbumes si se trata de un artista. Errores pueden surgir si:
	 * 
	 * � No hay actor autentificado
	 * � En el caso de visualizar los albums del artista, no existe id del artista
	 * � En el caso de visualizar las playlist del usuario, no existe la id del usuario
	 */
	public void listActorsImFollowing(final String username, final Integer userId, final Integer artistId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que est� autentificado
			Assert.isTrue(username != null);

			// Se muestra el listado de usuarios y artistas a los que sigo
			final Actor actor = this.actorService.findByPrincipal();
			actor.getActorsImFollowing();

			// Si se visualiza los �lbumes de un artista
			if (artistId != null && userId == null) {
				final Artist a = this.artistService.findOne(artistId);
				this.albumService.getArtistAlbums(a);
			}

			// Si se visualiza las paylists de un usuario
			if (artistId == null && userId != null) {
				final User u = this.userService.findOne(userId);
				u.getPlaylists();
			}

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a user must be able to: Mark as favorite a song
	 * of some artist, or stop marking it like favorite and navigate to their albums in
	 * the case of artist and to the playlists in the case of users.
	 * 
	 * En este caso se llevar� a cabo la acci�n de listar los actores que siguen a un actor
	 * autentificado como usuario o artista, adem�s de navegar a las playlists si se trata de un usuario
	 * y de navegar a los �lbumes si se trata de un artista. Errores pueden surgir si:
	 * 
	 * � No hay actor autentificado
	 * � El actor autentificado no es un usuario o un artista
	 * � En el caso de visualizar los albums del artista, no existe id del artista
	 * � En el caso de visualizar las playlist del usuario, no existe la id del usuario
	 */
	public void listActorsFollowingMe(final String username, final Integer userId, final Integer artistId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que est� autentificado
			Assert.isTrue(username != null);

			// Se muestra el listado de usuarios y artistas a los que sigo
			final Actor actor = this.actorService.findByPrincipal();

			Assert.isTrue(actor instanceof User || actor instanceof Artist, "No puedes tener seguidores");

			actor.getActorsFollowingMe();

			// Si se visualiza los �lbumes de un artista
			if (artistId != null && userId == null) {
				final Artist a = this.artistService.findOne(artistId);
				this.albumService.getArtistAlbums(a);
			}

			// Si se visualiza las paylists de un usuario
			if (artistId == null && userId != null) {
				final User u = this.userService.findOne(userId);
				u.getPlaylists();
			}

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListActorsFollowingMe() {

		final Object testingData[][] = {
			// Visualizar usuarios/artistas que me siguen sin estar autentificado -> false
			{
				null, null, null, IllegalArgumentException.class
			},
			// Visualizar usuarios/artistas que me siguen como admin -> false
			{
				"admin", null, null, IllegalArgumentException.class
			},
			// Visualizar usuarios/artistas que me siguen como manager -> false
			{
				"manager1", null, null, IllegalArgumentException.class
			},
			// Visualizar usuarios/artistas me siguen (vale para usuario y artista) -> true
			{
				"user1", null, null, null
			},
			// Visualizar usuarios/artistas me siguen (vale para usuario y artista) y navegar 
			// hacia los �lbumes de los artistas -> true
			{
				"user1", null, 115, null
			},
			// Visualizar usuarios/artistas que sigo (vale para usuario y artista) y navegar 
			// hacia los �lbumes de los artistas con id de artista incorrecto -> false
			{
				"user1", null, 32132, IllegalArgumentException.class
			},
			// Visualizar usuarios/artistas que sigo (vale para usuario y artista) y navegar 
			// hacia las playlists de los usuarios -> true
			{
				"user1", 105, null, null
			},
			// Visualizar usuarios/artistas que sigo (vale para todos los actores) y navegar 
			// hacia las playlists de los usuarios con id de usuario incorrecto -> false
			{
				"user1", 12332, null, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listActorsFollowingMe((String) testingData[i][0], (Integer) testingData[i][1], (Integer) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void driverListActorsImFollowing() {

		final Object testingData[][] = {
			// Visualizar usuarios/artistas que sigo sin estar autentificado -> false
			{
				null, null, null, IllegalArgumentException.class
			},
			// Visualizar usuarios/artistas que sigo (vale para todos los actores) -> true
			{
				"user1", null, null, null
			},
			// Visualizar usuarios/artistas que sigo (vale para todos los actores) y navegar 
			// hacia los �lbumes de los artistas -> true
			{
				"user1", null, 110, null
			},
			// Visualizar usuarios/artistas que sigo (vale para todos los actores) y navegar 
			// hacia los �lbumes de los artistas con id de artista incorrecto -> false
			{
				"user1", null, 32132, IllegalArgumentException.class
			},
			// Visualizar usuarios/artistas que sigo (vale para todos los actores) y navegar 
			// hacia las playlists de los usuarios -> true
			{
				"user1", 105, null, null
			},
			// Visualizar usuarios/artistas que sigo (vale para todos los actores) y navegar 
			// hacia las playlists de los usuarios con id de usuario incorrecto -> false
			{
				"user1", 12332, null, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listActorsImFollowing((String) testingData[i][0], (Integer) testingData[i][1], (Integer) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	@Test
	public void followUnfollowActor() {

		final Object testingData[][] = {
			// Seguir como no autentificado -> false
			{
				null, 108, null, true, IllegalArgumentException.class
			},
			// Dejar de seguir como no autentificado -> false
			{
				null, 108, null, false, IllegalArgumentException.class
			},
			// Seguir a si mismo siendo un usuario -> false
			{
				"user1", 104, null, true, IllegalArgumentException.class
			},
			// Seguir a un usuario ya seguido -> false
			{
				"user1", 105, null, true, IllegalArgumentException.class
			},
			// Seguir a un usuario con todo correcto -> true
			{
				"user1", 116, null, true, null
			},
			// Seguir con una id de usuario inexistente -> false
			{
				"user1", 6345, null, true, IllegalArgumentException.class
			},
			// Dejar de seguir a si mismo siendo un usuario -> false
			{
				"user1", 104, null, false, IllegalArgumentException.class
			},
			// Dejar de seguir a un usuario no seguido -> false
			{
				"user3", 105, null, false, IllegalArgumentException.class
			},
			// Dejar de seguir a un usuario con todo correcto -> true
			{
				"user1", 105, null, false, null
			},
			// Dejar de seguir con una id de usuario inexistente -> false
			{
				"user1", 6345, null, false, IllegalArgumentException.class
			},
			// Dejar de seguir a un artista no seguido -> false
			{
				"user3", null, 111, false, IllegalArgumentException.class
			},
			// Seguir a un artista ya seguido -> false
			{
				"user1", null, 111, true, IllegalArgumentException.class
			},
			// Dejar de seguir a un artista con todo correcto -> true
			{
				"user1", null, 110, false, null
			},
			// Dejar de seguir con una id de artista inexistente -> false
			{
				"user1", null, 321312, false, IllegalArgumentException.class
			},
			// Seguir a si mismo siendo un artista -> false
			{
				"artist1", null, 106, true, IllegalArgumentException.class
			},
			// Seguir a un artista con todo correcto -> true
			{
				"user3", null, 106, true, null
			},
			// Seguir con una id de artista inexistente -> false
			{
				"user1", null, 13123, true, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.followUnfollowActor((String) testingData[i][0], (Integer) testingData[i][1], (Integer) testingData[i][2], (Boolean) testingData[i][3], (Class<?>) testingData[i][4]);
	}
}
