
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Parameters;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ParametersServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ParametersService	parametersService;
	@Autowired
	private ActorService		actorService;


	// Templates --------------------------------------------------------------

	/*
	 * "An actor who is authenticated as a administrator must be able to:
	 * - Add or remove genres to the system"
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de editar los genres del parameters.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona autentificada no es un admin
	 * � La persona no est� autentificada
	 */
	public void editParameters(final String username, final Collection<String> genres, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que sea un admin
			this.actorService.checkIfAdministrator();

			// Inicializamos los atributos para la edici�n
			Parameters parameters;
			parameters = this.parametersService.getParameters();

			parameters.setGenres(genres);

			//Guardamos
			this.parametersService.save(parameters);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	// Drivers ----------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Test
	public void editParameters() {

		final Collection<String> genres = new ArrayList<String>();
		genres.add("BLUES");
		genres.add("JAZZ");
		genres.add("METAL");
		genres.add("ROCK");
		genres.add("OTHER");

		final Object testingData[][] = {
			// Edici�n de parameters como autentificado (1) -> false
			{
				"artist1", genres, IllegalArgumentException.class
			},
			// Edici�n de parameters como autentificado (2) -> false
			{
				"user1", genres, IllegalArgumentException.class
			},
			// Edici�n de parameters como autentificado (3) -> false
			{
				"manager1", genres, IllegalArgumentException.class
			},
			// Edici�n de parameters como no autentificado -> false
			{
				null, genres, IllegalArgumentException.class
			},
			// Edici�n de parameters con todo correcto -> true
			{
				"admin", genres, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.editParameters((String) testingData[i][0], (Collection<String>) testingData[i][1], ((Class<?>) testingData[i][2]));
	}
}
