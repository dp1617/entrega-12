
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Actor;
import domain.Artist;
import domain.Event;
import domain.EventsCalendar;
import domain.Manager;
import domain.User;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class EventServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private EventService			eventService;
	@Autowired
	private ActorService			actorService;
	@Autowired
	private UserService				userService;
	@Autowired
	private ManagerService			managerService;
	@Autowired
	private EventsCalendarService	eventsCalendarService;


	// Templates --------------------------------------------------------------

	/*
	 * Listar eventos por usuario
	 * Se fuerza el error no siendo usuario o no siendo usuario autentificado.
	 */
	public void listEvents(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			if (username != null)
				this.actorService.authenticated();
			else
				Assert.isTrue(username == null);

			this.userService.checkIfUser();
			// Visualizamos el listado de eventos
			this.eventService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * Listar eventos por usuario a partir de los artistas a los que sigue
	 * Se fuerza el error no siendo usuario o no siendo usuario autentificado.
	 */
	public void listEventsByArtists(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			if (username != null)
				this.actorService.authenticated();
			else
				Assert.isTrue(username == null);

			// Cargamos los eventos filtrado por artistas que sigo
			final User u = this.userService.findByPrincipal();
			this.userService.checkIfUser();

			this.eventService.findEventsByArtistsIFollow(u);

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * Unirse a un evento para asistir a el (usuario)
	 * Se fuerza el error no siendo usuario o no siendo usuario autentificado
	 * O intentando unirse a un evento que no existe en base de datos.
	 */
	public void userJoinEvent(final String username, final int eventID, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			this.userService.checkIfUser();

			final Event event = this.eventService.findOne(eventID);
			Assert.notNull(event);

			final User user = this.userService.findByPrincipal();
			Assert.notNull(user);

			if (event.getMaxParticipants() > event.getActualParticipants()) {
				//Guardar user en el evento
				final List<User> usersEvent = new ArrayList<User>();
				for (final User u : event.getUsers())
					usersEvent.add(u);
				usersEvent.add(user);
				event.setUsers(usersEvent);
				event.setActualParticipants(event.getActualParticipants() + 1);
				this.eventService.save(event);

				//Guardar evento en el user
				final List<Event> eventsUser = new ArrayList<Event>();
				for (final Event e : user.getEvents())
					eventsUser.add(e);
				eventsUser.add(event);
				user.setEvents(eventsUser);
				this.userService.save(user);
			}

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();

		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * 
	 * An actor who is authenticated as a manager or artist must be able to: Manage his or her actors events, which includes creating,
	 * listing, modifying, and deleting them.
	 * An event can be deleted even if there are users in it. If an event is past, it cannot be edited in any way.
	 * 
	 * En este caso de prueba se realizara la acci�n de listar los eventos logueados como manager o artist de forma que solo se listen sus eventos y
	 * no los de otros artistas. Puesto que en ambos casos se muestran los diferentes elementos y no depende de ids no hay ninguna situacion que pueda
	 * forzar un error
	 */
	public void eventsArtistOrManager(final String username, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			final Actor actor = this.actorService.findByPrincipal();
			Collection<Event> events = new ArrayList<Event>();

			if (actor instanceof Manager)
				for (final EventsCalendar c : ((Manager) actor).getEventsCalendars())
					events.addAll(c.getEvents());
			else if (actor instanceof Artist)
				events = ((Artist) actor).getEvents();

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * 
	 * An actor who is authenticated as a manager or artist must be able to: Manage his or her actors events, which includes creating,
	 * listing, modifying, and deleting them.
	 * An event can be deleted even if there are users in it. If an event is past, it cannot be edited in any way.
	 * 
	 * En este caso de prueba se realizara la acci�n de crear un evento. Tanto un artista como un manager pueden crear eventos.
	 * 
	 * Para forzar un fallo se pueden dar los siguientes casos.
	 * 
	 * - El usuario no esta autentificado
	 * - EL usuario autentificado no es un artist o un manager
	 * - Los atributos no son correctos
	 */
	@SuppressWarnings("deprecation")
	public void create(final String username, final String name, final String startDate, final String endDate, final String place, final int maxParticipants, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			Assert.isTrue(username != null);

			this.eventService.checkIfArtistOrManager();

			final Event event = new Event();

			if (this.actorService.findByPrincipal() instanceof Artist)
				event.setArtist((Artist) this.actorService.findByPrincipal());
			else if (this.actorService.findByPrincipal() instanceof Manager)
				event.setArtist(((Manager) this.actorService.findByPrincipal()).getArtist());

			final Collection<User> users = new ArrayList<User>();

			final Actor actor = this.actorService.findByPrincipal();

			event.setName(name);
			event.setStartDate(new Date(startDate));
			event.setEndDate(new Date(endDate));
			event.setPlace(place);
			event.setMaxParticipants(maxParticipants);
			event.setUsers(users);
			event.setActualParticipants(0);

			this.eventService.checkAttributes(event);

			this.eventService.setCalendarEvent(event, actor);

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * 
	 * An actor who is authenticated as a manager or artist must be able to: Manage his or her actors events, which includes creating,
	 * listing, modifying, and deleting them.
	 * An event can be deleted even if there are users in it. If an event is past, it cannot be edited in any way.
	 * 
	 * En este caso de prueba se realizara la acci�n de modifiar un evento. Tanto un artista como un manager pueden editarlos.
	 * 
	 * Para forzar un fallo se pueden dar los siguientes casos.
	 * 
	 * - El usuario no esta autentificado
	 * - EL usuario autentificado no es un artist o un manager
	 * - Los atributos no son correctos
	 * - El id del evento no existe
	 * - El manager o el artista intentan editar un evento que no les pertenece
	 */
	@SuppressWarnings("deprecation")
	public void edit(final String username, final int eventId, final String name, final String startDate, final String endDate, final String place, final int maxParticipants, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			Assert.isTrue(username != null);

			this.eventService.checkIfArtistOrManager();

			final Event event = this.eventService.findOne(eventId);

			this.eventService.checkEvent(event, this.actorService.findByPrincipal());

			if (this.actorService.findByPrincipal() instanceof Artist)
				event.setArtist((Artist) this.actorService.findByPrincipal());
			else if (this.actorService.findByPrincipal() instanceof Manager)
				event.setArtist(((Manager) this.actorService.findByPrincipal()).getArtist());

			final Collection<User> users = new ArrayList<User>();

			@SuppressWarnings("unused")
			final Actor actor = this.actorService.findByPrincipal();

			event.setName(name);
			event.setStartDate(new Date(startDate));
			event.setEndDate(new Date(endDate));
			event.setPlace(place);
			event.setMaxParticipants(maxParticipants);
			event.setUsers(users);
			event.setActualParticipants(0);

			this.eventService.checkAttributes(event);

			this.eventService.save(event);

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * 
	 * An actor who is authenticated as a manager or artist must be able to: Manage his or her actors events, which includes creating,
	 * listing, modifying, and deleting them.
	 * An event can be deleted even if there are users in it. If an event is past, it cannot be edited in any way.
	 * 
	 * En este caso de prueba se realizara la acci�n de borrar un evento. Tanto un artista como un manager pueden borrarlos.
	 * 
	 * Para forzar un fallo se pueden dar los siguientes casos.
	 * 
	 * - El usuario no esta autentificado
	 * - EL usuario autentificado no es un artist o un manager
	 * - El id del evento no existe
	 * - El manager o el artista intentan borrar un evento que no les pertenece
	 */
	public void delete(final String username, final int eventId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			Assert.isTrue(username != null);

			this.eventService.checkIfArtistOrManager();

			final Event event = this.eventService.findOne(eventId);

			this.eventService.checkEvent(event, this.actorService.findByPrincipal());

			this.eventService.delete(event);

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	/*
	 * 
	 * An actor who is authenticated as a manager must be able to: Manage his or her actors calendar events, which includes creating,
	 * listing, deleting and adding events from them.
	 * 
	 * En este caso de prueba se realizara la acci�n de listar los eventos de un manager a partir de un calendario de eventos.
	 * Puesto que el manager es el �nico que puede listar los calendarios de eventos tambien sera el unico que podra listar los eventos a partir de estos.
	 * 
	 * Para forzar un fallo se pueden dar los siguientes casos.
	 * 
	 * - El usuario no esta autentificado
	 * - EL usuario autentificado no es un manager
	 * - El id del calendario de eventos no existe
	 * - El manager intentan listar los eventos de un calendario que no le pertenece
	 */
	public void listByCalendar(final String username, final int eventsCalendarId, final Class<?> expected) {
		Class<?> caught = null;

		try {

			this.authenticate(username);

			Assert.notNull(username);

			this.managerService.checkIfManager();

			final Actor actor = this.actorService.findByPrincipal();

			final EventsCalendar eventsCalendar = this.eventsCalendarService.findOne(eventsCalendarId);

			Assert.isTrue(eventsCalendar.getManager().getId() == actor.getId());

			@SuppressWarnings("unused")
			final Collection<Event> events = eventsCalendar.getEvents();

			this.unauthenticate();

		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListEvents() {
		final Object testingData[][] = {
			// Visualizar listado como admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// Visualizar listado como manager -> false
			{
				"manager1", IllegalArgumentException.class
			},
			// Visualizar listado como user -> true
			{
				"user1", null
			},
			// Visualizar listado como no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listEvents((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverListEventsByArtists() {
		final Object testingData[][] = {
			// Visualizar listado como admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			// Visualizar listado como manager -> false
			{
				"manager1", IllegalArgumentException.class
			},
			// Visualizar listado como user -> true
			{
				"user1", null
			},
			// Visualizar listado como no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listEventsByArtists((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverUserJoinEvent() {
		final Object testingData[][] = {
			// Intentar unirse a evento como admin -> false
			{
				"admin", 168, IllegalArgumentException.class
			},
			// Intentar unirse a evento como manager -> false
			{
				"manager1", 169, IllegalArgumentException.class
			},
			// Intentar unirse a evento como user -> true
			{
				"user1", 169, null
			},
			// Intentar unirse como no autentificado -> false
			{
				null, 169, IllegalArgumentException.class
			},
			// Intentar unirse a un evento inexistente -> false
			{
				"user", 15107, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.userJoinEvent((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void eventsArtistOrManagerDriver() {
		final Object testingData[][] = {
			// Events logueado como manager -> true
			{
				"manager1", null
			},
			// Events logueado como manager -> true
			{
				"manager2", null
			},
			// Events logueado como manager -> true
			{
				"manager3", null
			},
			// Events logueado como artist -> true
			{
				"artist1", null
			},
			// Events logueado como artist -> true
			{
				"artist2", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.eventsArtistOrManager((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void createDriver() {
		final Object testingData[][] = {
			// Crear evento logueado como manager -> true
			{
				"manager1", "evento", "21/10/2018 19:00", "21/10/2018 22:00", "Lugar", 250, null
			},
			// Crear evento logueado como artist -> true
			{
				"artist1", "evento", "21/10/2018 19:00", "21/10/2018 22:00", "Lugar", 250, null
			},
			// Crear evento sin loguearse -> false
			{
				null, "evento", "21/10/2018 19:00", "21/10/2018 22:00", "Lugar", 250, IllegalArgumentException.class
			},
			// Crear evento logueado como user -> false
			{
				"user1", "evento", "21/10/2018 19:00", "21/10/2018 22:00", "Lugar", 250, IllegalArgumentException.class
			},
			// Crear evento con atributos incorrectos -> false
			{
				"artist1", "evento", null, "21/10/2018 22:00", null, 250, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.create((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (int) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	@Test
	public void editDriver() {
		final Object testingData[][] = {
			// Editar evento logueado como manager -> true
			{
				"manager2", 168, "evento", "21/10/2018 19:00", "21/10/2018 22:00", "Lugar", 50, null
			},
			// Editar evento logueado como artist -> true
			{
				"artist1", 168, "evento", "21/10/2018 19:00", "21/10/2018 22:00", "Lugar", 50, null
			},
			// Editar evento sin loguearse -> false
			{
				null, 168, "evento", "21/10/2018 19:00", "21/10/2018 22:00", "Lugar", 50, IllegalArgumentException.class
			},
			// Editar evento logueado como user -> false
			{
				"user1", 168, "evento", "21/10/2018 19:00", "21/10/2018 22:00", "Lugar", 50, IllegalArgumentException.class
			},
			// Editar evento con atributos incorrectos -> false
			{
				"artist1", 168, "evento", null, "21/10/2018 22:00", null, 50, IllegalArgumentException.class
			},
			// Editar evento con un id que no existe -> false
			{
				"artist1", 4278, "evento", null, "21/10/2018 22:00", null, 50, IllegalArgumentException.class
			},
			// Editar evento que no pertenece al usuario logueado -> false
			{
				"manager2", 169, "evento", "21/10/2018 19:00", "21/10/2018 22:00", "Lugar", 50, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.edit((String) testingData[i][0], (int) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (int) testingData[i][6], (Class<?>) testingData[i][7]);
	}

	@Test
	public void deleteDriver() {
		final Object testingData[][] = {
			// Borrar evento logueado como manager -> true
			{
				"manager2", 168, null
			},
			// Borrar evento logueado como artist -> true
			{
				"artist1", 168, null
			},
			// Borrar evento sin loguearse -> false
			{
				null, 168, IllegalArgumentException.class
			},
			// Borrar evento logueado como user -> false
			{
				"user1", 168, IllegalArgumentException.class
			},
			// Borrar evento con un id que no existe -> false
			{
				"artist1", 4278, IllegalArgumentException.class
			},
			// Borrar evento que no pertenece al usuario logueado -> false
			{
				"manager2", 169, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.delete((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void listByCalendarDriver() {
		final Object testingData[][] = {
			// Listar eventos logueado como manager -> true
			{
				"manager2", 166, null
			},
			// Listar eventos logueado como artist -> false
			{
				"artist1", 166, IllegalArgumentException.class
			},
			// Listar eventos sin loguearse -> false
			{
				null, 166, IllegalArgumentException.class
			},
			// Listar eventos con un id que no existe -> false
			{
				"manager2", 4278, IllegalArgumentException.class
			},
			// Listar eventos con un id de calendario que no pertenece al usuario logueado -> false
			{
				"manager2", 167, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listByCalendar((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
